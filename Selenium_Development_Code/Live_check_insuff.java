package check360_flow;

import javax.swing.JOptionPane;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;
public class Live_check_insuff {
	static String doc_com;
	static String raisecomment;
	static String clearcomment;
	static String docupload;
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		 //driver=new FirefoxDriver();
		System.setProperty("webdriver.chrome.driver", "/home/ubuntu/Desktop/Ram/chromedriver");
		driver = new ChromeDriver();
		driver.get("http://192.168.2.17:96");
		driver.manage().window().maximize();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
		Thread.sleep(10000);
		WebElement w1= driver.findElement(By.id("ddlAct"));
		Select s= new Select(w1);
		s.selectByValue("2");
		Thread.sleep(2000);
		 WebElement w31= driver.findElement(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[4]/select"));
		Select s41=new Select(w31);
		s41.selectByValue("1");
		Thread.sleep(1000);
		driver.findElement(By.id("btnsearch")).click();
		Thread.sleep(2000);
		String ref=driver.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr[1]/td[2]/span")).getText();
		driver.findElement(By.id("txtCaserefNo")).sendKeys(ref);
		driver.findElement(By.id("btnsearch")).click();
		Thread.sleep(2500);
		int size=driver.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr[1]/td[2]")).size();
		if(size!=0)
		{
			driver.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr[1]/td[2]")).click();
			Thread.sleep(5000);
			String Checkname=driver.findElement(By.xpath(".//*[@id='tabStrip']/div/ul/li[1]/a/span/span/span")).getText();
			
			if(Checkname.equals("Address"))
			{
				 WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div/iframe")); 
				  driver.switchTo().frame(element);		 
				Thread.sleep(1500);
				  
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlComponent_Input")).click();
				  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlComponent_DropDown']/div/ul/li")).size();
				  System.out.println("No of Copmonent in Address check = "+dc);
				  
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkAddressInsuff")).click();
				  Thread.sleep(2000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressInsuffRemark")).sendKeys("Not Sufficient data");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit_input")).click();
				  Thread.sleep(1500);
				  if(dc==1)
				  {
				  Thread.sleep(4000);
				  Alert alert = driver.switchTo().alert();
				  alert.accept();
				  }
				  else
				  {
				  driver.navigate().refresh(); 
				  Thread.sleep(5000);
				  }
				  
//				  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
//				  {
//				  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
//				  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
//				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
//				  }
//				  else
//				  {
//					  Thread.sleep(2000);
//				  driver.navigate().refresh(); 
//				  }
			}
			else if(Checkname.equals("Education"))
			{
				 WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[2]/iframe")); 
				  driver.switchTo().frame(element);
				  Thread.sleep(3000);
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkEducationInsuff")).click();
				  Thread.sleep(2000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationInsuffRemarks")).sendKeys("Not Sufficient data");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveSubmit_input")).click();
				  Thread.sleep(1500);
				 
				  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
				  {
				  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
				  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
				  }
				  else
				  {
					  Thread.sleep(2000);
				  driver.navigate().refresh(); 
				  }
			}
			 else if (Checkname.equals("Employment"))
			  {
			  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[3]/iframe")); 
			  driver.switchTo().frame(element);
			  Thread.sleep(3000);
			  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkEmploymentInsuff")).click();
			  Thread.sleep(2000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentInsuffRemarks")).sendKeys("Not Sufficient data");
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSubmit_input")).click();
			  Thread.sleep(1500);
			  
			  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
			  {
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
			  }
			  else
			  {
				  Thread.sleep(2000);
			  driver.navigate().refresh(); 
			  }
				 }
				else if (Checkname.equals("ID"))
				{
				  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[10]/iframe")); 
				  driver.switchTo().frame(element);
				  Thread.sleep(3000);
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkIdInsuff")).click();
				  Thread.sleep(2500);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdInsuffRemarks")).sendKeys("Not Sufficient data");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveSubmit_input")).click();
				  Thread.sleep(1500);
				 
				  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
				  {
				  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
				  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
				  }
				  else
				  {
					  Thread.sleep(2000);
				  driver.navigate().refresh(); 
				  }
				 }
				else if (Checkname.equals("Reference"))
				  {
				  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[4]/iframe")); 
				  driver.switchTo().frame(element);
				  Thread.sleep(3000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_CheckRefReportInsuff")).click();
				  Thread.sleep(2000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportInsuff")).sendKeys("Not Sufficient data");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
				  Thread.sleep(1500);
				  
				  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
				  {
				  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
				  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
				  }
				  else
				  {
					  Thread.sleep(2000);
				  driver.navigate().refresh(); 
				  }
					 }
				else if (Checkname.equals("DataBase"))
				  {
				  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[5]/iframe")); 
				  driver.switchTo().frame(element);
				  Thread.sleep(3000);
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkDataBaseInSuff")).click();
				  Thread.sleep(2000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDataBaseInSuffComments")).sendKeys("Not Sufficient data");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnDataBaseSaveSubmit_input")).click();
				  Thread.sleep(1500);	
				 
				  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
				  {
				  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
				  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
				  }
				  else
				  {
					  Thread.sleep(2000);
				  driver.navigate().refresh(); 
				  }					  
				  }
				else if (Checkname.equals("Credit"))
				  {
				  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[7]/iframe")); 
				  driver.switchTo().frame(element);
				  Thread.sleep(3000);
				  if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).isEnabled())
				  {
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).click();
					  Thread.sleep(700);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).sendKeys(Keys.DOWN);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).sendKeys(Keys.DOWN);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).sendKeys(Keys.ENTER);
					  Thread.sleep(1500);		
				   }
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkCreditInsuff")).click();
				  Thread.sleep(2000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditInsuffRemarks")).sendKeys("Not Sufficient data");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditSaveSubmit_input")).click();
				  Thread.sleep(1500);
				  
				  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
				  {
				  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
				  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
				  }
				  else
				  {
					  Thread.sleep(2000);
				  driver.navigate().refresh(); 
				 // tc--;
				  }
				  }
				else if (Checkname.equals("Court"))
				  {
				  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[8]/iframe")); 
				  driver.switchTo().frame(element);
				  Thread.sleep(3000);
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkCourtInsuff")).click();
				  Thread.sleep(2000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtInsuffRemark")).sendKeys("Not Sufficient data");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtSubmit_input")).click();
				  Thread.sleep(1500);
				  
				  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
				  {
				  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
				  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
				  }
				  else
				  {
					  Thread.sleep(2000);
				  driver.navigate().refresh(); 
				  //tc--;
				  }
				  }
				else if (Checkname.equals("Criminal"))
				  {
				  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[6]/iframe")); 
				  driver.switchTo().frame(element);
				  Thread.sleep(3000);
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkCriminalInsuff")).click();
				  Thread.sleep(2000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalInsuffRemark")).sendKeys("Not Sufficient data");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCriminalSubmit_input")).click();
				  Thread.sleep(1500);
				 
				  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
				  {
				  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
				  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
				  }
				  else
				  {
					  Thread.sleep(2000);
				  driver.navigate().refresh(); 
				 // tc--;
				  }
				  }
			driver.switchTo().defaultContent();
			driver.navigate().refresh(); 
			 try
			  {
				 //Thread.sleep(1500);
				 driver.switchTo().defaultContent();			 
					 driver.findElement(By.id("imgHome")).click();
					Thread.sleep(10000);
			  }
			  catch(NoSuchElementException e)
			  {
				  System.out.println(e);
			  }
			driver.findElement(By.id("btnActions")).click();
			Thread.sleep(7000);
			driver.navigate().refresh(); 
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlWorkflowType_Input")).click();
			Thread.sleep(500);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(ref);
			driver.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[1]")).click();
			Thread.sleep(1000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
			Thread.sleep(3000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl04_btnClearInsuff")).click();
			Thread.sleep(2000);
			driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl04_chkInsuffComponentSelectCheckBox")).click();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl04_txtInsuffClearComments")).sendKeys("document uploaded");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl04_btnUploadInsuffDocuments")).click();
			Thread.sleep(2000);
			String doc=driver.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00__0']/td[6]")).getText();
			if(doc.isEmpty())
			{
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Live_Attachments/Address/Add2_check.pdf");
				Thread.sleep(1000);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_btnSubmitAddedDocument")).click();
				Thread.sleep(1000);		
			}
			doc_com=driver.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00__0']/td[6]")).getText();
			driver.findElement(By.xpath(".//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();	
			Thread.sleep(1000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_btnClear_input")).click();
			Thread.sleep(3500);
			 WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();	
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnStages_input")).click();	
			Thread.sleep(10000);	
			w1= driver.findElement(By.id("ddlAct"));
			s= new Select(w1);
			s.selectByValue("2");
			Thread.sleep(2000);
			w31= driver.findElement(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[4]/select"));
			s41=new Select(w31);
			s41.selectByValue("1");
			Thread.sleep(1000);
			driver.findElement(By.id("btnsearch")).click();
			Thread.sleep(2000);
			driver.findElement(By.id("txtCaserefNo")).sendKeys(ref);
			driver.findElement(By.id("btnsearch")).click();
			Thread.sleep(2500);
			int sizee=driver.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr[1]/td[2]")).size();
			if(sizee!=0)
			{
				driver.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr[1]/td[2]")).click();
				Thread.sleep(5000);
				String Checkname1=driver.findElement(By.xpath(".//*[@id='tabStrip']/div/ul/li[1]/a/span/span/span")).getText();
				if(Checkname1.equals("Address"))
				{
					 WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnHistoryPage_input")).click();
					  Thread.sleep(1000);
				}
				else if(Checkname1.equals("Education"))
				{
					 WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[2]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnHistoryPage_input")).click();
					  Thread.sleep(1000);
					  new Live_check_insuff();
				}
				 else if (Checkname1.equals("Employment"))
				  {
				  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[3]/iframe")); 
				  driver.switchTo().frame(element);
				  Thread.sleep(3000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnHistoryPage_input")).click();
				  Thread.sleep(1000);
				  new Live_check_insuff();
					 }
					else if (Checkname1.equals("ID"))
					{
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[10]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnHistoryPage_input")).click();
					  Thread.sleep(1000);
					  new Live_check_insuff();
					 }
					else if (Checkname1.equals("Reference"))
					  {
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[4]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnHistoryPage_input")).click();
					  Thread.sleep(1000);
					  new Live_check_insuff();
						 }
					else if (Checkname1.equals("DataBase"))
					  {
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[5]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnHistoryPage_input")).click();
					  Thread.sleep(1000);
					  new Live_check_insuff();
					  }
					else if (Checkname1.equals("Credit"))
					  {
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[7]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnHistoryPage_input")).click();
					  Thread.sleep(1000);
					  new Live_check_insuff();
					  }
					else if (Checkname1.equals("Court"))
					  {
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[8]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnHistoryPage_input")).click();
					  Thread.sleep(1000);
					  new Live_check_insuff();
					  }
					else if (Checkname1.equals("Criminal"))
					  {
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[6]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnHistoryPage_input")).click();
					  Thread.sleep(1000);
					  new Live_check_insuff();
					  }
		}
		}
	}
	public Live_check_insuff()throws InterruptedException
	{
		Thread.sleep(2500);
	    raisecomment=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdmHistoryPopup1_C_grdInsuffHistory_ctl00_ctl04_lblRaisedRemarks")).getText();	
		 clearcomment=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdmHistoryPopup1_C_grdInsuffHistory_ctl00_ctl04_lblClearComments")).getText();
		 docupload=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdmHistoryPopup1_C_grdInsuffHistory_ctl00_ctl04_lblDoc")).getText();
		Assert.assertTrue(raisecomment.equals("Not Sufficient data"), "Raised Comment");
		Assert.assertTrue(clearcomment.equals("document uploaded"), "Cleared comment");
		Assert.assertTrue(docupload.equals(doc_com), "Uploaded document");
//		System.out.println("Raise Comment="+raisecomment+"\n"+"Clear Comment="+clearcomment+"\n"+"docupload="+docupload+"\n"+"Doccheck="+doc_com);
	}
}
