package Checks360;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Report_Check {
	WebDriverWait wait=new WebDriverWait(Driver.instance,50);
	public static String generate="btnReportGenerate"; 
	public static String preview="rwReportComponent_C_btnRptPreview_input";
	String selectall="_rfdSkinnedrwReportComponent_C_grdReportComponent_ctl00_ctl02_ctl00_chkReportSelectSelectCheckBox";
	int componentsize;
	List<String>  list= new ArrayList<String>();
	String status[],componentname[],compstatus[][],textname[],reportstatus[],reportcomp[],reportcompstatus[][];
	public void report() throws InterruptedException
	{
		Thread.sleep(5000);
		//wait.until(ExpectedConditions.elementToBeClickable(By.id(Case_Registration.action))).click();
		new Select(Driver.instance.findElement(By.id(Case_Registration.stagedropdown))).selectByValue("10");
		Thread.sleep(3500);
		Driver.instance.findElement(By.xpath(Case_Search.DBref)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id(generate))).click();
		//Driver.instance.findElement(By.id(generate)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id(preview)));
		if(!Driver.instance.findElement(By.id(selectall)).isSelected())
		{
			Driver.instance.findElement(By.id(selectall)).click();
		}
		componentsize=Driver.instance.findElements(By.xpath(".//*[@id='rwReportComponent_C_grdReportComponent']/div[2]/table/tbody/tr")).size();
		//System.out.println("componentsize"+componentsize);
		status=new String[componentsize];
		componentname=new String[componentsize];
		reportcomp=new String[componentsize];
		compstatus=new String[componentsize][componentsize];
		for(int size=0;size<componentsize;size++)
		{
			componentname[size]=Driver.instance.findElement(By.xpath(".//*[@id='rwReportComponent_C_grdReportComponent_ctl00__"+size+"']/td[7]")).getText();
			status[size]=Driver.instance.findElement(By.xpath(".//*[@id='rwReportComponent_C_grdReportComponent_ctl00__"+size+"']/td[17]/div[1]/table/tbody[1]/tr/td[1]/input")).getAttribute("value");
			//System.out.println(componentname[size]+"="+status[size]);
		}
		for(int size=0;size<componentsize;size++)
		{
				compstatus[size][size]=componentname[size]+"="+status[size];
				System.out.println(compstatus[size][size]);
		}
		//Thread.sleep(8000);
		//String winHandleBefore = Driver.instance.getWindowHandle();
		wait.until(ExpectedConditions.elementToBeClickable(By.id(preview))).click();
		Thread.sleep(3000);
		ArrayList<String> tabs = new ArrayList<String> ( Driver.instance.getWindowHandles());
		Driver.instance.switchTo().window(tabs.get(1));
		Driver.instance.findElement(By.xpath(".//*[@id='pageContainer2']/xhtml:div[2]")).click();
		Thread.sleep(1000);
		Driver.instance.findElement(By.xpath(".//*[@id='pageContainer3']/xhtml:div[2]")).click();
		Thread.sleep(2000);
		int textsize=Driver.instance.findElements(By.xpath(".//*[@id='pageContainer3']/xhtml:div[2]/xhtml:div")).size();
		textname=new String[textsize-2];
		reportstatus=new String[textsize-2];
		System.out.println("textsize="+textsize);
		for(int i=1;i<textsize-2;i++)
		{
			textname[i]=Driver.instance.findElement(By.xpath(".//*[@id='pageContainer3']/xhtml:div[2]/xhtml:div["+i+"]")).getText();
			System.out.println("textname="+textname[i]);
		}
		reportcompstatus=new String[componentsize][componentsize];
		int inputsize=0;
		for(int size=0;size<componentsize;size++)
		{
			for(int i=1;i<textsize-2;i++)
			{
				if(componentname[size].equals(textname[i]))
				{
					
					reportstatus[size]=Driver.instance.findElement(By.xpath(".//*[@id='pageContainer3']/xhtml:div[2]/xhtml:div["+(i+1)+"]")).getText();
					reportcompstatus[inputsize][inputsize]=componentname[size]+"="+reportstatus[size];
					System.out.println(reportcompstatus[inputsize][inputsize]);
					inputsize++;
				}
				/*if(status[size].equals(textname[i]))
				{
					System.out.println("Statustext="+textname[size]);
				}*/
			}
		}
		int testsize=0;
		for(int size=0;size<=inputsize;size++)
		{
			for(int sizes=0;sizes<=inputsize;sizes++)
			{
			if(reportcompstatus[testsize][testsize]!=null&&compstatus[size][sizes]!=null)
			{
				if(reportcompstatus[testsize][testsize].equals(compstatus[size][sizes]))
				{
					//System.out.println("Status in Executive Summary looks correct for the component's="+testsize+compstatus[size][sizes]);
					testsize++;
					break;
				}
			}
		}
		}
		//System.out.println("testsize="+testsize);
		//System.out.println("inputsize="+inputsize);
		if(testsize==inputsize)
		{
			System.out.println("Status in Executive Summary looks correct for the component's");
		}
		else
		{
			System.out.println("Status in Executive Summary looks not correct for the component's");
		}
		//for(String winHandle : Driver.instance.getWindowHandles()){
		   // Driver.instance.switchTo().window(winHandle);
		//}
		}

}
