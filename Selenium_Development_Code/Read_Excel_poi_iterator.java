package Checks360;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;


public class Read_Excel_poi_iterator {
    private static final String FILE_NAME = "/home/pyr/sheet.xls";

    public static void main(String[] args) throws IOException {

            FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
            Workbook workbook = new HSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> rowiterator = datatypeSheet.iterator();
            while (rowiterator.hasNext()) {
                Row currentRow = rowiterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                
  while (cellIterator.hasNext()) {
                    Cell currentCell = cellIterator.next();
                    if (currentCell.getCellTypeEnum() == CellType.STRING) {
                        System.out.print(currentCell.getStringCellValue() + " ");
                    } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                        System.out.print(currentCell.getNumericCellValue() + " ");
                    }            
                  
                }
                System.out.println();
            }
    }
}

