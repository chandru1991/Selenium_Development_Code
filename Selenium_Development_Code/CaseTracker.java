package Checks360;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import java.util.Date;  
import jxl.read.biff.BiffException;

public class CaseTracker {
	 WebDriverWait wait=new WebDriverWait(Driver.instance,50);
	 String casetracker="CaseTracker";
	 String advancesearch=".//*[@id='angularPag']/div/div[1]/a";
	 String casestatus="html/body/div[2]/div/div/div[2]/div/div[3]/select";
	 String Regby="html/body/div[2]/div/div/div[2]/div/div[10]/select";
	 String search="html/body/div[2]/div/div/div[3]/button";
	 String firstname="//input[@ng-model='AdvanceSrch.firstname']";
	 String lastname="//input[@ng-model='AdvanceSrch.LastName']";
	 String priority="//select[@ng-model='AdvanceSrch.priority']";
	 String corporate="//select[@ng-model='AdvanceSrch.ddlcorporatename']";
	 String clientname="//select[@ng-model='AdvanceSrch.ddlclientname']";
	 String projectname="//select[@ng-model='AdvanceSrch.ddlprojectname']";
	 String TATstatus="//select[@ng-model='AdvanceSrch.tatStatus']";
	 String caseoutcome="//select[@ng-model='AdvanceSrch.ddlCaseColourStatus']";
	 String casereceivedon_From="txtreceivedonfrom";
	 String casereceivedon_To="txtreceivedonto";
	 String reportpublishon_From="txtpublishedonfrom";
	 String reportpublishon_To="txtpublishedonto";
	 String pages=".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[2]/span[2]";
	 String searchbox="searchBox";
	 String searchbutton=".//*[@id='angularPag']/div/div[1]/span";
	 String records=".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[2]/span[1]";
	 int pagesize,cols,count,recordsize;
	 Boolean bool;
	 Date datefrom,dateto,date;
	 String totalpage,totalrecord;
	 static ArrayList<String> tabs;
	 String caseoutcome_result,Reportpublishedon_result,priorityresult,clientresult,projectresult,TATresult,Casereceivedon_result,refno,clientrefno;
	public void opencasetracker() throws BiffException, IOException, InterruptedException
	{
		Excel_Read.casetrack();
		wait.until(ExpectedConditions.elementToBeClickable(By.id(casetracker))).click();
		tabs = new ArrayList<String> (Driver.instance.getWindowHandles());
		Driver.instance.switchTo().window(tabs.get(1)).manage().window().maximize();
	}
	public void advancesearch() throws BiffException, IOException, InterruptedException, ParseException
	{
		Driver.instance.findElement(By.xpath(advancesearch)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(search)));	
		Driver.instance.findElement(By.xpath(firstname)).sendKeys(Excel_Read.input[1][0]);
		Driver.instance.findElement(By.xpath(lastname)).sendKeys(Excel_Read.input[1][1]);
		new Select(Driver.instance.findElement(By.xpath(casestatus))).selectByVisibleText(Excel_Read.input[1][2]);
		new Select(Driver.instance.findElement(By.xpath(priority))).selectByVisibleText(Excel_Read.input[1][3]);
		new Select(Driver.instance.findElement(By.xpath(corporate))).selectByVisibleText(Excel_Read.input[1][4]);
		new Select(Driver.instance.findElement(By.xpath(clientname))).selectByVisibleText(Excel_Read.input[1][5]);
		new Select(Driver.instance.findElement(By.xpath(projectname))).selectByVisibleText(Excel_Read.input[1][6]);
		new Select(Driver.instance.findElement(By.xpath(TATstatus))).selectByVisibleText(Excel_Read.input[1][7]);
		new Select(Driver.instance.findElement(By.xpath(caseoutcome))).selectByVisibleText(Excel_Read.input[1][8]);
		new Select(Driver.instance.findElement(By.xpath(Regby))).selectByVisibleText(Excel_Read.input[1][9]);
		Driver.instance.findElement(By.id(casereceivedon_From)).sendKeys(Excel_Read.input[1][10]);
		Driver.instance.findElement(By.id(casereceivedon_To)).sendKeys(Excel_Read.input[1][11]);
		Driver.instance.findElement(By.id(reportpublishon_From)).sendKeys(Excel_Read.input[1][12]);
		Driver.instance.findElement(By.id(reportpublishon_To)).sendKeys(Excel_Read.input[1][13]);
		Driver.instance.findElement(By.xpath(search)).click();
		Thread.sleep(2000);
		totalpage=Driver.instance.findElement(By.xpath(pages)).getText();
		pagesize=Integer.parseInt(totalpage);
		totalrecord=Driver.instance.findElement(By.xpath(records)).getText();
		recordsize=Integer.parseInt(totalrecord);
		if(recordsize==0)
		{
			 System.out.println("No Records Found");
			 Excel_write.inputExcel("No Records Found","NIL");
		}
		else{
		//System.out.println("pagesize="+pagesize);
		if(!Excel_Read.input[1][3].equals("All"))//Priority
		{
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
				//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					priorityresult=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[10]/span")).getText();
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					bool=false;
					if(Excel_Read.input[1][3].equals(priorityresult))
					{
						bool=true;
					}
					if(bool==false)
					{
						 Excel_write.inputExcel("Priority","Fail");
					}
					Assert.assertTrue(bool, "Priority not matched - "+refno);
				}
			}
			if(bool==true)
			{
				Excel_write.inputExcel("Priority","Pass");
			}
		}
		if(!Excel_Read.input[1][5].equals("All"))//client_name
		{
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
				//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					clientresult=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[4]/span")).getText();
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					bool=false;
					if(Excel_Read.input[1][5].equals(clientresult))
					{
						bool=true;
					}
					if(bool==false)
					{
						 Excel_write.inputExcel("ClientName","Fail");
					}
					Assert.assertTrue(bool, "client_name not matched - "+refno);
				}				
			}
			if(bool==true)
			{
				 Excel_write.inputExcel("ClientName","Pass");
			}
		}
		if(!Excel_Read.input[1][6].equals("All"))//project_name
		{
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
				//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					projectresult=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[5]/span")).getText();
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					bool=false;
					if(Excel_Read.input[1][6].equals(projectresult))
					{
						bool=true;
					}
					if(bool==false)
					{
						 Excel_write.inputExcel("Projectname","Fail");
					}
					Assert.assertTrue(bool, "project_name not matched - "+refno);
				}				
			}
			if(bool==true)
			{
				 Excel_write.inputExcel("Projectname","Pass");
			}
		}
		if(!Excel_Read.input[1][7].equals("All"))//TAT_Status
		{
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
				//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					TATresult=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[12]")).getText();
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					bool=false;
					if(Excel_Read.input[1][7].equals(TATresult))
					{
						bool=true;
					}
					if(bool==false)
					{
						 Excel_write.inputExcel("TAT_Status","Fail");
					}
					Assert.assertTrue(bool, "TAT_Status not matched - "+refno);
				}				
			}
			if(bool==true)
			{
				 Excel_write.inputExcel("TAT_Status","Pass");
			}
		}
		if(!Excel_Read.input[1][8].equals("All"))//Case_Outcome
		{
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
				//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					caseoutcome_result=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[11]/span")).getText();
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					bool=false;
					if(Excel_Read.input[1][8].equals(caseoutcome_result))
					{
						bool=true;
					}
					if(bool==false)
					{
						 Excel_write.inputExcel("Case_Outcome","Fail");
					}
					Assert.assertTrue(bool, "Case_Outcome not matched - "+refno);
				}				
			}
			if(bool==true)
			{
				 Excel_write.inputExcel("Case_Outcome","Pass");
			}
		}
		if((!Excel_Read.input[1][10].equals(""))&&(!Excel_Read.input[1][11].equals("")))//CaseReceivedon - fromto
			{
			datefrom=new SimpleDateFormat("dd/MM/yyyy").parse(Excel_Read.input[1][10]);  
		    dateto=new SimpleDateFormat("dd/MM/yyyy").parse(Excel_Read.input[1][11]);  
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
				//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					Casereceivedon_result=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[7]/span")).getText();
					date=new SimpleDateFormat("dd/MM/yyyy").parse(Casereceivedon_result); 
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					//System.out.println(date+"\n"+datefrom+"\n"+dateto);
					bool=false;
					if((datefrom.before(date)||datefrom.equals(date))&&(dateto.after(date)|| dateto.equals(date)))
					{
						bool=true;
					}
					if(bool==false)
					{
						 Excel_write.inputExcel("CaseReceivedon","Fail");
					}
					Assert.assertTrue(bool, "Case_Receivedon date not matched - "+refno);
				}				
			}
			if(bool==true)
			{
				 Excel_write.inputExcel("CaseReceivedon","Pass");
			}
		}
		else if ((Excel_Read.input[1][10].equals(""))&&(!Excel_Read.input[1][11].equals("")))//CaseRecievedon - to
		{
			dateto=new SimpleDateFormat("dd/MM/yyyy").parse(Excel_Read.input[1][11]);
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
				//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					Casereceivedon_result=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[7]/span")).getText();
					date=new SimpleDateFormat("dd/MM/yyyy").parse(Casereceivedon_result); 
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					//System.out.println(date+"\n"+datefrom+"\n"+dateto);
					bool=false;
					if(dateto.after(date)||dateto.equals(date))
					{
						bool=true;
					}
					if(bool==false)
					{
						 Excel_write.inputExcel("CaseReceivedon","Fail");
					}
					Assert.assertTrue(bool, "Case_Receivedon date not matched - "+refno);
				}				
			}
			if(bool==true)
			{
				 Excel_write.inputExcel("CaseReceivedon","Pass");
			}
		}
		else if ((!Excel_Read.input[1][10].equals(""))&&(Excel_Read.input[1][11].equals("")))//CaseReceivedon - from
		{
			datefrom=new SimpleDateFormat("dd/MM/yyyy").parse(Excel_Read.input[1][10]);
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
				//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					Casereceivedon_result=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[7]/span")).getText();
					date=new SimpleDateFormat("dd/MM/yyyy").parse(Casereceivedon_result); 
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					//System.out.println(date+"\n"+datefrom+"\n"+dateto);
					bool=false;
					if(datefrom.before(date)||datefrom.equals(date))
					{
						bool=true;
					}
					if(bool==false)
					{
						 Excel_write.inputExcel("CaseReceivedon","Fail");
					}
					Assert.assertTrue(bool, "Case_Receivedon date not matched - "+refno);
				}				
			}
			if(bool==true)
			{
				 Excel_write.inputExcel("CaseReceivedon","Pass");
			}
		}
		if((!Excel_Read.input[1][12].equals(""))&&(!Excel_Read.input[1][13].equals("")))//Report_Publishedon - fromto
		{
			datefrom=new SimpleDateFormat("dd/MM/yyyy").parse(Excel_Read.input[1][12]);  
			dateto=new SimpleDateFormat("dd/MM/yyyy").parse(Excel_Read.input[1][13]);  
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
				//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					Reportpublishedon_result=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[9]/a/span")).getText();
					date=new SimpleDateFormat("dd/MM/yyyy").parse(Reportpublishedon_result); 
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					//System.out.println(date+"\n"+datefrom+"\n"+dateto);
					bool=false;
					if((datefrom.before(date)||datefrom.equals(date))&&(dateto.after(date)||dateto.equals(date)))
					{
						bool=true;
					}
					if(bool==false)
					{
						 Excel_write.inputExcel("Report_Publishedon","Fail");
					}
					Assert.assertTrue(bool, "Report_Publishedon date not matched - "+refno);
				}				
			}
			if(bool==true)
			{
				 Excel_write.inputExcel("Report_Publishedon","Pass");
			}
		}
		else if((Excel_Read.input[1][12].equals(""))&&(!Excel_Read.input[1][13].equals("")))//Report_Publishedon - to
		{
			dateto=new SimpleDateFormat("dd/MM/yyyy").parse(Excel_Read.input[1][13]);  
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
				//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					Reportpublishedon_result=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[9]/a/span")).getText();
					date=new SimpleDateFormat("dd/MM/yyyy").parse(Reportpublishedon_result); 
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					//System.out.println(date+"\n"+datefrom+"\n"+dateto);
					bool=false;
					if(dateto.after(date)||dateto.equals(date))
					{
						bool=true;
					}
					if(bool==false)
					{
						 Excel_write.inputExcel("Report_Publishedon","Fail");
					}
					Assert.assertTrue(bool, "Report_Publishedon date not matched - "+refno);
				}				
			}
			if(bool==true)
			{
				 Excel_write.inputExcel("Report_Publishedon","Pass");
			}
		}
		else if((!Excel_Read.input[1][12].equals(""))&&(Excel_Read.input[1][13].equals("")))//Report_Publishedon - from
		{
			datefrom=new SimpleDateFormat("dd/MM/yyyy").parse(Excel_Read.input[1][12]);  
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
								//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					Reportpublishedon_result=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[9]/a/span")).getText();
					date=new SimpleDateFormat("dd/MM/yyyy").parse(Reportpublishedon_result); 
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					//System.out.println(date+"\n"+datefrom+"\n"+dateto);
					bool=false;
					if(datefrom.before(date)||datefrom.equals(date))
					{
						bool=true;
					}
					if(bool==false)
					{
						 Excel_write.inputExcel("Report_Publishedon","Fail");
					}
					Assert.assertTrue(bool, "Report_Publishedon date not matched - "+refno);
				}				
			}
			if(bool==true)
			{
				 Excel_write.inputExcel("Report_Publishedon","Pass");
			}
		}
		}
		close();
		//System.out.println(priorityresult+"\n"+clientresult+"\n"+projectresult+"\n"+TATresult+"\n"+caseoutcome_result+"\n"+Casereceivedon_result+"\n"+Reportpublishedon_result);	
	}	
	public void search() throws InterruptedException, IOException
	{
		if((!Excel_Read.input[1][14].equals(""))&&(!Excel_Read.input[1][15].equals("")))// ClientRefno - Refno
		{
			Driver.instance.findElement(By.id(searchbox)).sendKeys(Excel_Read.input[1][14]+","+Excel_Read.input[1][15]);
			Driver.instance.findElement(By.xpath(searchbutton)).click();
			Thread.sleep(2000);
			totalpage=Driver.instance.findElement(By.xpath(pages)).getText();
			pagesize=Integer.parseInt(totalpage);
			totalrecord=Driver.instance.findElement(By.xpath(records)).getText();
			recordsize=Integer.parseInt(totalrecord);
			if(recordsize==0)
			{
				System.out.println("No Records Found");
				Excel_write.inputExcel("No Records Found","NIL");
			}
			else
			{
			count=0;
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
								//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					clientrefno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[2]/span")).getText();
					if(refno.equals(Excel_Read.input[1][14]))
					{
						count++;
					}
					if(clientrefno.equals(Excel_Read.input[1][15]))
					{
						count++;
					}
				}
			}
			bool=false;
			if(count>1)
			{
				bool=true;
			}
			if(bool==false)
			{
				Excel_write.inputExcel("ClientRefno - Refno","Fail");
				Assert.assertTrue(bool,"Client Reference Number and Refernce Number not matched");
			}
			else
			{
				Excel_write.inputExcel("ClientRefno - Refno","Pass");
			}
			}
		}
		else if((Excel_Read.input[1][14].equals(""))&&(!Excel_Read.input[1][15].equals("")))//ClientRefno
		{
			Driver.instance.findElement(By.id(searchbox)).sendKeys(Excel_Read.input[1][15]);
			Driver.instance.findElement(By.xpath(searchbutton)).click();
			Thread.sleep(2000);
			totalpage=Driver.instance.findElement(By.xpath(pages)).getText();
			pagesize=Integer.parseInt(totalpage);
			totalrecord=Driver.instance.findElement(By.xpath(records)).getText();
			recordsize=Integer.parseInt(totalrecord);
			if(recordsize==0)
			{
				 System.out.println("No Records Found");
				 Excel_write.inputExcel("No Records Found","NIL");
			}
			else
			{
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
								//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					clientrefno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[2]/span")).getText();
					bool=false;
					if(clientrefno.equals(Excel_Read.input[1][15]))
					{
						bool=true;
					}
					if(bool==false)
					{
						Excel_write.inputExcel("ClientRefno","Fail");
					}
					Assert.assertTrue(bool,"Client Reference Number not matched");
				}
			}
			if(bool==true)
			{
				Excel_write.inputExcel("ClientRefno","Pass");
			}
			}
		}
		else if((!Excel_Read.input[1][14].equals(""))&&(Excel_Read.input[1][15].equals("")))//Refno
		{
			Driver.instance.findElement(By.id(searchbox)).sendKeys(Excel_Read.input[1][14]);
			Driver.instance.findElement(By.xpath(searchbutton)).click();
			Thread.sleep(2000);
			totalpage=Driver.instance.findElement(By.xpath(pages)).getText();
			pagesize=Integer.parseInt(totalpage);
			totalrecord=Driver.instance.findElement(By.xpath(records)).getText();
			recordsize=Integer.parseInt(totalrecord);
			if(recordsize==0)
			{
				 System.out.println("No Records Found");
				 Excel_write.inputExcel("No Records Found","NIL");
			}
			else
			{
			for(int sizes=1;sizes<=pagesize;sizes++)
			{
				Driver.instance.findElement(By.xpath(".//*[@id='angularPag']/div/div[2]/table/thead/tr/td[1]/ul/li["+(sizes+2)+"]/a")).click();
				Thread.sleep(2000);
				cols=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[7]/span")).size();
								//System.out.println("cols"+sizes+"="+cols);
				for(int colsize=1;colsize<=cols;colsize++)
				{
					refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+colsize+"]/td[1]/a/span")).getText();
					bool=false;
					if(refno.equals(Excel_Read.input[1][14]))
					{
						bool=true;
					}
					if(bool==false)
					{
						Excel_write.inputExcel("Refno","Fail");
					}
					Assert.assertTrue(bool,"Reference Number not matched");
				}
			}
			if(bool==true)
			{
				Excel_write.inputExcel("Refno","Pass");
			}
		}
		}
		close();
	}
	public void close() throws InterruptedException
	{
		Driver.instance.close();
		Thread.sleep(500);
		Driver.instance.switchTo().window(tabs.get(0));
	}

}
