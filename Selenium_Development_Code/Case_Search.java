package Checks360;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Case_Search extends txt_write {
	String ref,name,csfirstname,cslastname,firstlastname;
	int size;
	static WebDriverWait wait=new WebDriverWait(Driver.instance,20);
	private String CS="ButtonDuplicateSearch";
	private String CSsearch="txtCaseSearch";
	private String CSbutton="btnCasesearch";
	private String CSnamefirst="html/body/div[3]/div/div/div[2]/div[2]/table/tbody/tr/td[2]";
	private String CSnamelast="html/body/div[3]/div/div/div[2]/div[2]/table/tbody/tr/td[3]";
	public static String DBref=".//*[@id='grdTaskList']/tbody/tr[1]/td[2]/span";
	public String DBname=".//*[@id='grdTaskList']/tbody/tr/td[4]/span";
	//private WebElement CS=Driver.instance.findElement(By.id("ButtonDuplicateSearch"));
	boolean bool;
	public void case_search() throws Exception
	{
		//text("***************Case Search Functionality****************");
		try
		{
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(CS)));
		Thread.sleep(1000);		
		ref= Driver.instance.findElement(By.xpath(DBref)).getText();
		//System.out.println(ref);
		name= Driver.instance.findElement(By.xpath(DBname)).getText();
		//System.out.println(name);
		Thread.sleep(500);
		Driver.instance.findElement(By.id(CS)).click();
		Driver.instance.findElement(By.id(CSsearch)).clear();
		Driver.instance.findElement(By.id(CSsearch)).sendKeys(ref);
		Driver.instance.findElement(By.id(CSbutton)).click();
		Thread.sleep(1000);
		size=Driver.instance.findElements(By.xpath(CSnamefirst)).size();		
		if(size==1)
		{
			bool=true;
			text("Case_Search is working fine");
			csfirstname=Driver.instance.findElement(By.xpath(CSnamefirst)).getText();
			cslastname=Driver.instance.findElement(By.xpath(CSnamelast)).getText();
			firstlastname=csfirstname.concat(cslastname);
			//System.out.println(firstlastname);
			if(name.trim().replace(" ", "").equals(firstlastname.trim().replace(" ", "")))
			{				
				text("Case Matched");
				Driver.instance.findElement(By.id(CSsearch)).clear();
				Driver.instance.findElement(By.id(CSsearch)).sendKeys(ref+"a");
				Driver.instance.findElement(By.id(CSbutton)).click();
				Thread.sleep(1000);
				Driver.instance.findElement(By.xpath("html/body/div[4]/div/div/div[2]/input")).click();
				int cssize=Driver.instance.findElements(By.xpath(CSnamefirst)).size();
				if(cssize==0)
				{
					//System.out.println("CasedashBoard empty and working fine");
					text("CasedashBoard empty and working fine");
				}
				else
					//System.out.println("CasedashBoard not empty and not working fine");			
					text("CasedashBoard not empty and not working fine");
			}			
		}
		else
		{
			//System.out.println("Case_Search is not working");
			text("Case_Search is not working");
		}		
		Assert.assertTrue(bool);	
		Thread.sleep(1000);
		Driver.instance.findElement(By.xpath("html/body/div[3]/div/div/div[1]/button")).click();
		Thread.sleep(1000);
		}catch(Exception e)
		{
			//System.out.println("Case Search Button Not Visible"+e);
			text("Case Search Button Not Visible");
		}
		
		
	}
	}

