package check360_flow;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Client_insuff_DE {
	static int checksize;
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		WebElement sd;
		driver.get("http://192.168.2.17:86");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
		Thread.sleep(7000);
		String rno;
		//String raise=JOptionPane.showInputDialog("Select stages to raise insuff"+"\n"+"1.Data Entry"+"\n"+"2.Data Entry QC"+"\n"+"3.Verification");
		//int cep=Integer.parseInt(raise);
		WebElement w1= driver.findElement(By.id("ddlAct"));	 
		WebElement w2= driver.findElement(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[4]/select"));
		String Checkname;
		  int wait;
		  int TabCheck;
		  
		  int i,k=4,j=0;
		  int dcc = 0;
		  String CreditID;			
		Select s= new Select(w1);
		s.selectByValue("1");
		Thread.sleep(1000);
		Select s1=new Select(w2);
		 s1.selectByValue("2");
		 Thread.sleep(2500);
		 driver.findElement(By.id("btnsearch")).click();
		 rno=JOptionPane.showInputDialog("Enter Reference number:\n");
		 driver.findElement(By.id("txtCaserefNo")).clear();
			driver.findElement(By.id("txtCaserefNo")).sendKeys(rno);
			driver.findElement(By.id("btnsearch")).click();
			Thread.sleep(5000);			
			 wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).size();
			//System.out.println(wait);
			if(wait!=0)
			{
				try
				{
				WebElement w= driver.findElement(By.id("Reserverfor"));
				if(driver.findElement(By.id("Reserverfor")).isDisplayed())
				{
				Select s2= new Select(w);
				s2.selectByValue("0");
				Thread.sleep(1000);
				}
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
				
			driver.findElement(By.id("btnView")).click();
			Thread.sleep(3500);
/////////////////////////////////////////////////////////////TL Insuff//////////////////////////////////////////////
			driver.switchTo().activeElement();
			checksize=driver.findElements(By.xpath("//*[@id='secTable']/tbody/tr/td[1]/span")).size();
			System.out.println("checksize="+checksize);
			
			for(i=1;i<=checksize;i++)
			{				
				try
				{
				Thread.sleep(1500);
			Checkname=driver.findElement(By.xpath("//*[@id='secTable']/tbody/tr["+i+"]/td[1]/span")).getText();
			System.out.println(Checkname);
			if(Checkname.equals("Permanent")||Checkname.equals("Previous Address 2"))
			{
				if(driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).isDisplayed())
				{
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).click();
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).sendKeys("raise insuff "+Checkname);
				driver.findElement(By.xpath("(//input[@id='btninsuffrise'])["+i+"]")).click();				
				Thread.sleep(1500);
				driver.switchTo().alert().accept();
				//checksize--;
				}
			}
			else if(Checkname.equals("Highest 1")||Checkname.equals("10th"))
			{
				if(driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).isDisplayed())
				{
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).click();
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).sendKeys("raise insuff "+Checkname);
				driver.findElement(By.xpath("(//input[@id='btninsuffrise'])["+i+"]")).click();				
				Thread.sleep(1500);
				driver.switchTo().alert().accept();
				//checksize--;
				}
			}
			else if(Checkname.equals("Current Employment")||Checkname.equals("Previous Employment"))
			{
				if(driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).isDisplayed())
				{
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).click();
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).sendKeys("raise insuff "+Checkname);
				driver.findElement(By.xpath("(//input[@id='btninsuffrise'])["+i+"]")).click();				
				Thread.sleep(1500);
				driver.switchTo().alert().accept();
				//checksize--;
				}
			}
			else if(Checkname.equals("Reference 1"))
			{
				if(driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).isDisplayed())
				{
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).click();
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).sendKeys("raise insuff "+Checkname);
				driver.findElement(By.xpath("(//input[@id='btninsuffrise'])["+i+"]")).click();				
				Thread.sleep(1500);
				driver.switchTo().alert().accept();
				//checksize--;
				}
			}
			else if(Checkname.equals("Current Address Court Check")||Checkname.equals("Previous Address 2 Court Check"))
			{
				if(driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).isDisplayed())
				{
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).click();
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).sendKeys("raise insuff "+Checkname);
				driver.findElement(By.xpath("(//input[@id='btninsuffrise'])["+i+"]")).click();				
				Thread.sleep(1500);
				driver.switchTo().alert().accept();
				//checksize--;
				}
			}
			else if(Checkname.equals("Current Address Criminal Check")||Checkname.equals("Previous Address 2 Criminal Check"))
			{
				if(driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).isDisplayed())
				{
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).click();
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).sendKeys("raise insuff "+Checkname);
				driver.findElement(By.xpath("(//input[@id='btninsuffrise'])["+i+"]")).click();				
				Thread.sleep(1500);
				driver.switchTo().alert().accept();
				//checksize--;
				}
			}
			else if(Checkname.equals("Medical Test"))
			{
				if(driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).isDisplayed())
				{
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).click();
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).sendKeys("raise insuff "+Checkname);
				driver.findElement(By.xpath("(//input[@id='btninsuffrise'])["+i+"]")).click();				
				Thread.sleep(1500);
				driver.switchTo().alert().accept();
				//checksize--;
				}
			}
			else if(Checkname.equals("Credit Check 2")||Checkname.equals("Credit Check 3"))
			{
				if(driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).isDisplayed())
				{
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).click();
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).sendKeys("raise insuff "+Checkname);
				driver.findElement(By.xpath("(//input[@id='btninsuffrise'])["+i+"]")).click();				
				Thread.sleep(1500);
				driver.switchTo().alert().accept();
				//checksize--;
				}
			}
			else if(Checkname.equals("Voter ID")||Checkname.equals("ID Check 2"))
			{
				if(driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).isDisplayed())
				{
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).click();
				driver.findElement(By.xpath("(//input[@id='txtcomponentInsuffComment'])["+i+"]")).sendKeys("raise insuff "+Checkname);
				driver.findElement(By.xpath("(//input[@id='btninsuffrise'])["+i+"]")).click();				
				Thread.sleep(1500);
				driver.switchTo().alert().accept();
				//checksize--;
				}
			}
				}
				catch(NoSuchElementException e)
				{
					continue;
				}
			
			}			
			driver.findElement(By.xpath("html/body/div[3]/div/div/div/button")).click();
			}
			Thread.sleep(2500);
			Select s2= new Select(w1);
			s2.selectByValue("2");
			Thread.sleep(1000);
			Select s3=new Select(w2);
			 s3.selectByValue("2");
			 Thread.sleep(2500);
			driver.findElement(By.id("btnsearch")).click();
			Thread.sleep(1500);
			 driver.findElement(By.id("btnGetNext")).click();
			// driver.findElement(By.id("txtCaserefNo")).clear();
			//	driver.findElement(By.id("txtCaserefNo")).sendKeys(rno);
			//	driver.findElement(By.id("btnsearch")).click();
			//	Thread.sleep(5000);			
				/* wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).size();
				//System.out.println(wait);
				if(wait!=0)
				{
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).click();*/
				//}
			Thread.sleep(5000);
			 TabCheck = driver.findElements(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).size();
			 System.out.println("No of Checks in the Case:"+TabCheck);	
			 ///////////////////////////////////////////////TM Insuff/////////////////////////////////////////
			 for(int tc=1;tc<=TabCheck;tc++)
			 {
				 try
				 {
				 driver.switchTo().defaultContent();
				 	Thread.sleep(4000);
				 	  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).getText();
				 	  System.out.println("Checkname"+Checkname);
				 	 if (Checkname.equals("Address"))
					  {						  
				 		 try
				 		 {
						  //Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
						 WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div/iframe")); 
						  driver.switchTo().frame(element);
						  Thread.sleep(3000);
						  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkAddressInsuff")).click();
						  Thread.sleep(2000);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressInsuffRemark")).sendKeys("Not Sufficient data "+Checkname);
						  Thread.sleep(1000);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit_input")).click();
						  Thread.sleep(1500);
						 
						  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
						  {
						  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
						  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
						  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
						  }
						  else
						  {
							  Thread.sleep(2000);
						  driver.navigate().refresh(); 
						  tc--;
						  }
						  }
						  catch(UnhandledAlertException e)
						  {
							//driver.switchTo().alert().accept();
					 			continue;
						 }
						  Thread.sleep(2000);
					  }			
				 	 else if (Checkname.equals("Education"))
					  {
				 		 try
						  {
						  //Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[2]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkEducationInsuff")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationInsuffRemarks")).sendKeys("Not Sufficient data "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveSubmit_input")).click();
					  Thread.sleep(1500);
					 
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
				 	catch(UnhandledAlertException e)
					  {
				 		//driver.switchTo().alert().accept();
			 			continue;
					 }
					  Thread.sleep(2000);
					  }
				 	 else if (Checkname.equals("Employment"))
					  {
				 		try
						 {
						  Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[3]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkEmploymentInsuff")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentInsuffRemarks")).sendKeys("Not Sufficient data "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSubmit_input")).click();
					  Thread.sleep(1500);
					  
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
					 	catch(UnhandledAlertException e)
						 {
					 		//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(2000);
					  }
				 	else if (Checkname.equals("ID"))
					  {
				 		 try
						  {
						  //Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[10]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkIdInsuff")).click();
					  Thread.sleep(2500);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdInsuffRemarks")).sendKeys("Not Sufficient data "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveSubmit_input")).click();
					  Thread.sleep(1500);
					 
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					 }
					 catch(UnhandledAlertException e)
						 {
						//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(2000);
					  }
				 	 else if (Checkname.equals("Reference"))
					  {
				 		try
						 {
						  //Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[4]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_CheckRefReportInsuff")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportInsuff")).sendKeys("Not Sufficient data "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
					  Thread.sleep(1500);
					  
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
					 catch(UnhandledAlertException e)
						  {
						//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(2000);
					  }
				 	else if (Checkname.equals("DataBase"))
					  {
				 		 try
						  {
						 // Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[5]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkDataBaseInSuff")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDataBaseInSuffComments")).sendKeys("Not Sufficient data "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnDataBaseSaveSubmit_input")).click();
					  Thread.sleep(1500);	
					 
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  }					  
					  }
					 	catch(UnhandledAlertException e)
						  {
					 		//driver.switchTo().alert().accept();
				 			continue;
						  }
					  tc--;
					  Thread.sleep(2000);
					  }
				 	else if (Checkname.equals("Credit"))
					  {
				 		try
						 {
						  //Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[7]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).isEnabled())
					  {
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).click();
						  Thread.sleep(700);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).sendKeys(Keys.DOWN);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).sendKeys(Keys.DOWN);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).sendKeys(Keys.ENTER);
						  Thread.sleep(1500);		
					   }
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkCreditInsuff")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditInsuffRemarks")).sendKeys("Not Sufficient data "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditSaveSubmit_input")).click();
					  Thread.sleep(1500);
					  
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
					 	catch(UnhandledAlertException e)
						 {
					 		//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(2000);
					  }
				 	else if (Checkname.equals("Court"))
					  {
				 		try
						  {
						  Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[8]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkCourtInsuff")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtInsuffRemark")).sendKeys("Not Sufficient data "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtSubmit_input")).click();
					  Thread.sleep(1500);
					  
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
					 catch(UnhandledAlertException e)
						  {
						//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(2000);
					  }
				 	else if (Checkname.equals("Criminal"))
					  {
				 		try
						 {
						  //Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[6]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkCriminalInsuff")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalInsuffRemark")).sendKeys("Not Sufficient data "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCriminalSubmit_input")).click();
					  Thread.sleep(1500);
					 
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
					 	catch(UnhandledAlertException e)
						  {
					 		//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(2000);
					  }
				 /*	else if (Checkname.equals("Drug & Medical"))
					  {
				 		 try
						  {
						 Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[9]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkDrugMedicalInsuff")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalInsuffRemark")).sendKeys("Not Sufficient data "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnDrugMedicalSaveSubmit_input")).click();
					  Thread.sleep(1500);
					 
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
						  }
				 		catch(UnhandledAlertException e)
						  {
							  //driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(3000);
					  }*/
				}
					 catch(NoSuchElementException e)
				{
						 continue;
				}					  
					  
					}
				//}
			 //////////////////////////////////Insuff_Clear////////////////////////////////////////////
		  try
		  {
			 Thread.sleep(1500);
			 driver.switchTo().defaultContent();			 
				 driver.findElement(By.id("imgHome")).click();
				Thread.sleep(5000);
		  }
		  catch(NoSuchElementException e)
		  {
			  System.out.println(e);
		  }
				driver.findElement(By.id("btnActions")).click();				 
			 Thread.sleep(3000);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
				Thread.sleep(500);
				driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[1]")).click();
				Thread.sleep(1500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlWorkflowType_Input")).click();
				Thread.sleep(500);
				driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[3]")).click();
				Thread.sleep(1000);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
				Thread.sleep(2000);
				 wait = driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).size();
					//System.out.println(wait);
					if(wait!=0)
					{
					Thread.sleep(1000);
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl04_btnClearInsuff")).click();
					}
					Thread.sleep(2500);
					for(j=0;j<=checksize;j++)
					{
					try
						{
						String check=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00__"+j+"']/td[29]")).getText();
						while(k<=checksize)
						{							
						if(check.equals("Data Entry")||check.equals("Data Entry After Insuff Clear"))
						{
							Thread.sleep(1000);
							String insuffcheck=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00__"+j+"']/td[17]")).getText();
							System.out.println("insuff Cleared check "+insuffcheck);
							if(k<10)
							{
							driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+k+"_chkInsuffComponentSelectCheckBox")).click();
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+k+"_txtInsuffClearComments")).sendKeys("insuff cleared "+insuffcheck);
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+k+"_btnUploadInsuffDocuments")).click();
							Thread.sleep(2500);
							String doc=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00__0']/td[6]")).getText();
							if(doc.isEmpty())
							{
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/test2.pdf");
							Thread.sleep(1500);
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_btnSubmitAddedDocument")).click();
							Thread.sleep(2000);
							driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();				
							}
							else
							driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
							Thread.sleep(650);								
							}
							else
							{
								driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl"+k+"_chkInsuffComponentSelectCheckBox")).click();
								driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl"+k+"_txtInsuffClearComments")).sendKeys("insuff cleared "+insuffcheck);
								driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl"+k+"_btnUploadInsuffDocuments")).click();
								Thread.sleep(2500);
								String doc=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00__0']/td[6]")).getText();
								if(doc.isEmpty())
								{
								driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/test3.pdf");
								Thread.sleep(1500);
								driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_btnSubmitAddedDocument")).click();
								Thread.sleep(2000);
								driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
								//Thread.sleep(650);	
								}
								else
									driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
									Thread.sleep(650);											
							}							
							}	
						k=k+2;
						break;
						}
						}						
						catch(NoSuchElementException e)
						{
							continue;
						}
					}
					Thread.sleep(500);
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_btnClear_input")).click();
					Thread.sleep(3500);
					 WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();	
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnStages_input")).click();					
		  }
		 
		}
		
	


