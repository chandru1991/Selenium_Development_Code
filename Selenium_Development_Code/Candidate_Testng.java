package Project_Flow;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Files.Excel_Read_Logins;
import Files.Propertyfile;
import Files.Text_Writer;
import Project_Path.Candidate_Homepage_path;
import Project_Path.Client_Homepage_path;
import Project_Path.Gmail_path;
import Project_Path.Loginpage_path;

public class Candidate_Testng {
	SoftAssert softAssert=new SoftAssert();
	@BeforeTest()
	public void beforeTest() throws Exception
	{
		new Propertyfile();
		Excel_Read_Logins.Candidate_Excel_Read_Logins(Propertyfile.candidate_excelfilepath,Propertyfile.sheetname, Propertyfile.columncount);
	}
	@BeforeMethod()
	public void beforeMethod_client() throws Exception
	{
		new Base_Candidate();		
	}
	@AfterMethod()
	public void afterMethod() throws InterruptedException
	{
		Driver.instance.close();
	}
	@Test(dataProvider="candidate_dataprovider",priority=1,enabled=true)
	public void candidateLogin(String username,String password,String alert) throws Exception
	{
		System.out.println("	-------------------Candidate Login Functionality-------------------");
		new Text_Writer("	-------------------Candidate Login Functionality-------------------");
		//WebDriverWait wait=new WebDriverWait(Driver.instance,20);
		String usernotavailable;
		//int unamesize=0,pwsize=0;
		Login.candidate_login(username,password);//new ClientMaster();
		Thread.sleep(2500);
		int findAvailable=Driver.instance.findElements(By.xpath(Candidate_Homepage_path.candidate_emailid_field)).size();
		//Driver.wait.until(ExpectedConditions.elementToBeClickable(Driver.instance.findElement(By.xpath(Homepage_path.homelink)))).isDisplayed();
		if(findAvailable==1)
		{
		//System.out.println(username+" - "+password+" = "+"Logged in Successfully");
			usernotavailable="Logged in Successfully.";
		}
		else
		{
			usernotavailable=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
		}
		//System.out.println(usernotavailable);
		softAssert.assertEquals(usernotavailable.trim(), alert.trim(), "	"+username+" - "+password+" = "+"Fail");
		if(usernotavailable.trim().equals(alert.trim()))
		{			
			System.out.println("	"+username+" - "+password+" = "+"Pass");
			new Text_Writer("	"+username+" - "+password+" = "+"Pass");
		}
		else
		{
			System.out.println("	"+username+" - "+password+" = "+"Fail");
			new Text_Writer("	"+username+" - "+password+" = "+"Fail");
		}
	}
	@Test(priority=2,enabled=false)
	public void forgotPassword_candidate() throws Exception
	{
		WebDriverWait wait=new WebDriverWait(Driver.instance,20);
		System.out.println("	-------------------Forgot Password Functionality-------------------");
		new Text_Writer("	-------------------Forgot Password Functionality-------------------");
		Forgot_Password.Candidate_Forgot_Password();
		Driver.instance.get(Propertyfile.mailurl);
		Driver.instance.findElement(By.id(Gmail_path.gusername)).sendKeys(Propertyfile.gid);
		Driver.instance.findElement(By.xpath(Gmail_path.gusernamenext)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Gmail_path.gpassword))).sendKeys(Propertyfile.gpassword);
		Driver.instance.findElement(By.xpath(Gmail_path.gpasswordnext)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id(Gmail_path.gsearch))).sendKeys(Propertyfile.gsubject,Keys.ENTER);
		Thread.sleep(1500);
		try
		{
		wait.until(ExpectedConditions.elementToBeClickable(By.className(Gmail_path.gsubjectclick))).click();
		Thread.sleep(3000);
		}catch(Exception e)
		{
		System.out.println("	"+"Mail Not Received"+e.getMessage());	
		new Text_Writer("	"+"Mail Not Received"+e.getMessage());	
		}
		softAssert.assertAll();
	}
	@DataProvider
	public Object[][] candidate_dataprovider()
	{
		int row=Excel_Read_Logins.candidate_rowcount;
		int col=Excel_Read_Logins.candidate_colncount;
		Object data[][]=new Object[row][col];
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<col;j++)
			{
				data[i][j]=Excel_Read_Logins.candidate_cell[i+1][j];
			}
		}
		return data;		
	}
	
	@BeforeTest(alwaysRun=true)
	public void afterTest()
	{
		//Driver.instance.quit();
	}


}
