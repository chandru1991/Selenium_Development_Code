package check360_flow;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Client_YTS 
{

	public static void main(String[] args) throws InterruptedException 
	{
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();
	      driver.manage().window().maximize();
	      driver.get("http://192.168.2.17:96");
	      driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
		  Thread.sleep(10000);
		  driver.findElement(By.id("btnActions")).click();
		  Thread.sleep(3000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[13]")).click();
		  Thread.sleep(2500);
		  String rno;		
		  rno=JOptionPane.showInputDialog("Enter Reference Number");
		  int opt;
		  do
		  {
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCaseRefNo")).clear();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCaseRefNo")).sendKeys(rno);
		      String option=JOptionPane.showInputDialog("Would you like to do?"+"\n"+"1.Register additional Component"+"\n"+"2.Remove Not applicable component"+"\n"+"3.View CIF"+"\n"+"4.Exit");
	   	      opt=Integer.parseInt(option);			
		  int i;
		  String text;
		  if(opt==1)
		  {
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlSearch_Input")).click();
			  Thread.sleep(680);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlSearch_DropDown']/div/ul/li[1]")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
			  Thread.sleep(3000);
			  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
			  {
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
				  
			  }
			  else
			  {
				  for(i=5;i<85;i++)
				  {
					  if(i<10)
					  {
						 try
						  {
						  if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl0"+i+"_lblComponentName")).isDisplayed())
						  {
					  text= driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl0"+i+"_lblComponentName")).getText();
					 System.out.println(text);
					 if(text.equals("Previous Address 1")||text.equals("Previous Address 2"))
					 {
					 		Thread.sleep(700);
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl0"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl0"+i+"_txtComments")).sendKeys("Addtional Check "+i);
					 	}
						  }
					  }
						  catch(NoSuchElementException e)
						  {
							  continue;
						  }
					  }
					  else 
					  {
						 try
						  {
						  if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_lblComponentName")).isDisplayed())
						  {
					  text= driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_lblComponentName")).getText();
					 System.out.println(text);
					 	if(text.equals("Previous Employment 3")||text.equals("Previous Employment 2"))
					 {
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Addtional Check "+i);
					 	}
						  
						  else if(text.equals("Reference 2")||text.equals("Reference 3"))
					 	{
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Addtional Check "+i);
					 
						  }
					  
					  else if(text.equals("Highest 3")||text.equals("Highest 4")||text.equals("PG!")||text.equals("UG2"))
					 	{
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Addtional Check "+i);
					 
						  }
					  
					  else if(text.equals("Pan Card")||text.equals("Driving License")||text.equals("ID Check 1")||text.equals("ID Check 2"))
					 	{
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Addtional Check "+i);
					 
						   }
					  else if(text.equals("Previous Address 1 Court Check")||text.equals("Previous Address 2 Court Check"))
					 {
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Addtional Check "+i);
					 	}
					  else if(text.equals("Panel4")||text.equals("Panel3")||text.equals("Panel2"))
					 	{
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Addtional Check "+i);
					 	 }					 
					  else if(text.equals("Previous Address 1 Criminal Check")||text.equals("Previous Address 2 Criminal Check"))
					 	{
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Addtional Check "+i);
					 	  }	  
					  
					  else if(text.equals("Credit Check 3")||text.equals("Credit Check 4"))
					 {
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Addtional Check "+i);
					 	}
						 
				  }
						
					  
					  }
					  catch(NoSuchElementException e)
					  {
						  continue;
					  }
					  
				  }
				  }
				  Thread.sleep(1500);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSave")).click();
				  Thread.sleep(1500);
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
				  Thread.sleep(3500);
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  
			  }
				  }
		  else if(opt==2)
		  {
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlSearch_Input")).click();
			  Thread.sleep(680);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlSearch_DropDown']/div/ul/li[2]")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
			  Thread.sleep(3000);
			  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
			  {
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
				  
			  }
			  else
			  {
				  for(i=5;i<85;i++)
				  {
					  if(i<10)
					  {
						 try
						  {
						  if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl0"+i+"_lblComponentName")).isDisplayed())
						  {
					  text= driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl0"+i+"_lblComponentName")).getText();
					 System.out.println(text);
					 if(text.equals("Previous Address 1")||text.equals("Previous Address 2"))
					 {
					 		Thread.sleep(700);
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl0"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl0"+i+"_txtComments")).sendKeys("Not Applicable Check "+i);
					 	}
						  }
					  }
						  catch(NoSuchElementException e)
						  {
							  continue;
						  }
					  }
					  else 
					  {
						 try
						  {
						  if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_lblComponentName")).isDisplayed())
						  {
					  text= driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_lblComponentName")).getText();
					 System.out.println(text);
					 	if(text.equals("Previous Employment 1")||text.equals("Previous Employment 2"))
					 {
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Not Applicable Check "+i);
					 	}
						  
						  else if(text.equals("Reference 2")||text.equals("Reference 1"))
					 	{
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Not Applicable Check "+i);
					 
						  }
					  
					  else if(text.equals("Highest 1")||text.equals("Highest 2")||text.equals("10th")||text.equals("UG1"))
					 	{
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Not Applicable Check "+i);
					 
						  }
					  
					  else if(text.equals("Pan Card")||text.equals("Driving License")||text.equals("ID Check 1")||text.equals("ID Check 2"))
					 	{
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Not Applicable Check "+i);
					 
						   }
					  else if(text.equals("Previous Address 1 Court Check")||text.equals("Previous Address 2 Court Check"))
					 {
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Not Applicable Check "+i);
					 	}
					  else if(text.equals("Panel1")||text.equals("Panel3")||text.equals("Panel2"))
					 	{
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Not Applicable Check "+i);
					 	 }					 
					  else if(text.equals("Previous Address 1 Criminal Check")||text.equals("Previous Address 2 Criminal Check"))
					 	{
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Not Applicable Check "+i);
					 	  }	  
					  
					  else if(text.equals("Credit Check 1")||text.equals("Credit Check 2"))
					 {
					 		driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_chkCheck")).click();
					 		Thread.sleep(650);
					 		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseEdit_ctl00_ctl"+i+"_txtComments")).sendKeys("Not Applicable Check "+i);
					 	}
						 
				  }
						
					  
					  }
					  catch(NoSuchElementException e)
					  {
						  continue;
					  }
					  
				  }
				  }
				  Thread.sleep(1500);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSave")).click();
				  Thread.sleep(1500);
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
				  Thread.sleep(3500);
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  
			  }
				  }
		  else if(opt==3)
		  {
			 driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlSearch_Input")).click();
			  Thread.sleep(680);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlSearch_DropDown']/div/ul/li[2]")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
			  Thread.sleep(3000);
			  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
			  {
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
				  
			  }
			  else
			  {
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_lnkViewCIF")).click();
			  Thread.sleep(1500);
			  }
		  }
		 
			 
		  }while(opt<4);
		 Thread.sleep(2500); 
		  driver.findElement(By.id("ctl00_imgHome")).click();
		  
	}
}