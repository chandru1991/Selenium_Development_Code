package Checks360;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Bulk_VRInitiation extends txt_write {
	WebDriverWait wait=new WebDriverWait(Driver.instance,50);
	private String initiationsearch="Button3";
	public String totalrecords="/html/body/form/div[5]/div[3]/div/div/div/div/div[2]/table/thead/tr/td[2]/span[1]";
	String employername[]=new String[20];
	private String bulkinitiatebutton="btnBulkInitiate";
	boolean bool=false;
	int buttonvisible;
	String text[]=new String[500];
	String refno[]=new String[20];
	String refnopopup[]=new String[20];
	String componentname[]=new String[20];
	int count,rowcount=0,refcount,matchedrefno;
	String TOmail=".//*[@id='mailHeader']/p[1]/div/input";
	String CCmail=".//*[@id='mailHeader']/p[2]/div/input";
	String sendmail=".//*[@id='text']/div[2]/div[3]/button[1]";
	String closealert="html/body/div[3]/div/div/table/tbody/tr[1]/td/button";
	String employer;
	static String initialstatus=".//*[@id='srcpnl']/table/tbody/tr[5]/td[2]/select";
	static String Componentselect=".//*[@id='srcpnl']/table/tbody/tr[2]/td[10]/select";
	List<String>  list= new ArrayList<String>();
	Set<String> hashlist = new HashSet<>();
	String employernames[];
	String reportcommentpopupclose="ModalFooterClose";
	String reportcomment="ctl00_ContentPlaceHolder1_btnEmploymentAddComments";
	String VRinitiatedropdown="/html/body/form/div[5]/div[3]/div/div/div/div/div[1]/table/tbody/tr[2]/td[8]/select";
	static String casehome="imgHome";
	int refsize=90;
	int counts;
	public void BulkInitiation() throws Exception
	{
		//text("***************Bulk VR Initiation***************");
		new Select(Driver.instance.findElement(By.id(Case_Registration.stagedropdown))).selectByValue("6");
		Thread.sleep(2000);
		new Select(Driver.instance.findElement(By.xpath(VRinitiatedropdown))).selectByValue("2");
		Thread.sleep(2000);
		Driver.instance.findElement(By.id(initiationsearch)).click();
		Thread.sleep(2000);
		String verificationsize=Driver.instance.findElement(By.xpath(totalrecords)).getText();
		int totalsize=Integer.parseInt(verificationsize);
		//System.out.println(totalsize);
		
		for(int row=1;row<=totalsize;row++)
		{
			employername[row]=Driver.instance.findElement(By.xpath("/html/body/form/div[5]/div[3]/div/div/div/div/div[2]/div/div/table/tbody/tr["+row+"]/td[21]")).getText();
		}
		for(String empvalue:employername)
		{
			if(empvalue != null && empvalue.length() > 0) {
		          list.add(empvalue);
		          hashlist.addAll(list);
		          list.clear();
		          list.addAll(hashlist);
		       }
		}
		employernames = list.toArray(new String[list.size()]);
		employer=(String) JOptionPane.showInputDialog(new JFrame(), "Choose a Employer to do Bulk VR initiation","Selection", JOptionPane.QUESTION_MESSAGE, null,employernames,employernames[0]);
		//System.out.println("employer="+employer);
		if(!Driver.instance.findElement(By.id(bulkinitiatebutton)).isDisplayed())
		{
			text("Bulk VR Initiation button not shown - working fine");
			bool=true;
		}
		Assert.assertTrue("Should not show bulk VR Initiation button before selection",bool);
		bool=false;
		for(int row=1;row<=totalsize;row++)
		{
			for(int nextrow=1;nextrow<=totalsize;nextrow++)
			{
				if(employer.trim().equals(employername[row].trim()))
				{
				if(row!=nextrow)
				{
				if(employername[row].trim().equals(employername[nextrow].trim()))
				{				
					bool=true;
					if(!Driver.instance.findElement(By.xpath("/html/body/form/div[5]/div[3]/div/div/div/div/div[2]/div/div/table/tbody/tr["+row+"]/td[1]/input")).isSelected())
					{
						Driver.instance.findElement(By.xpath("/html/body/form/div[5]/div[3]/div/div/div/div/div[2]/div/div/table/tbody/tr["+row+"]/td[1]/input")).click();
						refno[rowcount]=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+row+"]/td[2]/span")).getText();
						//builder.moveToElement(Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+row+"]/td[2]/span"))).perform();
						componentname[rowcount]=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+row+"]")).getAttribute("title");
						//System.out.println("Stage="+rowcount+"_"+refno[rowcount]);
						//System.out.println("Component="+rowcount+"_"+componentname[rowcount]);
						rowcount++;count++;
					}										
				}
				}
				}
			}
		}
			Assert.assertTrue("Bulk VR Initiation Not Applied for Single Employer",bool);
		System.out.println("count="+count);
		bool=false;
		if(Driver.instance.findElement(By.id(bulkinitiatebutton)).isDisplayed())
		{
			text("Bulk VR Initiation button shown - working fine");
			bool=true;
		}
		Assert.assertTrue("Should show bulk VR Initiation button After selection",bool);
		Driver.instance.findElement(By.id(bulkinitiatebutton)).click();
		Thread.sleep(3000);
		int refnopopupsize=Driver.instance.findElements(By.xpath(".//*[@id='bodyContent']/table[2]/tbody/tr/td[2]")).size();
		System.out.println("popupsize"+refnopopupsize);
		if(refnopopupsize!=0)
		{
		for(int refno=3;refno<=refnopopupsize+1;refno++)
		{
		refnopopup[refcount]=Driver.instance.findElement(By.xpath(".//*[@id='bodyContent']/table[2]/tbody/tr["+refno+"]/td[2]")).getText();
		refcount++;
		System.out.println("popup="+refnopopup[refcount]);
		}
		System.out.println("refcount="+refnopopup[0]+"\t"+refnopopup[1]+"\t"+refnopopup[2]);
		}
		for(int refmatch=0;refmatch<count;refmatch++)
		{
			for(int refmatchs=0;refmatchs<count;refmatchs++)
			{
				if(refnopopup[refmatch].trim().equals(refno[refmatchs].trim()))
				{
					matchedrefno++;
				}
		}
		}
		System.out.println("matchedrefno="+matchedrefno);
		bool=false;
		if(matchedrefno==count)
		{
			text("Selected case's are showing in Bulk VRinitiation Template");
			bool=true;
		}
		Assert.assertTrue("Selected case's are not showing in Bulk VRinitiation Template", bool);
		Driver.instance.findElement(By.xpath(TOmail)).sendKeys("ramachandran@kadambatechnologies.com");
		Driver.instance.findElement(By.xpath(CCmail)).sendKeys("kumaresan@kadambatechnologies.com");
		Driver.instance.findElement(By.xpath(sendmail)).click();		
		Thread.sleep(10000);
		Driver.instance.findElement(By.xpath(closealert)).click();
		Thread.sleep(1500);
		new Select(Driver.instance.findElement(By.xpath(initialstatus))).selectByValue("1");
		Thread.sleep(1000);
		Driver.instance.findElement(By.id(initiationsearch)).click();
		Thread.sleep(1500);
		System.out.println("Finalrowcount="+rowcount);
		for(int refnosearch=0;refnosearch<rowcount;refnosearch++)
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(initiationsearch)));
			Driver.instance.findElement(By.id(Case_Registration.workcaseref)).clear();
			Driver.instance.findElement(By.id(Case_Registration.workcaseref)).sendKeys(refno[refnosearch]);
			Thread.sleep(1500);
			new Select(Driver.instance.findElement(By.xpath(Componentselect))).selectByVisibleText(componentname[refnosearch]);
			Driver.instance.findElement(By.id(initiationsearch)).click();			
			Thread.sleep(5000);
			int confirmationcase=Driver.instance.findElements(By.xpath(Case_Search.DBref)).size();
			if(confirmationcase!=0)
			{
				Driver.instance.findElement(By.xpath(Case_Search.DBref)).click();
				Thread.sleep(6000);
				WebElement element = Driver.instance.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[3]/iframe")); 
				Driver.instance.switchTo().frame(element);
				  Thread.sleep(3000);
				Driver.instance.findElement(By.id(reportcomment)).click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(reportcommentpopupclose)));
				Driver.instance.findElement(By.partialLinkText("VRBulkmail")).click();
				Thread.sleep(10000);
				Driver.instance.switchTo().frame(1);
				Thread.sleep(10000);
				for(int refs=2;refs<refsize;refs++)
				{
				text[counts]=Driver.instance.findElement(By.xpath(".//*[@id='pageContainer1']/xhtml:div[2]/xhtml:div["+refs+"]")).getText();
				System.out.println("Text="+text[counts]);
				counts++;
				}
				int checkcount = 0;
				for(int size=0;size<refcount;size++)
				{
					for(int getsize=0;getsize<refsize;getsize++)
					{
						try
						{
						if(text[getsize].trim().equals(refnopopup[size].trim()))
						{
							checkcount++;
						}
					}catch(Exception e)
					{
						continue;
					}
					}
				}
				if(checkcount==count)
				{
					text("All case Present in the PDF for Reference Number = "+refno[refnosearch]);
					Driver.instance.switchTo().parentFrame();
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(reportcommentpopupclose))).click();
					Driver.instance.switchTo().defaultContent();
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(casehome))).click();					
				}
				else
				{
					text("Case Not Present in PDF");
				}
				}			
		}
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Logout.Home_sp_logout))).click();		
	}

}
