package Checks360;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base extends txt_write {
	//static WebDriverWait wait=new WebDriverWait(Driver.instance,50);
	static Properties property;
	String browser;
	String chromedriverpath;	
	static Boolean bool=false;	
	static String url;
	static String documentuploadpath1,documentuploadpath2,documentuploadpath3,documentuploadpath4;
	protected static String client,project;
	static String Caseref=".//*[@id='grdTaskList']/tbody/tr[1]/td[2]/span";
	public Login openbrowser() throws Exception
		{	 
			try
			{
		property=new Properties();
		property.load(new FileInputStream("/home/pyr/Script_New/webconfig"));
		url=property.getProperty("url");
		browser=property.getProperty("browser");
		chromedriverpath=property.getProperty("chromedriverpath");
		documentuploadpath1=property.getProperty("documentuploadpath1");
		documentuploadpath2=property.getProperty("documentuploadpath2");
		documentuploadpath3=property.getProperty("documentuploadpath3");
		documentuploadpath4=property.getProperty("documentuploadpath4");
		client=property.getProperty("Client");
		project=property.getProperty("Project");
		switch(browser)
		{
		case "chrome":
			System.setProperty("webdriver.chrome.driver", chromedriverpath);
			Driver.instance=new ChromeDriver();
			Driver.instance.manage().window().maximize();
			Driver.instance.get(url);
			bool=true;
			break;
		
		case "firefox":
			//Driver.instance=new FirefoxDriver();
			FirefoxProfile firefoxProfile = new FirefoxProfile();
			firefoxProfile.setPreference("xpinstall.signatures.required", false); 
			Driver.instance = new FirefoxDriver(firefoxProfile);
			Driver.instance.manage().window().maximize();
			Driver.instance.get(url);
			//Driver.instance.navigate().to(url);
			bool=true;
			break;
			
		default:
			JOptionPane.showMessageDialog(null, "Browser Not Available","Alert", 0);
			
		}
			}catch(Exception e)
			{
				System.out.println("issue in browser"+"=="+e);
				text("issue in browser");				
			}
			return new Login();
}
}