package Checks360;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.jws.soap.SOAPBinding.Style;

import org.apache.poi.ss.usermodel.CellStyle;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Filename_Restriction {
	static WebDriverWait wait=new WebDriverWait(Driver.instance,50);
	String master=".//*[@id='ctl00_RadMenu1']/ul/li/a/span";
	String clientmaster=".//*[@id='ctl00_RadMenu1']/ul/li/div/ul/li[9]/a/span";
	String addnewclient="ctl00_ContentPlaceHolder1_btnAdd";
	String usermanual="ctl00_ContentPlaceHolder1_rdwAdd_C_FileUploadUserManualfile0";
	String loa="ctl00_ContentPlaceHolder1_rdwAdd_C_FileUploadLOAfile0";
	String creditform="ctl00_ContentPlaceHolder1_rdwAdd_C_FileUploadLOAfile0";
	String cancel="ctl00_ContentPlaceHolder1_rdwAdd_C_btnCancel";
	int getsize=0;
	public void restriction() throws Exception
	{
		Thread.sleep(6000);
		Driver.instance.findElement(By.xpath(master)).click();
		Thread.sleep(1000);
		Driver.instance.findElement(By.xpath(clientmaster)).click();
		Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(addnewclient))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(cancel)));
		Driver.instance.findElement(By.id(usermanual)).sendKeys("/home/pyr/Desktop/Live_Attachments/onenote table of contents_ onetoc2_onenote table of contents onetoc2_onenote table of contents .pdf");
		Thread.sleep(2000);		
		returnvalue();
		if(getsize==1)
		{
			txt_write.text("File name restriction is working fine in Client Master for Usermanual"); 
			Excel_write.inputExcel("Upload User manual", "Pass");
			Driver.instance.findElement(By.xpath("//span[contains(.,'OK')]")).click();
		}
		else
		{
			txt_write.text("File name restriction is not working fine in Client Master for Usermanual");
			Excel_write.inputExcel("Upload User manual", "Fail");
		}
		Thread.sleep(1000);
		Driver.instance.findElement(By.id(loa)).sendKeys("/home/pyr/Desktop/Live_Attachments/onenote table of contents_ onetoc2_onenote table of contents onetoc2_onenote table of contents .pdf");
		Thread.sleep(2000);		
		returnvalue();
		if(getsize==2)
		{
			txt_write.text("File name restriction is working fine in Client Master for LOA");
			Excel_write.inputExcel("Upload LOA", "Pass");
			Driver.instance.findElement(By.xpath("//span[contains(.,'OK')]")).click();
		}
		else
		{
			txt_write.text("File name restriction is not working fine in Client Master for LOA");
			Excel_write.inputExcel("Upload LOA", "Fail");
		}
		Thread.sleep(1000);
		Driver.instance.findElement(By.id(creditform)).sendKeys("/home/pyr/Desktop/Live_Attachments/onenote table of contents_ onetoc2_onenote table of contents onetoc2_onenote table of contents .pdf");
		Thread.sleep(2000);		
		returnvalue();
		if(getsize==3)
		{
			txt_write.text("File name restriction is working fine in Client Master for Credit Form");
			Excel_write.inputExcel("Upload Credit Form", "Pass");
			Driver.instance.findElement(By.xpath("//span[contains(.,'OK')]")).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(cancel))).click();
			Thread.sleep(2000);
			//wait.until(ExpectedConditions.elementToBeClickable(By.id(Logout.Home_sp_logout))).click();
			//Driver.instance.close();
		}
		else
		{
			txt_write.text("File name restriction is not working fine in Client Master for Credit Form");
			Excel_write.inputExcel("Upload Credit Form", "Fail");
		}		
	}
	public void returnvalue()
	{
		if((Driver.instance.findElement(By.xpath("//span[contains(.,'OK')]")).isDisplayed()) && (!Driver.instance.findElement(By.xpath("//span[contains(.,'Cancel')]")).isDisplayed()))
		{
			getsize++;
		}
	}
}
