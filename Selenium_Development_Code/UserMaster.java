package Project_Flow;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import Files.Propertyfile;
import Files.Text_Writer;
import Project_Path.SP_Homepage_path;
import Project_Path.UserMaster_Path;

public class UserMaster {
	Boolean bool;
	public UserMaster() throws Exception
	{
		WebDriverWait wait=new WebDriverWait(Driver.instance,20);
		SoftAssert asert=new SoftAssert();
		try
		{
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(SP_Homepage_path.masters))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(UserMaster_Path.user))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(UserMaster_Path.adduserbutton)));
		String  usermasterheadername=Driver.instance.findElement(By.xpath(SP_Homepage_path.allstagename)).getText();
		String usermasteruserlists=Driver.instance.findElement(By.xpath(UserMaster_Path.usermaster_userlist)).getText();
		bool=false;
		bool=usermasterheadername.trim().equals(Propertyfile.usermasterheader.trim());
		asert.assertTrue(bool, "User Header name is not same");
		if(bool==true)
		{
			System.out.println("User Header name is same");
			new Text_Writer("User Header name is same");
		}
		else
		{
			new Text_Writer("User Header name is not same");
		}
		bool=false;
		bool=usermasteruserlists.trim().equals(Propertyfile.usermasterheader.trim());
		asert.assertTrue(bool, "User List name is not same");
		if(bool==true)
		{
			System.out.println("User List name is same");
			new Text_Writer("User List name is same");
		}
		else
		{
			new Text_Writer("User List name is not same");
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(UserMaster_Path.adduserbutton))).click();
		Thread.sleep(1000);
		String createuser_header=Driver.instance.findElement(By.xpath(UserMaster_Path.usermaster_createuserheader)).getText();
		String createuserlists=Driver.instance.findElement(By.xpath(UserMaster_Path.usermaster_userlist)).getText();
		String create_user=Driver.instance.findElement(By.xpath(UserMaster_Path.usermaster_createuser)).getText();
		bool=false;
		bool=createuser_header.equals(Propertyfile.usermastercreate.trim());
		asert.assertTrue(bool,"Create User header is not same in Create User Page");
		if(bool==true)
		{
			System.out.println("Create User Header name is same in Create User Page");
			new Text_Writer("Create User Header name is same in Create User Page");
		}
		else
		{
			new Text_Writer("Create User Header name is not same in Create User Page");
		}
		bool=false;
		bool=createuserlists.trim().equals(Propertyfile.usermasterheader.trim());
		asert.assertTrue(bool, "User List name is not same in Create User Page");
		if(bool==true)
		{
			System.out.println("User List name is same in Create User Page");
			new Text_Writer("User List name is same in Create User Page");
		}
		else
		{
			new Text_Writer("User List name is not same in Create User Page");
		}
		bool=create_user.trim().equals(Propertyfile.usermastercreate.trim());
		asert.assertTrue(bool,"Create user name is not same in Create User Page");
		if(bool==true)
		{
			System.out.println("Create user name is same in Create User Page");
			new Text_Writer("Create user name is same in Create User Page");
		}
		else
		{
			new Text_Writer("Create user name is not same in Create User Page");
		}
		Driver.instance.findElement(By.xpath(UserMaster_Path.firstname)).sendKeys();
		}
		catch(Exception e)
		{
			System.out.println("ERROR: "+e.getMessage());
			new Text_Writer("ERROR: "+e.getMessage());
		}
		
	}

}
