package Checks360;

import org.testng.annotations.Test;

import com.beust.jcommander.Parameter;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
public class TestNG {
	int count=0;
	static DateFormat dateFormat;
	static Date date;
	static String currentdate;
	public static void datefun()
	{
		  dateFormat= new SimpleDateFormat("dd_MM_yyyy HH:mm:ss a");
		  date = new Date();
		  currentdate= dateFormat.format(date);
	}
	
	@BeforeTest
//	@Parameters({ "browser" })	
	  public void beforeTest() throws Exception {
		  pageInstance.Base_class().openbrowser();		  
	  }
  @Test(priority =1,enabled = false/*invocationCount=400*/)//false
  public void normal() throws Exception {
	  datefun();
	  Excel_write.createExcel("Normal Case Registration","Normal Case Registration","Result");
	  txt_write.text("***************Case Registration with Normal Flow***************"+"\t"+currentdate);
	  pageInstance.Excelread().Caseregn();
	  pageInstance.Log()
	  			  .login()
	              .selectcomponent()
	              .DE_logout();
	  Excel_write.writeExcel();
	 /* if(count==300)
	  {
		  Driver.instance.quit();
	  }
	  else
	  {
	  System.out.println(count++);
	  normal();	  
	  count++;
  }*/
  }
  @Test(priority =2,enabled = false)//true
  public void insuff() throws Exception {
	  datefun();
	  txt_write.text("***************Case Registration with Insuff Flow***************"+"\t"+currentdate);	  
	  pageInstance.Log()
	  			  .login()
	              .insuffcheck()
	              .Home_logout();
  }
  @Test(priority =3,enabled = false)
	  public void CEP() throws Exception {
	  datefun();
	  txt_write.text("***************Case Registration with CEP Flow***************"+"\t"+currentdate);	  
		  pageInstance.Log()
		  			  .login()
		              .CEP()
		              .Home_logout();	  
  }
  @Test(priority =4,enabled = false)
  public void insuffCEP() throws Exception {
	  datefun();
	  txt_write.text("***************Case Registration with CEP & Insuff Flow***************"+"\t"+currentdate);  
	  pageInstance.Log()
	  			  .login()
	              .insuffcep()
	              .Home_logout();	
  }
  @Test(priority =5,enabled = false)
  public void NA() throws Exception {
	  datefun();
	  txt_write.text("***************Case Registration with Not Applicable Flow***************"+"\t"+currentdate);
	  pageInstance.Log()
		          .login()
                  .NA()
                  .Home_logout();	
 	  
   }
  @Test(priority =6,enabled = false)
 public void CS_search() throws Exception {
	  datefun();
	  txt_write.text("***************Case Search Functionality****************"+"\t"+currentdate);
	  pageInstance.Log().login();
	  pageInstance.CS().case_search();
	  pageInstance.Lout().Home_logout();
	  
  }
  @Test(priority =7,enabled = false)
  public void Re_assign() throws Exception {
	 // txt_write.text("***************Case Registration with Normal Flow***************"+"\t"+currentdate);
 	  pageInstance.Log().login();
 	  pageInstance.RS().reassignment();
 	  //pageInstance.Lout().Home_logout(); 	  
   }
  
  @Test(priority =8,enabled = false)
  public void Reference_find() throws Exception {
	  //txt_write.text("***************Case Registration with Normal Flow***************"+"\t"+currentdate);
 	  pageInstance.Log().login();
 	  pageInstance.reference().ref(); 
   }
  
  @Test(priority =8,enabled = false)
  public void Select_allrefno() throws Exception {
	  //txt_write.text("***************Case Registration with Normal Flow***************"+"\t"+currentdate);
 	  pageInstance.Log().login();
 	  pageInstance.screenshotall().ref();
   }

  @Test(priority =9,enabled = false)
  public void Bulk_VRInitiation() throws Exception {
	  datefun();
	  txt_write.text("***************Bulk VR Initiation***************"+"\t"+currentdate);
	  pageInstance.Log().login();
	  pageInstance.BulkVR().BulkInitiation();
  	}
  @Test(priority =10,enabled = false)
  public void Filename_Restriction() throws Exception {
	  datefun();
	  Excel_write.createExcel("Filename_Restriction","File Name Restriction","Result");
	  txt_write.text("***************File name Restriction***************"+"\t"+currentdate);
	  pageInstance.Log().login();
	  pageInstance.filter().restriction();
	  Excel_write.writeExcel();
	  pageInstance.Lout().Home_logout();
	  
  	}
  @Test(priority =11,enabled = false)
  public void Mastermis() throws Exception {
	  datefun();
	  Excel_write.createExcel("Mastermis","Master MIS","Result");
	  txt_write.text("***************Master MIS***************"+"\t"+currentdate);
	  pageInstance.Log().login();
	  pageInstance.mastermis().mis();
	  Excel_write.writeExcel();
	  pageInstance.Lout().Home_logout();
  	}
  @Test(priority =12,enabled = false)
  public void Report_Check() throws Exception {
	  datefun();
	  Excel_write.createExcel("Final_Report","Report","Result");
	  txt_write.text("***************Final_Report***************"+"\t"+currentdate);
	  pageInstance.Log().login();
	  pageInstance.reportcheck().report();
	  Excel_write.writeExcel();
	 // pageInstance.Lout().Home_logout();
  	}
  @Test(priority =13,enabled = false)
  public void casetracker_advancesearch() throws Exception {
	  datefun();
	  Excel_write.createExcel("Case_Tracker_Advance Search","CaseTracker","Result");
	  txt_write.text("***************Case_Tracker_Advance Search***************"+"\t"+currentdate);
	  pageInstance.Log().login();
	  pageInstance.casetracker().opencasetracker();
	  pageInstance.casetracker().advancesearch();
	  Excel_write.writeExcel();
	 pageInstance.Lout().Home_logout();
  	}
  @Test(priority =14,enabled = true)
  public void casetracker_search() throws Exception {
	  datefun();
	  Excel_write.createExcel("Case_Tracker_Normal Search","CaseTracker","Result");
	  txt_write.text("***************Case_Tracker_Normal Search***************"+"\t"+currentdate);
	  pageInstance.Log().login();
	  pageInstance.casetracker().opencasetracker();
	  pageInstance.casetracker().search();
	  Excel_write.writeExcel();
	  pageInstance.Lout().Home_logout();
  	}
  @AfterTest
  public void afterTest() {
	  Driver.instance.quit();
  }

}
