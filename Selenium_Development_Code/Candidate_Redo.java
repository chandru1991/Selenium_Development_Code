package check360_flow;

import java.util.concurrent.TimeoutException;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Candidate_Redo {
	static WebDriver driver;
	static String Checkname;
	public static void main(String[] args) throws InterruptedException, TimeoutException {
		// TODO Auto-generated method stub
		driver = new FirefoxDriver();
		
		//System.setProperty("webdriver.chrome.driver", "/home/ubuntu/chromedriver");
		//WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		WebElement sd;
		driver.get("http://192.168.2.17:96");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
		Thread.sleep(7000);
		String rno=JOptionPane.showInputDialog("Enter Reference number:\n");
		//String redo=JOptionPane.showInputDialog("Choose in Which stage to rise REDO"+"\n"+"1.DEQC"+"\n"+"2.Report Generation");
		//int stage=Integer.parseInt(redo);
		WebElement w1= driver.findElement(By.id("ddlAct"));	 
		/*if(stage==1)
		{
		Select s= new Select(w1);
		s.selectByValue("4");
		Thread.sleep(1000);
		}*/		
			Select s= new Select(w1);
			s.selectByValue("8");
			Thread.sleep(1000);		
		WebElement w2= driver.findElement(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[4]/select"));
		 Select s1=new Select(w2);
		 s1.selectByValue("2");
		 Thread.sleep(2500);
		 driver.findElement(By.id("txtCaserefNo")).clear();
			driver.findElement(By.id("txtCaserefNo")).sendKeys(rno);
			driver.findElement(By.id("btnsearch")).click();
			Thread.sleep(5000);
			
			  int wait;
			  int TabCheck;
			  int dcc = 0;
			  String CreditID; 
			 wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).size();
			//System.out.println(wait);
			if(wait!=0)
			{
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).click();
			}
			Thread.sleep(5500);
			 TabCheck = driver.findElements(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).size();
			 System.out.println("No of Checks in the Case:"+TabCheck);	
			 for(int tc=1;tc<=TabCheck;tc++)
			 {
				 try
				 {
				 driver.switchTo().defaultContent();
				 	Thread.sleep(4000);
				 	  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).getText();
				 	  System.out.println("Checkname"+Checkname);
				 	 if (Checkname.equals("Address"))
					  {
				 		 try
						  {
						  //Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
						 WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div/iframe")); 
						  driver.switchTo().frame(element);
						  Thread.sleep(3000);
						  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkAddressRedo")).click();
						  Thread.sleep(2000);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressRedoRemark")).sendKeys("Incorrect "+Checkname);
						  Thread.sleep(1000);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit_input")).click();
						  Thread.sleep(1500);
						 
						  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
						  {
						  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
						  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
						  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
						  }
						  else
						  {
							  Thread.sleep(2000);
						  driver.navigate().refresh(); 
						  tc--;
						  }
						  }
						  catch(UnhandledAlertException e)
						  {
							//driver.switchTo().alert().accept();
					 			continue;
						 }
						  Thread.sleep(3000);
					  }			
				 	 else if (Checkname.equals("Education"))
					  {
				 		 try
						  {
						  //Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[2]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkEducationRedo")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationRedoRemarks")).sendKeys("Incorrect "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveSubmit_input")).click();
					  Thread.sleep(1500);
					 
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
				 	catch(UnhandledAlertException e)
					  {
				 		//driver.switchTo().alert().accept();
			 			continue;
					 }
					  Thread.sleep(3000);
					  }
				 	 else if (Checkname.equals("Employment"))
					  {
				 		try
						 {
						  Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[3]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkEmploymentRedo")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentRedoRemarks")).sendKeys("Incorrect "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSubmit_input")).click();
					  Thread.sleep(1500);
					  
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
					 	catch(UnhandledAlertException e)
						 {
					 		//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(3000);
					  }
				 	else if (Checkname.equals("ID"))
					  {
				 		 try
						  {
						  //Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[10]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkIdRedo")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdRedoMarks")).sendKeys("Incorrect "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveSubmit_input")).click();
					  Thread.sleep(1500);
					 
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					 }
					 catch(UnhandledAlertException e)
						 {
						//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(3000);
					  }
				 	 else if (Checkname.equals("Reference"))
					  {
				 		try
						 {
						  //Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[4]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_CheckRefReportRedo")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportRedo")).sendKeys("Incorrect "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
					  Thread.sleep(1500);
					  
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
					 catch(UnhandledAlertException e)
						  {
						//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(3000);
					  }
				 	else if (Checkname.equals("DataBase"))
					  {
				 		 try
						  {
						 // Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[5]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkDataBaseRedo")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDataBaseRedoComments")).sendKeys("Incorrect "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnDataBaseSaveSubmit_input")).click();
					  Thread.sleep(1500);	
					 
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  }					  
					  }
					 	catch(UnhandledAlertException e)
						  {
					 		//driver.switchTo().alert().accept();
				 			continue;
						  }
					  tc--;
					  Thread.sleep(3000);
					  }
				 	else if (Checkname.equals("Credit"))
					  {
				 		try
						 {
						  //Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[7]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkcreditRedo")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditRedoMarks")).sendKeys("Incorrect "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditSaveSubmit_input")).click();
					  Thread.sleep(1500);
					  
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
					 	catch(UnhandledAlertException e)
						 {
					 		//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(3000);
					  }
				 	else if (Checkname.equals("Court"))
					  {
				 		try
						  {
						  Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[8]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkCourtRedo")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtRedoRemark")).sendKeys("Incorrect "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtSubmit_input")).click();
					  Thread.sleep(1500);
					  
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
					 catch(UnhandledAlertException e)
						  {
						//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(3000);
					  }
				 	else if (Checkname.equals("Criminal"))
					  {
				 		try
						 {
						  //Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[6]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkCriminalRedo")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalRedoRemark")).sendKeys("Incorrect "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCriminalSubmit_input")).click();
					  Thread.sleep(1500);
					 
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
					 	catch(UnhandledAlertException e)
						  {
					 		//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(3000);
					  }
				 	else if (Checkname.equals("Drug & Medical"))
					  {
				 		 try
						  {
						 Thread.sleep(2000);
						  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
						 Thread.sleep(1500);
					  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[9]/iframe")); 
					  driver.switchTo().frame(element);
					  Thread.sleep(3000);
					  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkDrugMedicalRedo")).click();
					  Thread.sleep(2000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalRedoComments")).sendKeys("Incorrect "+Checkname);
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnDrugMedicalSaveSubmit_input")).click();
					  Thread.sleep(1500);
					 
					  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
					  }
					  else
					  {
						  Thread.sleep(2000);
					  driver.navigate().refresh(); 
					  tc--;
					  }
					  }
					 	catch(UnhandledAlertException e)
						  {
					 		//driver.switchTo().alert().accept();
				 			continue;
						  }
					  Thread.sleep(3000);
					  }
				 }
				 	 catch(NoSuchElementException e)
				 	 {
				 		 continue;
				 	 }
					  }	 
			 driver.switchTo().defaultContent();
			 Thread.sleep(2000);
			 driver.findElement(By.id("imgHome")).click();
			 Thread.sleep(3000);
	}
}




			/* if(stage==1)
			 {
				 Select s= new Select(w1);
					s.selectByValue("1");
					Thread.sleep(1500);
					 Select s2=new Select(w2);
					 s2.selectByValue("1");
					 Thread.sleep(2500);
					 driver.findElement(By.id("txtCaserefNo")).clear();
						driver.findElement(By.id("txtCaserefNo")).sendKeys(rno);
						driver.findElement(By.id("btnsearch")).click();
						Thread.sleep(5000);
						wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).size();
						//System.out.println(wait);
						if(wait!=0)
						{
						Thread.sleep(1000);
						driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).click();
						}	
					 
			 }
			 else if(stage==2)
			 {
				 Select s= new Select(w1);
					s.selectByValue("5");
					Thread.sleep(1500);
					 Select s2=new Select(w2);
					 s2.selectByValue("1");
					 Thread.sleep(2500);
					 driver.findElement(By.id("txtCaserefNo")).clear();
						driver.findElement(By.id("txtCaserefNo")).sendKeys(rno);
						driver.findElement(By.id("btnsearch")).click();
						Thread.sleep(5000);
						wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).size();
						//System.out.println(wait);
						if(wait!=0)
						{
						Thread.sleep(1000);
						driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).click();
						}	
			 }*/
	