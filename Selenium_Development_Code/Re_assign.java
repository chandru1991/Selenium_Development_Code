package Checks360;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class Re_assign extends Case_Registration
	{
	String refno,uname,reassignname;
	String reassignid=".//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[6]";
	String reassignbutton="ctl00_ContentPlaceHolder1_btnReassign_input";
	String Userselect="ctl00_ContentPlaceHolder1_ddlUsers_Arrow";
	String Reassignsubmit="ctl00_ContentPlaceHolder1_rdwCaseAssignment_C_btnsubmit_input";
	String reassigncheckbox="ctl00_ContentPlaceHolder1_rdwCaseAssignment_C_grdCaseAssignmentResource_ctl00_ctl04_SelectedEmployeeSelectCheckBox";
	String Statesdropdown="ctl00_ContentPlaceHolder1_ddlStates_Arrow";
	String statesdata=".//*[@id='ctl00_ContentPlaceHolder1_ddlStates_DropDown']/div/ul/li[contains(text(),'Data Entry')]";
	String closereassign="ctl00_ContentPlaceHolder1_rdwCaseAssignment_C_ButtonCancel_input";
	int casesize=20;
		public void reassignment() throws InterruptedException
		{
		  wait.until(ExpectedConditions.elementToBeClickable(By.id(action)));
		  new Select(Driver.instance.findElement(By.id(stagedropdown))).selectByValue("1");
		  Thread.sleep(5000);
		  for(int count=1;count<=casesize;count++)
		  {
			  String casestatus=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+count+"]/td[12]")).getText();
			  refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+count+"]/td[2]/span")).getText();
			  uname=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+count+"]/td[11]/span")).getText();
			  if(casestatus.trim().equals("WIP"))
			  {
				  break;
			  }
		  }
		  System.out.println(refno);
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(action))).click();
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(workstage)));
			  Driver.instance.findElement(By.id(actionstage)).click();
			  Thread.sleep(1000);
			  Driver.instance.findElement(By.xpath(reassignid)).click();
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(reassignbutton)));
			  Driver.instance.findElement(By.id(actioncaserefsearch)).sendKeys(refno);
			  WebElement userid=Driver.instance.findElement(By.id("ctl00_lblUsername"));
			  //String uname=userid.getText().replace("Welcome ","").trim();
			  Driver.instance.findElement(By.id(Userselect)).click();
			  List<WebElement> element=Driver.instance.findElements(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlUsers_DropDown']/div/ul/li"));
			  int usersize=element.size();
			  System.out.println(usersize);
			  for(int si=1;si<=usersize;si++)
			  {
				  String username=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlUsers_DropDown']/div/ul/li["+si+"]")).getText();
				  if(username.trim().equals(uname.trim()))
				  {
					  Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlUsers_DropDown']/div/ul/li["+si+"]")).click();
				  }
			  }		
			  Thread.sleep(2000);
			  Driver.instance.findElement(By.id(Statesdropdown)).click();
			  Thread.sleep(1000);
			  Driver.instance.findElement(By.xpath(statesdata)).click();
			  Thread.sleep(2000);
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(actioncasesearch))).click();
			  //Driver.instance.findElement(By.id(actioncasesearch)).click();
			  Thread.sleep(2000);
			  int resize=Driver.instance.findElements(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[6]")).size();
			  System.out.println("resize="+resize);
			  if(resize==0)
			  {
				  System.out.println("Case not found under Employee");
			  }
			  else
			  {
				  WebElement elementcase=Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl04_chkSelectBox"));
				  elementcase.click();				  
			  }
			  Thread.sleep(1000);
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(reassignbutton))).click();
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(Reassignsubmit)));
			  int checksize=Driver.instance.findElements(By.id(reassigncheckbox)).size();
			  if(checksize>0)
			  {
				  Driver.instance.findElement(By.id(reassigncheckbox)).click();
				  WebElement name=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_rdwCaseAssignment_C_grdCaseAssignmentResource_ctl00__0']/td[3]"));
				  reassignname=name.getText();
				  wait.until(ExpectedConditions.elementToBeClickable(By.id(Reassignsubmit))).click();			
				  wait.until(ExpectedConditions.elementToBeClickable(By.id(closereassign))).click();
				  wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan"))).click();
				  System.out.println("Case Re-assigned successfully");
				  Driver.instance.findElement(By.id(Logout.Home_sp_logout)).click();
				  //Joptionpane.input
			  }
			  else
			  {
				  System.out.println("No User's are available to re-assign the case");
			  }
			  
			  
			  
		}
	}
