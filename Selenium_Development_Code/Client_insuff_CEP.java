package check360_flow;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Client_insuff_CEP {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		WebElement sd;
		int k=4,j=0;
		int checksize=45;
		driver.get("http://192.168.2.17:96");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
		Thread.sleep(7000);
		String rno=JOptionPane.showInputDialog("Enter Reference number:\n");
		Thread.sleep(500);
		String raise=JOptionPane.showInputDialog("Select stages to raise Insuff & CEP"+"\n"+"1.Data Entry"+"\n"+"2.Data Entry QC");
		int stage=Integer.parseInt(raise);
		WebElement w1= driver.findElement(By.id("ddlAct"));	 
		WebElement w2= driver.findElement(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[4]/select"));
		if(stage==1)
		{		
		Select s= new Select(w1);
		s.selectByValue("2");
		Thread.sleep(2000);
		Select s1=new Select(w2);
		s1.selectByValue("2");
		Thread.sleep(2000);
		
		}
		else if(stage==2)
		{
			Select s= new Select(w1);
			s.selectByValue("4");
			Thread.sleep(1000);
			Select s1=new Select(w2);
			s1.selectByValue("3");
			Thread.sleep(2000);
			
		}
		
		 //Select s1=new Select(w2);
		 //s1.selectByValue("3");
		// Thread.sleep(2500);
		 driver.findElement(By.id("txtCaserefNo")).clear();
			driver.findElement(By.id("txtCaserefNo")).sendKeys(rno);
			driver.findElement(By.id("btnsearch")).click();
			Thread.sleep(7000);
			String Checkname;
			  int wait;
			  int TabCheck;
			  int dcc = 0;
			  String CreditID; 
			 wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).size();
			//System.out.println(wait);
			if(wait!=0)
			{
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).click();
			
			Thread.sleep(5500);
			 TabCheck = driver.findElements(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).size();
			 System.out.println("No of Checks in the Case:"+TabCheck);	
			 for(int tc=1;tc<=TabCheck;tc++)
			 {
				 try
				 {
				 if(stage==1)
				 {
			 	//System.out.println(tc);
			 	driver.switchTo().defaultContent();
			 	Thread.sleep(2000);
			 	  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).getText();
			 	  System.out.println("Checkname"+Checkname);
			 	 if (Checkname.equals("Employment"))
				  {
					  Thread.sleep(2000);
					  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
					 Thread.sleep(1500);
				  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[3]/iframe")); 
				  driver.switchTo().frame(element);
				  Thread.sleep(3000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).click();
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys("TCS");
				  Thread.sleep(2000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys(Keys.DOWN);
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys(Keys.ENTER);
				  Thread.sleep(2500);
					
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeID")).sendKeys("554545");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeDesignation")).sendKeys("Tester");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeDepartment")).sendKeys("Testing");
				  
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtFromDate_dateInput")).sendKeys("2013-12-05");
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkTillDate")).click();
				  Thread.sleep(2500);
					
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtLastSalaryDrawn")).sendKeys("4 Lakhs");
				  
				  sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCurrencyType_Input"));
				  sd.sendKeys("USD");
				  sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentType_Input"));
				  sd.sendKeys("Permanent");
				  sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmployeeSalaryType_Input"));
				  sd.sendKeys("Per Month");
				  sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRelationship1_Input"));
				  sd.sendKeys("HR");
						
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1Name")).sendKeys("Ravi");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1Designation")).sendKeys("HR");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1ContactNo1")).sendKeys("9632587401");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1EmailID1")).sendKeys("ravi@gmail.com");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtReasonOfLeaving")).sendKeys("Salary");
				  Thread.sleep(2500);
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkEmploymentInsuff")).click();
				  Thread.sleep(2000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentInsuffRemarks")).sendKeys("Not Sufficient data "+Checkname);
				  Thread.sleep(1000);
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkCanVerifyNow")).click();
				  Thread.sleep(3500);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRelievingDate_dateInput")).sendKeys("25/12/2017");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentCEPRemarks")).sendKeys("Contact after given date");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSubmit_input")).click();
				  Thread.sleep(3500);
				  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
					 {
						 driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
					 }
					 else
						 driver.navigate().refresh();
				  Thread.sleep(3500);
				  }
			 	 else if (Checkname.equals("Reference"))
				  {
			 		Thread.sleep(2000);
					  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
					 Thread.sleep(1500);
				  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[4]/iframe")); 
				  driver.switchTo().frame(element);
				  Thread.sleep(3000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlReferType_Input")).click();
				  Thread.sleep(1000);
				  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlReferType_DropDown']/div/ul/li[contains(text(), 'Academic')]")).click();
				  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtReferenceName")).sendKeys("Referer  Name");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefDes")).sendKeys("Referer  Designation");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefContactNo1")).sendKeys("9632014587");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefEmailId1")).sendKeys("ref@gmail.com");
				
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).clear();
			  
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).sendKeys("India");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).sendKeys(Keys.ENTER);
			  Thread.sleep(3000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).click();
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).sendKeys("Tamil Nadu");
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).sendKeys(Keys.ENTER);
			  Thread.sleep(3000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).sendKeys("Chennai");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).sendKeys(Keys.ENTER);
			  Thread.sleep(3000);

			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).sendKeys("PSYEC");
			  Thread.sleep(2000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).sendKeys(Keys.DOWN);
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).sendKeys(Keys.ENTER);
			  Thread.sleep(2500);
			  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_CheckRefReportInsuff")).click();
			  Thread.sleep(2000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportInsuff")).sendKeys("Not Sufficient data "+Checkname);
			  Thread.sleep(1000);
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_CheckRefReportYTR")).click();
				  Thread.sleep(2000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtExpectedRelievingDate_dateInput")).sendKeys("25/12/2016");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportYTR")).sendKeys("Conatct after given date refence");
				 Thread.sleep(1500);
				 driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
				 Thread.sleep(3500);
				 if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
				 {
					 driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
				 }
				 else
					 driver.navigate().refresh();
					Thread.sleep(3000);
				  }
				 }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////			 	 
			 	//Thread.sleep(3500);
				//driver.findElement(By.id("imgHome")).click();
				//Thread.sleep(5000);
				//driver.findElement(By.id("btnActions")).click();
//				Thread.sleep(3000);
				 
				 else if(stage==2)
				 {
					 driver.switchTo().defaultContent();
					 	Thread.sleep(2000);
					 	  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).getText();
					 	  System.out.println("Checkname"+Checkname);
					 	 if (Checkname.equals("Employment"))
						  {
							  Thread.sleep(2000);
							  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
							 Thread.sleep(3500);
						 WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[3]/iframe")); 
						 driver.switchTo().frame(element);
						  Thread.sleep(3000);
						  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkEmploymentInsuff")).click();
						  Thread.sleep(2000);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentInsuffRemarks")).sendKeys("Not Sufficient data "+Checkname);
						  Thread.sleep(1000);
						  if(!driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentCEPRemarks")).isEnabled())
						  {
						  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkCanVerifyNow")).click();
						  Thread.sleep(5500);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRelievingDate_popupButton")).click();
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRelievingDate_calendar_NN")).click();
						  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_txtRelievingDate_calendar_Top']/tbody/tr[3]/td[4]/a")).click();
						  //driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRelievingDate_dateInput")).sendKeys("20/12/2016");
						  Thread.sleep(2500);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentCEPRemarks")).sendKeys("Contact after date");
						  Thread.sleep(2000);
						  }
						  try
						  {
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSubmit_input")).click();
						  Thread.sleep(2000);
						  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
							 {
								 driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
							 }
							 else
								 driver.navigate().refresh();
						  }
						  catch(NoAlertPresentException e)
						  {
							  driver.switchTo().alert().accept();
						  }
						  
						  }
						  
					 	 else if (Checkname.equals("Reference"))
						  {
					 		Thread.sleep(2000);
							  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
							 Thread.sleep(3500);
						  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[4]/iframe")); 
						  driver.switchTo().frame(element);
						  Thread.sleep(3000);
						  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_CheckRefReportInsuff")).click();
						  Thread.sleep(2000);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportInsuff")).sendKeys("Not Sufficient data "+Checkname);
						  Thread.sleep(1000);
						  if(!driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportYTR")).isEnabled())
						  {
							  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_CheckRefReportYTR")).click();
						  Thread.sleep(3500);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtExpectedRelievingDate_popupButton")).click();
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtExpectedRelievingDate_calendar_NN")).click();
						  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_txtExpectedRelievingDate_calendar_Top']/tbody/tr[4]/td[5]/a")).click();
						  Thread.sleep(2500);
						 // driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtExpectedRelievingDate_dateInput")).sendKeys("20/12/2017");
						 // Thread.sleep(2000);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportYTR")).sendKeys("Contact after date");
						  Thread.sleep(2000);
						  }						  
						  try
						  {
							  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
							 Thread.sleep(2000);
							  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
								 {
									 driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
								 }
								 else
									 driver.navigate().refresh();				  
							  
						  }
						  catch(NoAlertPresentException e)
						  {
							  driver.switchTo().alert().accept();
						  }
						  }
					 	Thread.sleep(3500);
				 }
				 }
				 catch(NoSuchElementException e)
				 {
					 continue;
				 }
			 }
			}
			 if(stage==2)
			 {
						//driver.findElement(By.id("imgHome")).click();
						//Thread.sleep(5000);
						//driver.findElement(By.id("btnActions")).click();
						//Thread.sleep(3000);
						String option=JOptionPane.showInputDialog("Do you want to clear"+"\n"+"1.Insuff"+"\n"+"2.CEP");
						int select=Integer.parseInt(option);
						if(select==1)
						{
							 try
							  {
								 Thread.sleep(1500);
								 driver.switchTo().defaultContent();			 
									 driver.findElement(By.id("imgHome")).click();
									Thread.sleep(7000);
							  }
							  catch(NoSuchElementException e)
							  {
								  System.out.println(e);
							  }
									driver.findElement(By.id("btnActions")).click();				 
								 Thread.sleep(3000);
									driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
									Thread.sleep(500);
									driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[1]")).click();
									Thread.sleep(1500);
									driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
									driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
									driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlWorkflowType_Input")).click();
									Thread.sleep(500);
									driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[3]")).click();
									Thread.sleep(1000);
									driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
									Thread.sleep(2000);
									 wait = driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).size();
										//System.out.println(wait);
										if(wait!=0)
										{
										Thread.sleep(1000);
										driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl04_btnClearInsuff")).click();
										}
										Thread.sleep(2500);
										for(j=0;j<=checksize;j++)
										{
										try
											{
											String check=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00__"+j+"']/td[29]")).getText();
											String check1=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00__"+j+"']/td[15]")).getText();
											while(k<=checksize)
											{							
											if((check.equals("Data Entry QC")||check.equals("Data Entry QC After Insuff Clear"))&&(check1.equals("Employment")||check1.equals("Reference")))
											{
												Thread.sleep(1000);
												String insuffcheck=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00__"+j+"']/td[17]")).getText();
												System.out.println("insuff Cleared check "+check1);
												if(k<10)
												{
												driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+k+"_chkInsuffComponentSelectCheckBox")).click();
												driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+k+"_txtInsuffClearComments")).sendKeys("insuff cleared "+check1);
												/*driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+k+"_btnUploadInsuffDocuments")).click();
												Thread.sleep(2500);
												String doc=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00__0']/td[6]")).getText();
												if(doc.isEmpty())
												{
												driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/test2.pdf");
												Thread.sleep(1500);
												driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_btnSubmitAddedDocument")).click();
												Thread.sleep(2000);
												driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();				
												}
												else
												driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
												Thread.sleep(650);		*/						
												}
												else
												{
													driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl"+k+"_chkInsuffComponentSelectCheckBox")).click();
													driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl"+k+"_txtInsuffClearComments")).sendKeys("insuff cleared "+check1);
													/*driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl"+k+"_btnUploadInsuffDocuments")).click();
													Thread.sleep(2500);
													String doc=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00__0']/td[6]")).getText();
													if(doc.isEmpty())
													{
													driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/test3.pdf");
													Thread.sleep(1500);
													driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_btnSubmitAddedDocument")).click();
													Thread.sleep(2000);
													driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
													//Thread.sleep(650);	
													}
													else
														driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
														Thread.sleep(650);		*/									
												}							
												}	
											k=k+2;
											break;
											}
											}						
											catch(NoSuchElementException e)
											{
												continue;
											}
										}
										Thread.sleep(500);
										driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_btnClear_input")).click();
										Thread.sleep(3500);
										 WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
										  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
										  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();	
										  Thread.sleep(1000);
										  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnStages_input")).click();					
										}
						if(select==2)
						{
							try
							  {
								 Thread.sleep(1500);
								 driver.switchTo().defaultContent();			 
									 driver.findElement(By.id("imgHome")).click();
									Thread.sleep(5000);
							  }
							  catch(NoSuchElementException e)
							  {
								  System.out.println(e);
							  }
									driver.findElement(By.id("btnActions")).click();	
							Thread.sleep(2500);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
						Thread.sleep(500);
						driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[4]")).click();
						Thread.sleep(1500);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlWorkflowType_Input")).click();
						Thread.sleep(500);
						driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[3]")).click();
						Thread.sleep(1000);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
						Thread.sleep(2000);
						 wait = driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).size();
							//System.out.println(wait);
							if(wait!=0)
							{
							Thread.sleep(1000);
							driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).click();
												
							Thread.sleep(4500);
						/*	String doc=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00__0']/td[6]")).getText();
							if(doc.isEmpty())
							{
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/sddf.pdf");
							Thread.sleep(1500);
							}
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_btnAddDocument")).click();
							Thread.sleep(1500);*/
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_txtClearedRemarks")).sendKeys("Ready to contact");
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_ButtonSubmit_input")).click();
							Thread.sleep(3500);
							driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
							Thread.sleep(2000);
							}					
							//////////////////////////////////////////////////////////////////////////////////////////////////
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlWorkflowType_Input")).click();
							Thread.sleep(500);
							driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[3]")).click();
							Thread.sleep(1000);
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
							Thread.sleep(2000);
							 wait = driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).size();
								//System.out.println(wait);
								if(wait!=0)
								{
								Thread.sleep(1000);
								driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).click();
								
								Thread.sleep(4500);
								/*String doc=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00__0']/td[6]")).getText();
								if(doc.isEmpty())
								{
								driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/sddf.pdf");
								Thread.sleep(1500);
								}
								driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_btnAddDocument")).click();
								Thread.sleep(1500);*/
								driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_txtClearedRemarks")).sendKeys("Ready to contact");
								driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_ButtonSubmit_input")).click();
								Thread.sleep(3500);
								driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
								Thread.sleep(2000);
								}
						}
								driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnStages_input")).click();					
								
				 }			 	 
			 if(stage==1)
			 {
			String option=JOptionPane.showInputDialog("Do you want to clear"+"\n"+"1.Insuff"+"\n"+"2.CEP");
			int select=Integer.parseInt(option);
			if(select==1)
			{
				 try
				  {
					 Thread.sleep(1500);
					 driver.switchTo().defaultContent();			 
						 driver.findElement(By.id("imgHome")).click();
						Thread.sleep(5000);
				  }
				  catch(NoSuchElementException e)
				  {
					  System.out.println(e);
				  }
						driver.findElement(By.id("btnActions")).click();				 
					 Thread.sleep(3000);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
						Thread.sleep(500);
						driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[1]")).click();
						Thread.sleep(1500);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlWorkflowType_Input")).click();
						Thread.sleep(500);
						driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[3]")).click();
						Thread.sleep(1000);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
						Thread.sleep(2000);
						 wait = driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).size();
							//System.out.println(wait);
							if(wait!=0)
							{
							Thread.sleep(1000);
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl04_btnClearInsuff")).click();
							}
							Thread.sleep(2500);
							for(j=0;j<=checksize;j++)
							{
							try
								{
								String check=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00__"+j+"']/td[29]")).getText();
								String check1=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00__"+j+"']/td[15]")).getText();
								while(k<=checksize)
								{							
								if((check.equals("Data Entry")||check.equals("Data Entry After Insuff Clear"))&&(check1.equals("Employment")||check1.equals("Reference")))
								{
									Thread.sleep(1000);
									String insuffcheck=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00__"+j+"']/td[17]")).getText();
									System.out.println("insuff Cleared check "+check1);
									if(k<10)
									{
									driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+k+"_chkInsuffComponentSelectCheckBox")).click();
									driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+k+"_txtInsuffClearComments")).sendKeys("insuff cleared "+check1);
									/*driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+k+"_btnUploadInsuffDocuments")).click();
									Thread.sleep(2500);
									String doc=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00__0']/td[6]")).getText();
									if(doc.isEmpty())
									{
									driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/test2.pdf");
									Thread.sleep(1500);
									driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_btnSubmitAddedDocument")).click();
									Thread.sleep(2000);
									driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();				
									}
									else
									driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
									Thread.sleep(650);		*/						
									}
									else
									{
										driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl"+k+"_chkInsuffComponentSelectCheckBox")).click();
										driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl"+k+"_txtInsuffClearComments")).sendKeys("insuff cleared "+check1);
										/*driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl"+k+"_btnUploadInsuffDocuments")).click();
										Thread.sleep(2500);
										String doc=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00__0']/td[6]")).getText();
										if(doc.isEmpty())
										{
										driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/test3.pdf");
										Thread.sleep(1500);
										driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_btnSubmitAddedDocument")).click();
										Thread.sleep(2000);
										driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
										//Thread.sleep(650);	
										}
										else
											driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
											Thread.sleep(650);	*/										
									}							
									}	
								k=k+2;
								break;
								}
								}						
								catch(NoSuchElementException e)
								{
									continue;
								}
							}
							Thread.sleep(500);
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_btnClear_input")).click();
							Thread.sleep(3500);
							 WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
							  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
							  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();	
							  Thread.sleep(1000);
							  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnStages_input")).click();					
							}
			if(select==2)
			{
				try
				  {
					 Thread.sleep(1500);
					 driver.switchTo().defaultContent();			 
						 driver.findElement(By.id("imgHome")).click();
						Thread.sleep(7000);
				  }
				  catch(NoSuchElementException e)
				  {
					  System.out.println(e);
				  }
					driver.findElement(By.id("btnActions")).click();	
				Thread.sleep(2500);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
			Thread.sleep(500);
			driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[4]")).click();
			Thread.sleep(1500);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlWorkflowType_Input")).click();
			Thread.sleep(500);
			driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[3]")).click();
			Thread.sleep(1000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
			Thread.sleep(2000);
			 wait = driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).size();
				//System.out.println(wait);
				if(wait!=0)
				{
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).click();
									
				Thread.sleep(4500);
				/*String doc=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00__0']/td[6]")).getText();
				if(doc.isEmpty())
				{
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/sddf.pdf");
				Thread.sleep(1500);
				}
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_btnAddDocument")).click();
				Thread.sleep(1500);*/
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_txtClearedRemarks")).sendKeys("Ready to contact");
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_ButtonSubmit_input")).click();
				Thread.sleep(3500);
				driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
				Thread.sleep(2000);
				}					
				//////////////////////////////////////////////////////////////////////////////////////////////////
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlWorkflowType_Input")).click();
				Thread.sleep(500);
				driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[3]")).click();
				Thread.sleep(1000);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
				Thread.sleep(2000);
				 wait = driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).size();
					//System.out.println(wait);
					if(wait!=0)
					{
					Thread.sleep(1000);
					driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).click();
					
					Thread.sleep(4500);
					/*String doc=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00__0']/td[6]")).getText();
					if(doc.isEmpty())
					{
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/sddf.pdf");
					Thread.sleep(1500);
					}
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_btnAddDocument")).click();
					Thread.sleep(1500);*/
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_txtClearedRemarks")).sendKeys("Ready to contact");
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_ButtonSubmit_input")).click();
					Thread.sleep(3500);
					driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
					Thread.sleep(2000);
					}
			}
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnStages_input")).click();					
								 	 
		 }	  
	}

}
