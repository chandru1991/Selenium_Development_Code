package Project_Flow;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import Files.Excel_Read_Logins;
import Files.Propertyfile;
import Files.Screenshots;
import Files.Text_Writer;
import Project_Path.Client_Homepage_path;
import Project_Path.Gmail_path;
import Project_Path.Loginpage_path;
import Project_Path.SP_Homepage_path;

public class SP_Testng {
	SoftAssert softAssert=new SoftAssert();
	@BeforeTest()
	public void beforeTest() throws Exception
	{
		new Propertyfile();
		Excel_Read_Logins.SP_Excel_Read_Logins(Propertyfile.sp_excelfilepath, Propertyfile.sheetname, Propertyfile.columncount);
	}
	@BeforeMethod()
	public void beforeMethod_sp() throws Exception
	{
		new Base_ServiceProvider();
	}
	@AfterMethod()
	public void afterMethod() throws InterruptedException
	{
		Driver.instance.close();
	}
	@Test(dataProvider="sp_dataprovider",priority=1,enabled=true)
	public void login(String username,String password,String alert) throws Exception
	{
		SoftAssert softAssert=new SoftAssert();
		System.out.println("	"+"-------------------SP_Login Functionality-------------------");
		new Text_Writer("	"+"-------------------SP_Login Functionality-------------------");
		//WebDriverWait wait=new WebDriverWait(Driver.instance,20);
		String usernotavailable;
		//int unamesize=0,pwsize=0;
		Login.sp_login(username,password);//new ClientMaster();
		Thread.sleep(2500);
		int findAvailable=Driver.instance.findElements(By.xpath(SP_Homepage_path.home)).size();
		//Driver.wait.until(ExpectedConditions.elementToBeClickable(Driver.instance.findElement(By.xpath(Homepage_path.homelink)))).isDisplayed();
		if(findAvailable==1)
		{
		//System.out.println(username+" - "+password+" = "+"Logged in Successfully");
			usernotavailable="Logged in Successfully.";
		}
		else
		{
			usernotavailable=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
		}
		softAssert.assertEquals(usernotavailable.trim(), alert.trim(), "	"+username+" - "+password+" = "+"Fail");
		if(usernotavailable.trim().equals(alert.trim()))
		{			
			System.out.println("	"+username+" - "+password+" = "+"Pass");
			new Text_Writer("	"+username+" - "+password+" = "+"Pass");
		}
		else
		{
			new Screenshots(username+"_"+password);
			System.out.println("	"+username+" - "+password+" = "+"Fail");
			new Text_Writer("	"+username+" - "+password+" = "+"Fail");
		}
		softAssert.assertAll();
	}
	
	@Test(priority=2,enabled=false)
	public void forgotPassword_serviceprovider() throws Exception
	{
		
		WebDriverWait wait=new WebDriverWait(Driver.instance,20);
		System.out.println("	"+"-------------------Forgot Password Functionality-------------------");
		new Text_Writer("	"+"-------------------Forgot Password Functionality-------------------");
		//Forgot_Password.SP_Forgot_Password();
		Driver.instance.get(Propertyfile.mailurl);
		Driver.instance.findElement(By.id(Gmail_path.gusername)).sendKeys(Propertyfile.gid);
		Driver.instance.findElement(By.xpath(Gmail_path.gusernamenext)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Gmail_path.gpassword))).sendKeys(Propertyfile.gpassword);
		Driver.instance.findElement(By.xpath(Gmail_path.gpasswordnext)).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id(Gmail_path.gsearch))).sendKeys(Propertyfile.gsubject,Keys.ENTER);
		Thread.sleep(1500);
		try
		{
		wait.until(ExpectedConditions.elementToBeClickable(By.className(Gmail_path.gsubjectclick))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Reset Password"))).click();
		Driver.instance.get("http://192.168.2.239:5000/resetpassword/serviceprovideruser/56f240be8a56675312ef347a");
		
		Thread.sleep(3000);
		}catch(Exception e)
		{
		System.out.println("	"+"Mail Not Received"+e.getMessage());	
		new Text_Writer("	"+"Mail Not Received"+e.getMessage());	
		}
		softAssert.assertAll();
	}
	
	@DataProvider
	public Object[][] sp_dataprovider()
	{
		int row=Excel_Read_Logins.sp_rowcount;
		int col=Excel_Read_Logins.sp_colncount;
		Object data[][]=new Object[row][col];
		for(int i=0;i<row;i++)
		{
			for(int j=0;j<col;j++)
			{
				data[i][j]=Excel_Read_Logins.sp_cell[i+1][j];
			}
		}
		return data;		
	}
	
	@BeforeTest(alwaysRun=true)
	public void afterTest()
	{
		//Driver.instance.quit();
	}
	
}
