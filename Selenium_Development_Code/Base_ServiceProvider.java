package Project_Flow;

import javax.swing.JOptionPane;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import Files.Propertyfile;

public class Base_ServiceProvider {
	
		Boolean bool=false;
		public Base_ServiceProvider() throws Exception
			{	 
			try
				{
			switch(Propertyfile.browser)
			{
			
			case "chrome":
				System.setProperty("webdriver.chrome.driver", Propertyfile.chromedriverpath);
				Driver.instance=new ChromeDriver();
				Driver.instance.manage().window().maximize();
				Driver.instance.get(Propertyfile.openworldurl);
				bool=true;
				break;
				
			case "firefox":
				//Driver.instance=new FirefoxDriver();
				FirefoxProfile firefoxProfile = new FirefoxProfile();
				firefoxProfile.setPreference("xpinstall.signatures.required", false); 
				Driver.instance = new FirefoxDriver(firefoxProfile);
				Driver.instance.manage().window().maximize();
				Driver.instance.get(Propertyfile.openworldurl);
				//Driver.instance.navigate().to(url);
				bool=true;
				break;
				
			default:
				JOptionPane.showMessageDialog(null, "Browser Not Available","Alert", 0);
			}
				}catch(Exception e)
				{
					System.out.println("issue in browser"+"=="+e);
				//	text("issue in browser");				
				}
				//return new Login();
	}
	}


