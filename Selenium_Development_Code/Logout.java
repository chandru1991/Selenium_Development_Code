package Checks360;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Logout extends txt_write{
	public static WebDriverWait wait=new WebDriverWait(Driver.instance,30);
	static String DE_sp_logout="lnkLogout";
	static String Home_sp_logout="ctl00_lnkLogout";
	public void DE_logout()
	{
		try
		{
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(DE_sp_logout))).click();	
		//Driver.instance.findElement(By.id(sp_logout)).click();
		Thread.sleep(1000);
		url();
		
		}catch(Exception e)
		{
			System.out.println(e);
		}
	}
	public void Home_logout()
	{
		try
		{
		wait.until(ExpectedConditions.elementToBeClickable(By.id(Home_sp_logout))).click();	
		//Driver.instance.findElement(By.id(sp_logout)).click();
		Thread.sleep(1000);
		url();
		
		}catch(Exception e)
		{
			System.out.println(e);
		}
	}
	public void url() throws Exception
	{
		String current=Driver.instance.getCurrentUrl();
		System.out.println("current:"+current);
		if(current.trim().equals(Base.url.trim()))
		{
			Base.bool=true;
			//System.out.println("Logout Successfully");		
			text("User Logout Successfully");
			Excel_write.inputExcel("User Logout", "pass");	
			Thread.sleep(500);
		}
	}

}
