package check360_flow;

import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;
import com.thoughtworks.selenium.Selenium;

public class Candidate_FinalReport {
static int wait=0;
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://192.168.2.17:96");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
		//Thread.sleep(7000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String rno;
		Thread.sleep(2000);
		 WebElement w1= driver.findElement(By.id("ddlAct"));
		 Select s= new Select(w1);
			s.selectByValue("9");
			Thread.sleep(1000);
			WebElement w2= driver.findElement(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[4]/select"));
			 Select s1=new Select(w2);
			 s1.selectByValue("2");
			 // driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[contains(text(), 'Candidate Case Regn')]")).click();
			  Thread.sleep(3000);
			  driver.findElement(By.id("btnsearch")).click();
			  driver.findElement(By.id("txtCaserefNo")).clear();
			  rno=JOptionPane.showInputDialog("Enter Reference number:\n");
			  driver.findElement(By.id("txtCaserefNo")).sendKeys(rno);
			  driver.findElement(By.id("btnsearch")).click();
			  Thread.sleep(2000);
			  wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/thead/tr/td[2]")).size();
			  //System.out.println(wait);
if(wait!=0)
{
	Thread.sleep(1000); 
	try
	{
	WebElement w= driver.findElement(By.id("Reserverfor"));
	if(driver.findElement(By.id("Reserverfor")).isDisplayed())
	{
	Select s2= new Select(w);
	s2.selectByValue("0");
	//Thread.sleep(1000);
	}
	}
	catch(Exception e)
	{
		System.out.println(e);
	}
	 }
else
{
	 System.out.println("Given Reference No doesnt exists in this Stage");
	 JOptionPane.showInternalMessageDialog(null, "Ref No. doesn't exist","Alert", JOptionPane.ERROR_MESSAGE);
	 driver.close();
}
	Thread.sleep(2500);	
	//Thread.sleep(2000);
		Select s5= new Select(w1);
		s5.selectByValue("10");
		Thread.sleep(1000);	
		//WebElement w2= driver.findElement(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[4]/select"));
		 Select s3=new Select(w2);
		 s3.selectByValue("2");
		 Thread.sleep(1000);
		driver.findElement(By.id("txtCaserefNo")).clear();
		driver.findElement(By.id("txtCaserefNo")).sendKeys(rno);
		driver.findElement(By.id("btnsearch")).click();
		Thread.sleep(4000);
		wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/thead/tr/td[2]")).size();
		//System.out.println(wait);
		if(wait!=0)
		{
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).click();
		Thread.sleep(5000);
		}
		//driver.findElement(By.id("btnGetNext")).click();
		Thread.sleep(5000);

		 driver.findElement(By.id("btnReportGenerate")).click();
	     
	     Thread.sleep(6000);
	    // WebElement e=driver.findElement(By.xpath("//*[@id='_rfdSkinnedrwReportComponent_C_grdReportComponent_ctl00_ctl02_ctl00_chkReportSelectSelectCheckBox']"));
	   
	    if(!driver.findElement(By.id("_rfdSkinnedrwReportComponent_C_grdReportComponent_ctl00_ctl02_ctl00_chkReportSelectSelectCheckBox")).isSelected())
	    {
	    driver.findElement(By.id("_rfdSkinnedrwReportComponent_C_grdReportComponent_ctl00_ctl04_chkReportSelectSelectCheckBox")).click();
	     //driver.findElement(By.xpath("//*[@id='_rfdSkinnedrwReportComponent_C_grdReportComponent_ctl00_ctl14_chkReportSelectSelectCheckBox']")).click();
	     Thread.sleep(6000);
	     driver.findElement(By.id("rwReportComponent_C_ddlTemplate_Input")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlTemplate_DropDown']/div/ul/li[contains(text(), 'PWC Report')]")).click();
		  Thread.sleep(2000);
		  
		  driver.findElement(By.id("rwReportComponent_C_ddlReportHeader_Input")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlReportHeader_DropDown']/div/ul/li[contains(text(), 'Final')]")).click();
		  Thread.sleep(2000);
		  
		  driver.findElement(By.id("rwReportComponent_C_ddlCaseStatus_Input")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlCaseStatus_DropDown']/div/ul/li[contains(text(), 'Unable to Verify')]")).click();
		  Thread.sleep(2000);

	     
	        
	       driver.findElement(By.id("rwReportComponent_C_btnRptPreview_input")).click();
	       Thread.sleep(1000);
	       driver.findElement(By.id("rwReportComponent_C_btnPublishReport_input")).click();
	       Thread.sleep(10000);
	       Alert alert4 = driver.switchTo().alert();
			alert4.accept();
			Thread.sleep(10000);
			System.out.println("Report Published");
			driver.close();
	    }
	    else 
	    {	      
	     driver.findElement(By.id("rwReportComponent_C_ddlTemplate_Input")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlTemplate_DropDown']/div/ul/li[contains(text(), 'PWC Report')]")).click();
		  Thread.sleep(2000);
		  
		  driver.findElement(By.id("rwReportComponent_C_ddlReportHeader_Input")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlReportHeader_DropDown']/div/ul/li[contains(text(), 'Final')]")).click();
		  Thread.sleep(2000);
		  
		  driver.findElement(By.id("rwReportComponent_C_ddlCaseStatus_Input")).click();
		  Thread.sleep(2500);
		  //driver.findElement(By.id("rwReportComponent_C_ddlCaseStatus_Input")).sendKeys(Keys.DOWN);
		  //driver.findElement(By.id("rwReportComponent_C_ddlCaseStatus_Input")).sendKeys(Keys.ENTER);
		  driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlCaseStatus_DropDown']/div/ul/li[contains(text(), 'Unable to Verify')]")).click();
		  Thread.sleep(2000);

	     
		  String currentwindow=driver.getWindowHandle();
	      driver.findElement(By.id("rwReportComponent_C_btnRptPreview_input")).click();
	       Thread.sleep(5000);
	       for(String newwindow : driver.getWindowHandles())
	       {
	    	   driver.switchTo().window(newwindow);
	       }
	       Thread.sleep(5000);
	       JavascriptExecutor jse=(JavascriptExecutor)driver;
	       jse.executeScript("window.scrollBy(0,250)", "");
	       driver.close();
	       driver.switchTo().window(currentwindow);
	       driver.findElement(By.id("rwReportComponent_C_btnPublishReport_input")).click();
	       Thread.sleep(10000);
	       Alert alert3 = driver.switchTo().alert();
	       alert3.accept();
	       //Thread.sleep(10000);
	       System.out.println("Report Published");
	    }

	}

}
