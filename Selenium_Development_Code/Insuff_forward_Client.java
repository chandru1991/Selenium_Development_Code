package check360_flow;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Insuff_forward_Client {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		WebElement sd;
		int wait;
		driver.get("http://192.168.2.17:96");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
		Thread.sleep(7000);
		driver.findElement(By.id("btnActions")).click();
		 Thread.sleep(3000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
			Thread.sleep(500);
			driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[1]")).click();
			Thread.sleep(1500);
			String rno=JOptionPane.showInputDialog("Enter Reference number:\n");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlWorkflowType_Input")).click();
			Thread.sleep(500);
			driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[3]")).click();
			Thread.sleep(1000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
			Thread.sleep(2000);
			//String name=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[6]")).getText().replace(" ","");
			//System.out.println(name);
			 wait = driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).size();
				//System.out.println(wait);
				if(wait!=0)
				{
				Thread.sleep(1000);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl04_btnClearInsuff")).click();
				}
				Thread.sleep(2500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl04_btnForwardInsuff")).click();
				Thread.sleep(2500);
				//String forward=JOptionPane.showInputDialog("Do You want forward this check to"+"\n"+"1.Client"+"\n"+"2.Candidate");
				//int option=Integer.parseInt(forward);				
					//driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_rdbSendInsuffComponentsTo_0")).click();
					Thread.sleep(500);
					driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl04_chkInsuffComponentSelectCheckBox")).click();
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl04_txtInsuffForwardComments")).sendKeys("Forward to Client");
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_btnForward_input")).click();
					Thread.sleep(2000);
					WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();	
					  Thread.sleep(1000);
					  driver.findElement(By.id("ctl00_lnkLogout")).click();
					  driver.get("http://192.168.2.17:96/clientlogin.aspx");
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("democlient");
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@360");
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
						Thread.sleep(4000);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
						Thread.sleep(650);
						driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[5]")).click();
						Thread.sleep(1500);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
						Thread.sleep(1500);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl04_btnClearInsuff")).click();
						Thread.sleep(1500);
						driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl04_chkInsuffComponentSelectCheckBox")).click();
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl04_txtInsuffClearComments")).sendKeys("Clear client "+rno);
						/*driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl04_btnUploadInsuffDocuments")).click();
						Thread.sleep(2000);
						String doc=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00__0']/td[6]")).getText();
						if(doc.isEmpty())
						{
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/2586.pdf");
							Thread.sleep(1500);
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_btnSubmitAddedDocument_input")).click();
							Thread.sleep(2000);
							driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
						}
						else
						{
						driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
						}*/
						    Thread.sleep(1500);
							driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_btnClearInsuff")).click();
							Thread.sleep(1500);
							WebDriverWait await1 = new WebDriverWait(driver, 15);//explicit wait
							  await1.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
							  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();	
							  Thread.sleep(1000);
						}				

	}


