package check360_flow;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Stopcheck {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
      WebDriver driver=new FirefoxDriver();
      driver.manage().window().maximize();
      driver.get("http://192.168.2.17:96");
      driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
	  Thread.sleep(10000);
	  driver.findElement(By.id("btnActions")).click();
	  Thread.sleep(3000);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
	  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[contains(text(), 'Stop Check')]")).click();
	  Thread.sleep(2500);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
	  String rno=JOptionPane.showInputDialog("Enter the Reference number");
	 // String checkname=JOptionPane.showInputDialog("Select component to stop"+"\n"+"1.Address"+"\n"+"2.Education"+"\n"+"3.Employment"+"\n"+"4.Reference"+"\n"+"5.ID"+"\n"+"6.Court"+"\n"+"7.Criminal"+"\n"+"8.Database"+"\n"+"9.Credit"+"\n"+"10.Drug & Medical");
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
	  Thread.sleep(2500);
	
	 driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl02_ctl00_PageSizeComboBox_Input")).click();
	 driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl02_ctl00_PageSizeComboBox_Input")).sendKeys(Keys.DOWN);
	 driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl02_ctl00_PageSizeComboBox_Input")).sendKeys(Keys.ENTER);
	 Thread.sleep(3000);
	 String checksize=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00_TopPager']/thead/tr/td/table/tbody/tr/td[2]/div[5]")).getText().substring(0,2).trim();
	 int size=Integer.parseInt(checksize);
	 System.out.println(size);
	 do
	 {
	 String checkname=JOptionPane.showInputDialog("Select component to stop"+"\n"+"1.Address"+"\n"+"2.Education"+"\n"+"3.Employment"+"\n"+"4.Reference"+"\n"+"5.ID"+"\n"+"6.Court"+"\n"+"7.Criminal"+"\n"+"8.Database"+"\n"+"9.Credit"+"\n"+"10.Drug & Medical"+"\n"+"11.Exit");
	  int cn=Integer.parseInt(checkname);	 
	  for(int i=0;i<size;i++)
	  {
		 
		  if(cn==1)
		  {
			  String comname=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).getText();
			  System.out.println(comname);
		  if(comname.equals("Permanent")||comname.equals("Current Address"))
		  {
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).click();
			  Thread.sleep(2500);
			  driver.switchTo().frame("ctl00_ContentPlaceHolder1_rwStopCheck_C_txtStopCheckReason_contentIframe");
			  driver.findElement(By.xpath("html/body")).sendKeys("Not Valid");
			  Thread.sleep(1000);
			  driver.switchTo().defaultContent();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwStopCheck_C_btnStopCheckSubmit_input")).click();
			  Thread.sleep(3000);
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  Thread.sleep(3000);			 
			  break;
		  }
	  }
		  if(cn==2)
		  {
			  String comname=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).getText();
			  System.out.println(comname);
		  if(comname.equals("Highest 1")||comname.equals("UG1")||comname.equals("12th")||comname.equals("10th"))
		  {
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).click();
			  Thread.sleep(2500);
			  driver.switchTo().frame("ctl00_ContentPlaceHolder1_rwStopCheck_C_txtStopCheckReason_contentIframe");
			  driver.findElement(By.xpath("html/body")).sendKeys("Not Valid");
			  Thread.sleep(1000);
			  driver.switchTo().defaultContent();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwStopCheck_C_btnStopCheckSubmit_input")).click();
			  Thread.sleep(3000);
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  Thread.sleep(3000);			
			  break;
		  }
	  }
		  if(cn==3)
		  {
			  String comname=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).getText();
			  System.out.println(comname);
		  if(comname.equals("Previous Employment")||comname.equals("Current Employment"))
		  {
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).click();
			  Thread.sleep(2500);
			  driver.switchTo().frame("ctl00_ContentPlaceHolder1_rwStopCheck_C_txtStopCheckReason_contentIframe");
			  driver.findElement(By.xpath("html/body")).sendKeys("Not Required");
			  Thread.sleep(1000);
			  driver.switchTo().defaultContent();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwStopCheck_C_btnStopCheckSubmit_input")).click();
			  Thread.sleep(3000);
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  Thread.sleep(3000);			 
			  break;
		  }
	  }
		  if(cn==4)
		  {
			  String comname=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).getText();
			  System.out.println(comname);
		  if(comname.equals("Reference 1")||comname.equals("Reference 2"))
		  {
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).click();
			  Thread.sleep(2500);
			  driver.switchTo().frame("ctl00_ContentPlaceHolder1_rwStopCheck_C_txtStopCheckReason_contentIframe");
			  driver.findElement(By.xpath("html/body")).sendKeys("Not Valid");
			  Thread.sleep(1000);
			  driver.switchTo().defaultContent();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwStopCheck_C_btnStopCheckSubmit_input")).click();
			  Thread.sleep(3000);
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  Thread.sleep(3000);			 
			  break;
		  }
	  }
		  if(cn==5)
		  {
			  String comname=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).getText();
			  System.out.println(comname);
		  if(comname.equals("Voter ID")||comname.equals("PAN CARD")||comname.equals("Driving License"))
		  {
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).click();
			  Thread.sleep(2500);
			  driver.switchTo().frame("ctl00_ContentPlaceHolder1_rwStopCheck_C_txtStopCheckReason_contentIframe");
			  driver.findElement(By.xpath("html/body")).sendKeys("Not Valid");
			  Thread.sleep(1000);
			  driver.switchTo().defaultContent();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwStopCheck_C_btnStopCheckSubmit_input")).click();
			  Thread.sleep(3000);
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  Thread.sleep(3000);			 
			  break;
		  }
	  }
		  if(cn==6)
		  {
			  String comname=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).getText();
			  System.out.println(comname);
		  if(comname.equals("Permanent Court Check")||comname.equals("Current Address Court Check"))
		  {
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).click();
			  Thread.sleep(2500);
			  driver.switchTo().frame("ctl00_ContentPlaceHolder1_rwStopCheck_C_txtStopCheckReason_contentIframe");
			  driver.findElement(By.xpath("html/body")).sendKeys("Not Valid");
			  Thread.sleep(1000);
			  driver.switchTo().defaultContent();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwStopCheck_C_btnStopCheckSubmit_input")).click();
			  Thread.sleep(3000);
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  Thread.sleep(3000);			 
			  break;
		  }
	  }
		  if(cn==7)
		  {
			  String comname=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).getText();
			  System.out.println(comname);
		  if(comname.equals("Permanent Criminal Check")||comname.equals("Current Address Criminal Check"))
		  {
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).click();
			  Thread.sleep(2500);
			  driver.switchTo().frame("ctl00_ContentPlaceHolder1_rwStopCheck_C_txtStopCheckReason_contentIframe");
			  driver.findElement(By.xpath("html/body")).sendKeys("Not Valid");
			  Thread.sleep(1000);
			  driver.switchTo().defaultContent();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwStopCheck_C_btnStopCheckSubmit_input")).click();
			  Thread.sleep(3000);
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  Thread.sleep(3000);		
			  break;
		  }
	  }
		  if(cn==8)
		  {
			  String comname=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).getText();
			  System.out.println(comname);
		  if(comname.equals("Database")||comname.equals("Panel 1"))
		  {
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).click();
			  Thread.sleep(2500);
			  driver.switchTo().frame("ctl00_ContentPlaceHolder1_rwStopCheck_C_txtStopCheckReason_contentIframe");
			  driver.findElement(By.xpath("html/body")).sendKeys("Not Valid");
			  Thread.sleep(1000);
			  driver.switchTo().defaultContent();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwStopCheck_C_btnStopCheckSubmit_input")).click();
			  Thread.sleep(3000);
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  Thread.sleep(3000);	
			  break;
		  }
	  }
		  if(cn==9)
		  {
			  String comname=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).getText();
			  System.out.println(comname);
		  if(comname.equals("Credit Check 1")||comname.equals("Credit Check 3"))
		  {
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).click();
			  Thread.sleep(2500);
			  driver.switchTo().frame("ctl00_ContentPlaceHolder1_rwStopCheck_C_txtStopCheckReason_contentIframe");
			  driver.findElement(By.xpath("html/body")).sendKeys("Not Valid");
			  Thread.sleep(1000);
			  driver.switchTo().defaultContent();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwStopCheck_C_btnStopCheckSubmit_input")).click();
			  Thread.sleep(3000);
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  Thread.sleep(3000);	
			  break;
		  }
	  }
		  if(cn==10)
		  {
			  String comname=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).getText();
			  System.out.println(comname);
		  if(comname.equals("Medical Test")||comname.equals("Panel 1"))
		  {
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__"+i+"']/td[14]")).click();
			  Thread.sleep(2500);
			  driver.switchTo().frame("ctl00_ContentPlaceHolder1_rwStopCheck_C_txtStopCheckReason_contentIframe");
			  driver.findElement(By.xpath("html/body")).sendKeys("Not Valid");
			  Thread.sleep(1000);
			  driver.switchTo().defaultContent();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwStopCheck_C_btnStopCheckSubmit_input")).click();
			  Thread.sleep(3000);
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  Thread.sleep(3000);		
			  break;
		  }
		  		  
	  }
		  if(cn==11)
			  driver.findElement(By.id("ctl00_lnkLogout")).click();
		  
	  }  
		  
	 }while(size!=0);
		  
		  driver.close();
		  
		  
	}

}
