package Checks360;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Mastermis {
	WebDriverWait wait=new WebDriverWait(Driver.instance,50);
	String report=".//*[@id='ctl00_RadMenu2']/ul/li/a/span";
	String mastermis=".//*[@id='ctl00_RadMenu2']/ul/li/div/ul/li[6]/a/span";
	String search="ctl00_ContentPlaceHolder1_btnSearch";
	String clientarrow="ctl00_ContentPlaceHolder1_ddlClient_Arrow";
	String projectarrow="ctl00_ContentPlaceHolder1_ddlProject_Arrow";
	String casesourcearrow="ctl00_ContentPlaceHolder1_ddlWorkFlow_Arrow";
	String fromdate="ctl00_ContentPlaceHolder1_RadDatePickerInDateRangeFrom_dateInput";
	String todate="ctl00_ContentPlaceHolder1_RadDatePickerInDateRangeTo_dateInput";
	String exceldownload="ctl00_ContentPlaceHolder1_ImageButton2";
	String clientname[]=new String [50];
	String projectname[]=new String[50];
	String casesource[]=new String[10];
	List<String>  list= new ArrayList<String>();
	int clientsize,projectsize,casesourcesize,fun=0;
	String clientselection,projectselection,casesourceselection;
	String[] clientnames,projectnames,casesourcename;
	String date[]={"01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
	String month[]={"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	String year[]={"2010","2011","2012","2013","2014","2015","2016","2017","2018","2019","2020","2021","2022","2023","2024","2025"};
	String dates,months,years;
	static String misfromdate,mistodate;
	static String from_date,to_date;
	Date fromdates,todates;
	JFrame frames=new JFrame();
	JComboBox from_datelist = new JComboBox(date);
	JComboBox from_monthlist = new JComboBox(month);
	JComboBox from_yearlist = new JComboBox(year);
	JComboBox to_datelist = new JComboBox(date);
	JComboBox to_monthlist = new JComboBox(month);
	JComboBox to_yearlist = new JComboBox(year);
	JPanel from_myPanel = new JPanel();
	JPanel to_myPanel = new JPanel();
	public void mis() throws InterruptedException, IOException, ParseException
	{
		from_myPanel.add(new JLabel("Date"));
		from_myPanel.add(from_datelist);
		from_myPanel.add(Box.createHorizontalStrut(15)); // a spacer
		from_myPanel.add(new JLabel("Month"));
		from_myPanel.add(from_monthlist);
		from_myPanel.add(Box.createHorizontalStrut(15)); // a spacer
		from_myPanel.add(new JLabel("Year"));
		from_myPanel.add(from_yearlist);
		to_myPanel.add(new JLabel("Date"));
		to_myPanel.add(to_datelist);
		to_myPanel.add(Box.createHorizontalStrut(15)); // a spacer
		to_myPanel.add(new JLabel("Month"));
		to_myPanel.add(to_monthlist);
		to_myPanel.add(Box.createHorizontalStrut(15)); // a spacer
		to_myPanel.add(new JLabel("Year"));
		to_myPanel.add(to_yearlist);
		Thread.sleep(7000);
		Driver.instance.findElement(By.xpath(report)).click();
		Thread.sleep(1000);
		Driver.instance.findElement(By.xpath(mastermis)).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(search)));
//////////////////////////////////////////////Client Selection///////////////////////////////////////////////////////////
		Driver.instance.findElement(By.id(clientarrow)).click();
		Thread.sleep(800);
		clientsize=Driver.instance.findElements(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlClient_DropDown']/div/ul/li")).size();
		System.out.println("Clientsize="+clientsize);
		for(int size=1;size<=clientsize;size++)
		{
			clientname[size]=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlClient_DropDown']/div/ul/li["+size+"]")).getText();
			System.out.println("Clientname="+clientname[size]);
		
		}
		list.clear();
		for(String clientvalue:clientname)
		{
			if(clientvalue != null && clientvalue.length() > 0) {
		          list.add(clientvalue);
		       }
		}
		clientnames = list.toArray(new String[list.size()]);
		clientselection=(String) JOptionPane.showInputDialog(new JFrame(), "Choose the Client","Selection", JOptionPane.QUESTION_MESSAGE, null,clientnames,clientnames[0]);
		Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlClient_DropDown']/div/ul/li[contains(text(),'"+clientselection+"')]")).click();
		////////////////////////////////////////////////Project Selection///////////////////////////////////////////////////////////
		Driver.instance.findElement(By.id(projectarrow)).click();
		Thread.sleep(800);
		projectsize=Driver.instance.findElements(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlProject_DropDown']/div/ul/li")).size();
		System.out.println("projectsize="+projectsize);
		for(int size=1;size<=projectsize;size++)
		{
			projectname[size]=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlProject_DropDown']/div/ul/li["+size+"]")).getText();
			System.out.println("projectname="+projectname[size]);
		
		}
		list.clear();
		for(String projectvalue:projectname)
		{
			if(projectvalue != null && projectvalue.length() > 0) {
		          list.add(projectvalue);
		       }
		}
		projectnames = list.toArray(new String[list.size()]);
		projectselection=(String) JOptionPane.showInputDialog(new JFrame(), "Choose the Project","Selection", JOptionPane.QUESTION_MESSAGE, null,projectnames,projectnames[0]);
		Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlProject_DropDown']/div/ul/li[contains(text(),'"+projectselection+"')]")).click();
//////////////////////////////////////////////CaseSource Selection///////////////////////////////////////////////////////////
		Driver.instance.findElement(By.id(casesourcearrow)).click();
		Thread.sleep(800);
		casesourcesize=Driver.instance.findElements(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlWorkFlow_DropDown']/div/ul/li")).size();
		System.out.println("casesourcesize="+casesourcesize);
		for(int size=1;size<=casesourcesize;size++)
		{
			casesource[size]=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlWorkFlow_DropDown']/div/ul/li["+size+"]")).getText();
			System.out.println("casesource="+casesource[size]);
		
		}
		list.clear();
		for(String casesourcevalue:casesource)
		{
			if(casesourcevalue != null && casesourcevalue.length() > 0) {
		          list.add(casesourcevalue);
		       }
		}
		casesourcename = list.toArray(new String[list.size()]);
		casesourceselection=(String) JOptionPane.showInputDialog(new JFrame(), "Choose the Case Source","Selection", JOptionPane.QUESTION_MESSAGE, null,casesourcename,casesourcename[0]);
		Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlWorkFlow_DropDown']/div/ul/li[contains(text(),'"+casesourceselection+"')]")).click();
		fromtodate();
		if(fun==1)
		{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(search))).click();
		try
		{
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(exceldownload))).click();
		Excel_write.inputExcel("Excel Download", "Pass");
		}catch(Exception e)
		{
			Excel_write.inputExcel("Excel Download", "Fail - Due to export button not visible");
		}
	}
	}
	public void fromtodate() throws ParseException
	{
		fun=1;
		JOptionPane.showConfirmDialog(null, from_myPanel,"Choose From Date", JOptionPane.OK_CANCEL_OPTION);
		Driver.instance.findElement(By.id(fromdate)).clear();
		String from_date=from_datelist.getSelectedItem()+"/"+from_monthlist.getSelectedItem()+"/"+from_yearlist.getSelectedItem();
		Driver.instance.findElement(By.id(fromdate)).sendKeys(from_date);
		JOptionPane.showConfirmDialog(null, to_myPanel,"Choose To Date", JOptionPane.OK_CANCEL_OPTION);
		Driver.instance.findElement(By.id(todate)).clear();
		String to_date=to_datelist.getSelectedItem()+"/"+to_monthlist.getSelectedItem()+"/"+to_yearlist.getSelectedItem();
		Driver.instance.findElement(By.id(todate)).sendKeys(to_date);
		
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy");
		try {
			fromdates = dateFormat.parse(from_date);
			todates = dateFormat.parse(to_date);
		    dateFormat=new SimpleDateFormat("dd/MM/yyyy");
		    misfromdate=dateFormat.format(fromdates);
		    mistodate=dateFormat.format(todates);
		    //System.out.println("Date :"+misfromdate+mistodate);
		}catch (Exception e) {
		    // TODO: handle exception
		    e.printStackTrace();
		}
		fromdates = dateFormat.parse(misfromdate);
		todates = dateFormat.parse(mistodate);
		if(fromdates.after(todates))
		{
		fun=0;
		JOptionPane.showMessageDialog(null, "From date should be lesser than To date");
		Driver.instance.findElement(By.xpath("//span[contains(.,'OK')]")).click();
		fromtodate();
		}
	}
	                                                                                                            
}
