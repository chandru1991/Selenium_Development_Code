package Checks360;

import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.testng.Assert;

public class Excel_write {
	 static Row row;
     static Cell cell;
     static Workbook workbook;
     static Sheet sheet;
     static Font font;
     static  CellStyle style;
     static FileOutputStream fileOut;
     static int rows=1;
	public static void createExcel(String filename,String header,String result) throws IOException
	{
		TestNG.datefun();
		workbook = new HSSFWorkbook();
		//fileOut = new FileOutputStream("/home/pyr/Script_New/TestNg_Output/"+filename+"_"+TestNG.currentdate+".xlsx");
		fileOut = new FileOutputStream("/home/pyr/Script_New/TestNg_Output/"+filename+".xlsx");
		sheet = workbook.createSheet("Output");
		font = workbook.createFont();
        style = workbook.createCellStyle();
        row = sheet.createRow(0);
		cell = row.createCell(0);            
		cell.setCellValue(header); 
		font.setBold(true);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		cell.setCellStyle(Excel_write.style);
		cell = row.createCell(1);            
		cell.setCellValue(result); 
		font.setBold(true);
		cell.setCellStyle(Excel_write.style);
		rows=1;
	}
	public static void inputExcel(String name, String output) throws IOException
	{
			row = sheet.createRow(rows++);
			cell = row.createCell(0);
			cell.setCellValue(name); 	
			cell = row.createCell(1);
			cell.setCellValue(output); 
	}
	public static void writeExcel() throws IOException
	{
		workbook.write(fileOut);
    	fileOut.close();
	}
}
