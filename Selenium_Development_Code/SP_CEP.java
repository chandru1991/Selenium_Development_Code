package check360_flow;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SP_CEP {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		WebElement sd;
		driver.get("http://192.168.2.17:96");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
		Thread.sleep(7000);
		String rno=JOptionPane.showInputDialog("Enter Reference number:\n");
		String raise=JOptionPane.showInputDialog("Select stages to raise CEP"+"\n"+"1.Data Entry"+"\n"+"2.Data Entry QC");
		int cep=Integer.parseInt(raise);
		WebElement w1= driver.findElement(By.id("ddlAct"));	 
		if(cep==1)
		{		
		Select s= new Select(w1);
		s.selectByValue("2");
		Thread.sleep(1000);
		}
		else if(cep==2)
		{
			Select s= new Select(w1);
			s.selectByValue("4");
			Thread.sleep(1000);
		}
		WebElement w2= driver.findElement(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[4]/select"));
		 Select s1=new Select(w2);
		 s1.selectByValue("1");
		 Thread.sleep(2500);
		 driver.findElement(By.id("txtCaserefNo")).clear();
			driver.findElement(By.id("txtCaserefNo")).sendKeys(rno);
			driver.findElement(By.id("btnsearch")).click();
			Thread.sleep(5000);
			String Checkname;
			  int wait;
			  int TabCheck;
			  int dcc = 0;
			  String CreditID; 
			 wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).size();
			//System.out.println(wait);
			if(wait!=0)
			{
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).click();
			}
			Thread.sleep(5500);
			 TabCheck = driver.findElements(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).size();
			 System.out.println("No of Checks in the Case:"+TabCheck);	
			 for(int tc=1;tc<=TabCheck;tc++)
			 {
				 if(cep==1)
				 {
			 	//System.out.println(tc);
			 	driver.switchTo().defaultContent();
			 	Thread.sleep(2000);
			 	  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).getText();
			 	  System.out.println("Checkname"+Checkname);
			 	 if (Checkname.equals("Employment"))
				  {
			 		 driver.switchTo().defaultContent();
			 		 driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
					 Thread.sleep(1500);
				  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[3]/iframe")); 
				  driver.switchTo().frame(element);
				  Thread.sleep(3000);
				  if(!driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentCEPRemarks")).isEnabled())
				  {
				  Thread.sleep(2000);				 
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).click();
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys("TCS");
				  Thread.sleep(2000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys(Keys.DOWN);
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys(Keys.ENTER);
				  Thread.sleep(2500);
					
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeID")).sendKeys("554545");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeDesignation")).sendKeys("Tester");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeDepartment")).sendKeys("Testing");
				  
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtFromDate_dateInput")).sendKeys("2013-12-05");
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkTillDate")).click();
				  Thread.sleep(2500);
					
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtLastSalaryDrawn")).sendKeys("4 Lakhs");
				  
				  sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCurrencyType_Input"));
				  sd.sendKeys("USD");
				  sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentType_Input"));
				  sd.sendKeys("Permanent");
				  sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmployeeSalaryType_Input"));
				  sd.sendKeys("Per Month");
				  sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRelationship1_Input"));
				  sd.sendKeys("HR");
						
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1Name")).sendKeys("Ravi");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1Designation")).sendKeys("HR");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1ContactNo1")).sendKeys("9632587401");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1EmailID1")).sendKeys("ravi@gmail.com");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtReasonOfLeaving")).sendKeys("Salary");
				  Thread.sleep(2500);
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkCanVerifyNow")).click();
				  Thread.sleep(3500);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRelievingDate_dateInput")).sendKeys("25/12/2017");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentCEPRemarks")).sendKeys("Contact after given date");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSubmit_input")).click();
				  Thread.sleep(3500);
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
				  Thread.sleep(3500);
				  }
			 		else
			 		{
			 			 driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSubmit_input")).click();
						  Thread.sleep(3500);
						  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
						  Thread.sleep(3500);
			 		}
				  }
			 	 else if (Checkname.equals("Reference"))
				  {
			 		 driver.switchTo().defaultContent();
			 		driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
					 Thread.sleep(1500);
			 	 WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[4]/iframe")); 
				  driver.switchTo().frame(element);
				  Thread.sleep(3000);
				  if(!driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportYTR")).isEnabled())
				  {
		 		Thread.sleep(2000);
				  
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlReferType_Input")).click();
				  Thread.sleep(1000);
				  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlReferType_DropDown']/div/ul/li[contains(text(), 'Academic')]")).click();
				  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtReferenceName")).sendKeys("Referer  Name");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefDes")).sendKeys("Referer  Designation");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefContactNo1")).sendKeys("9632014587");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefEmailId1")).sendKeys("ref@gmail.com");
				
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).clear();
			  
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).sendKeys("India");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).sendKeys(Keys.ENTER);
			  Thread.sleep(3000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).click();
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).sendKeys("Tamil Nadu");
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).sendKeys(Keys.ENTER);
			  Thread.sleep(3000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).sendKeys("Chennai");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).sendKeys(Keys.ENTER);
			  Thread.sleep(3000);

			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).sendKeys("PSYEC");
			  Thread.sleep(2000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).sendKeys(Keys.DOWN);
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).sendKeys(Keys.ENTER);
			  Thread.sleep(2500);
				  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_CheckRefReportYTR")).click();
				  Thread.sleep(2000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtExpectedRelievingDate_dateInput")).sendKeys("25/12/2016");
				  Thread.sleep(1000);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportYTR")).sendKeys("Conatct after given date refence");
				 Thread.sleep(1500);
				 driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
				 Thread.sleep(3500);
				 driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
					Thread.sleep(3000);
				  }
			 		 else
			 		 {
			 			 driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
						 Thread.sleep(3500);
						 driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
							Thread.sleep(3000);
			 		 }
				  }
			 }
				 else if(cep==2)
				 {
					 driver.switchTo().defaultContent();
					 	Thread.sleep(2000);
					 	  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).getText();
					 	  System.out.println("Checkname"+Checkname);
					 	 if (Checkname.equals("Employment"))
						  {
							  Thread.sleep(2000);
							  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
							 Thread.sleep(3500);
						 WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[3]/iframe")); 
						 driver.switchTo().frame(element);
						  Thread.sleep(3000);
						  if(!driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentCEPRemarks")).isEnabled())
						  {
						  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkCanVerifyNow")).click();
						  Thread.sleep(5500);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRelievingDate_popupButton")).click();
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRelievingDate_calendar_NN")).click();
						  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_txtRelievingDate_calendar_Top']/tbody/tr[3]/td[4]/a")).click();
						  //driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRelievingDate_dateInput")).sendKeys("20/12/2016");
						  Thread.sleep(2500);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentCEPRemarks")).sendKeys("Contact after date");
						  Thread.sleep(2000);
						  }
						  try
						  {
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSubmit_input")).click();
						  WebDriverWait await = new WebDriverWait(driver, 15);
						  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
						  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
						  }
						  catch(NoAlertPresentException e)
						  {
							  driver.switchTo().alert().accept();
						  }
						  
						  }
						  
					 	 else if (Checkname.equals("Reference"))
						  {
					 		Thread.sleep(2000);
							  driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li["+tc+"]/a/span/span/span")).click();
							 Thread.sleep(3500);
						  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[4]/iframe")); 
						  driver.switchTo().frame(element);
						  Thread.sleep(3000);
						  if(!driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportYTR")).isEnabled())
						  {
							  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_CheckRefReportYTR")).click();
						  Thread.sleep(3500);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtExpectedRelievingDate_popupButton")).click();
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtExpectedRelievingDate_calendar_NN")).click();
						  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_txtExpectedRelievingDate_calendar_Top']/tbody/tr[4]/td[5]/a")).click();
						  Thread.sleep(2500);
						 // driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtExpectedRelievingDate_dateInput")).sendKeys("20/12/2017");
						 // Thread.sleep(2000);
						  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportYTR")).sendKeys("Contact after date");
						  Thread.sleep(2000);
						  }						  
						  try
						  {
							  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
							  WebDriverWait await = new WebDriverWait(driver, 15);
							  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
							  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();						  
							  
						  }
						  catch(NoAlertPresentException e)
						  {
							  driver.switchTo().alert().accept();
						  }
						  }
				 }
			 }
			 Thread.sleep(3500);
			driver.findElement(By.id("imgHome")).click();
			Thread.sleep(5000);
			driver.findElement(By.id("btnActions")).click();
			Thread.sleep(3000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
			Thread.sleep(500);
			driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[4]")).click();
			Thread.sleep(1500);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlWorkflowType_Input")).click();
			Thread.sleep(500);
			driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[1]")).click();
			Thread.sleep(1000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
			Thread.sleep(2000);
			 wait = driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).size();
				//System.out.println(wait);
				if(wait!=0)
				{
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).click();
									
				Thread.sleep(4500);
				String doc=driver.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00__0']/td[6]")).getText();
				if(doc.isEmpty())
				{
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/sddf.pdf");
				Thread.sleep(1500);
				}
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_btnAddDocument")).click();
				Thread.sleep(1500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_txtClearedRemarks")).sendKeys("Ready to contact");
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_ButtonSubmit_input")).click();
				Thread.sleep(3500);
				driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
				Thread.sleep(2000);
				}					
				//////////////////////////////////////////////////////////////////////////////////////////////////
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlWorkflowType_Input")).click();
				Thread.sleep(500);
				driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[1]")).click();
				Thread.sleep(1000);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
				Thread.sleep(2000);
				 wait = driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).size();
					//System.out.println(wait);
					if(wait!=0)
					{
					Thread.sleep(1000);
					driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).click();
					
					Thread.sleep(4500);
					String doc=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00__0']/td[6]")).getText();
					if(doc.isEmpty())
					{
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/sddf.pdf");
					Thread.sleep(1500);
					}
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_btnAddDocument")).click();
					Thread.sleep(1500);
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_txtClearedRemarks")).sendKeys("Ready to contact");
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_ButtonSubmit_input")).click();
					Thread.sleep(3500);
					driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
					Thread.sleep(2000);
					}
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnStages_input")).click();					
					
				  
	}

}
