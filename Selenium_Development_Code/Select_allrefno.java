package Checks360;

import java.io.File;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Select_allrefno extends Case_Registration {
	static int get=0,casesize,count=1,pagesize,nextpage;
	static String pref,refno;
	public void ref() throws Exception
	{
		//Thread.sleep(10000);
		new Select(Driver.instance.findElement(By.id(stagedropdown))).selectByValue("2");
		Thread.sleep(2000);
		casesize=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[2]/span")).size();
		//pref=JOptionPane.showInputDialog("Enter Reference Number\n");
		String page=Driver.instance.findElement(By.xpath(".//*[@id='angularPage']/div/div/div/div[2]/table/thead/tr/td[2]/span[2]")).getText();
		pagesize = Integer.valueOf(page);
		nextpage=Driver.instance.findElements(By.xpath(".//*[@id='angularPage']/div/div/div/div[2]/table/thead/tr/td[1]/ul/li/a")).size();	
		//Driver.instance.findElement(By.cssSelector("//[class='ng-pristine ng-valid']")).sendKeys("arun");
		pagecheck();
		if(count==pagesize)
		{
		JOptionPane.showMessageDialog(null, "Screenshot taken for all reference number","Alert", 0);
		}
	}
	public void pagecheck() throws Exception
	{
		for(int caseref=1;caseref<=casesize;caseref++)
		{
			
			refno=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+caseref+"]/td[2]/span")).getText();
			//System.out.println(refno);
			//if(pref.trim().equals(refno.trim()))
			//{
				String refno1=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+caseref+"]/td[3]/span")).getText();
				String refno2=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+caseref+"]/td[4]/span")).getText();
				String refno3=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+caseref+"]/td[5]/span")).getText();
				String refno4=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+caseref+"]/td[6]/span")).getText();
				String refno5=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+caseref+"]/td[8]/span")).getText();
				String refno6=Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+caseref+"]/td[9]/span")).getText();
				Driver.instance.findElement(By.xpath(".//*[@id='grdTaskList']/tbody/tr["+caseref+"]/td[16]/a")).click();
				//((JavascriptExecutor) instance.Driver).executeScript("scroll(0,250);");
				Thread.sleep(1000); 
				Scrolldown();
				//get=1;
				Driver.instance.findElement(By.xpath("html/body/div[3]/div/div/div/button")).click();
				Thread.sleep(1000);
				System.out.println();
				System.out.println(refno+"\t"+refno1+"\t"+refno2+"\t"+refno3+"\t"+refno4+"\t"+refno5+"\t"+refno6);
				System.out.println();
				//break;
			//}		
			if((caseref==casesize) && (count!=pagesize))
			{
				conditioncheck();
			}
		}	
	}
	public void conditioncheck() throws Exception
	{
			Driver.instance.findElement(By.xpath(".//*[@id='angularPage']/div/div/div/div[2]/table/thead/tr/td[1]/ul/li["+(nextpage-1)+"]/a")).click();
			Thread.sleep(4000);
			casesize=Driver.instance.findElements(By.xpath(".//*[@id='grdTaskList']/tbody/tr/td[2]/span")).size();
			count++;
			pagecheck();
	}
	public  void Scrolldown() throws Exception{
		float componentsize=Driver.instance.findElements(By.xpath("html/body/div[3]/div/div/div/div/div[2]/table/tbody/tr")).size();
		System.out.println("componentsize="+componentsize);
		float totalsize=componentsize/8;
		System.out.println("totalsize="+totalsize);
		
		/*List<WebElement> e=Driver.instance.findElements(By.xpath("html/body/div[3]/div/div/div/div/div[2]/table/tbody/tr"));
		int i=1;
		for(WebElement s:e)
		{
			String s1=s.getText();
			System.out.println(i+"="+s1);
			i++;
		}*/
		//System.out.println(componentsize);
		int i=1,scrollvalue=400;
		if(totalsize-(int)totalsize!=0)
		{
			totalsize=totalsize+1;
		}
		while(i<=totalsize)
		{
		WebElement scrollArea = Driver.instance.findElement(By.className("inner-table"));
		JavascriptExecutor js = (JavascriptExecutor) Driver.instance; 
		js.executeScript("arguments[0].scrollTop = arguments[1];",scrollArea, scrollvalue); 
		Thread.sleep(1000); 
		getscreenshot(i);
		scrollvalue+=400;
		i++;
		}
}
	public static void getscreenshot(int getval) throws Exception 
    {
   	
            File scrFile = ((TakesScreenshot)Driver.instance).getScreenshotAs(OutputType.FILE);
           String screnshot=refno.concat("_"+getval);  
            //System.out.println(screnshot);
            FileUtils.copyFile(scrFile, new File("/home/pyr/Script_New/Screenshot/"+screnshot+".png"));
    }
}