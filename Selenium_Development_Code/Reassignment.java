package check360_flow;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Reassignment {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();
	      driver.manage().window().maximize();
	      driver.get("http://192.168.2.17:96");
	      driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
		  Thread.sleep(10000);
		  driver.findElement(By.id("btnActions")).click();
		  Thread.sleep(3000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[6]")).click();
		  Thread.sleep(2500);
		  String rno=JOptionPane.showInputDialog("Enter Reference number");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlUsers_Input")).click();
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlUsers_DropDown']/div/ul/li[contains(text(), 'demo employee')]")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
		  Thread.sleep(2500);
		  int wait = driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[7]")).size();
			//System.out.println(wait);
			 if(wait!=0)
			 {
			  Thread.sleep(1000);
			  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl04_chkSelectBox")).click();
			  Thread.sleep(500);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnReassign_input")).click();
			  Thread.sleep(1500);
			  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rdwCaseAssignment_C_grdCaseAssignmentResource_ctl00_ctl04_SelectedEmployeeSelectCheckBox")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwCaseAssignment_C_btnsubmit_input")).click();
			  Thread.sleep(2500);
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  Thread.sleep(3500);
			 }
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnStages_input")).click();
	}

}
