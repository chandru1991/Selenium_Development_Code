package check360_flow;

import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;  
import java.util.ArrayList;  
import org.openqa.selenium.By;  
import org.openqa.selenium.WebDriver;  
import org.openqa.selenium.firefox.FirefoxDriver;  
import org.testng.annotations.BeforeTest;  
import org.testng.annotations.Test;

public class Viewfinalreport {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  String curwindow=null;
  @BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    //baseUrl = "http://192.168.2.17:96/";
    baseUrl= "http://paws2.com";
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }
  @Test
  public void testViewfinalreport() throws Exception {
    driver.get(baseUrl + "/Login.aspx");
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
    Thread.sleep(2500);
    driver.findElement(By.id("btnActions")).click();
    Thread.sleep(1000);
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Arrow")).click();
    Thread.sleep(500);
    driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[contains(text(), 'View Final Report Document')]")).click();
    Thread.sleep(1000);
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
    String rno=JOptionPane.showInputDialog("Enter Reference Number:");
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
    Thread.sleep(1000);
//     curwindow = driver.getWindowHandle();
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCaseList_ctl00_ctl04_btnViewdoc")).click();
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdmAddDoc_C_grdFinalReportDocument_ctl00_ctl04_btnViewdoc")).click();
    //driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdmAddDoc_C_btnCloseWindow")).click();
    Thread.sleep(5000);
    Robot rob=new Robot();
    rob.keyPress(KeyEvent.VK_DOWN);
    Thread.sleep(200);
    rob.keyRelease(KeyEvent.VK_DOWN);
    rob.keyPress(KeyEvent.VK_ENTER);
    Thread.sleep(200);
    rob.keyRelease(KeyEvent.VK_ENTER);
for(String newwindow : driver.getWindowHandles()){
driver.switchTo().window(newwindow);
}
  }
}