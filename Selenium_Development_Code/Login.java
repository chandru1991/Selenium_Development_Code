package Project_Flow;

import org.openqa.selenium.By;
import Project_Path.Loginpage_path;

public class Login {
	
	public static void login(String uname,String pwd)
	{
		Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
		Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(uname);
		Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
		Driver.instance.findElement(By.xpath(Loginpage_path.password)).sendKeys(pwd);
	}
	public static void sp_login(String username,String password)
	{
		login(username,password);
		Driver.instance.findElement(By.xpath(Loginpage_path.sp_login)).click();
	}
	public static void client_login(String username,String password)
	{
		login(username,password);
		Driver.instance.findElement(By.xpath(Loginpage_path.client_login)).click();
	}
	public static void candidate_login(String username,String password)
	{
		login(username,password);
		Driver.instance.findElement(By.xpath(Loginpage_path.candidate_login)).click();
	}
	
}
