package check360_flow;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class QuickSubmit {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://192.168.2.17:96/clientlogin.aspx");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("democlient");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@360");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
		Thread.sleep(3000);
		String rno= JOptionPane.showInputDialog("Enter Reference number");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
		Thread.sleep(1000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCandidateList_ctl00_ctl04_btnSubmitAllComponents")).click();
		Thread.sleep(500);
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_txtClientComments")).sendKeys("Verified");
		JPanel mypanel=new JPanel();
		JLabel label=new JLabel("Do you want to set Priority as high");
		mypanel.add(label);
		int priority=JOptionPane.showConfirmDialog(null,mypanel,"Alert", JOptionPane.YES_NO_OPTION);
		//String priority=JOptionPane.showInputDialog("Do you want to set Priority as high Y/N");
		if(priority == JOptionPane.YES_OPTION)
		{
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_ddlPriority_Input")).sendKeys("High");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_txtClientComments")).sendKeys("Do it as soon possible before the date 24/09/2017 candidate must be verified before the given date make it ASAP take it as high priority and complete it???Thank you");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_btnComponentSave")).click();
		WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
		  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
		  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		  Thread.sleep(3000);
		}
		else
		{
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_txtClientComments")).sendKeys("24.09.2017 Limited period!!!");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_btnComponentSave")).click();
		WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
		  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
		  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		  Thread.sleep(3000);
	}
		driver.findElement(By.id("ctl00_lnkLogout")).click();
		Thread.sleep(4000);
	  

	}

}
