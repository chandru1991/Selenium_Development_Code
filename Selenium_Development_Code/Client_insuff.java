package check360_flow;

import javax.swing.JOptionPane;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Client_insuff {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();
		driver.manage().window().maximize();
		//driver.get("http://192.168.1.22/clientlogin.aspx");
		driver.get("http://192.168.2.17:96");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("democlient");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@360");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
		Thread.sleep(8000);
		String rno= JOptionPane.showInputDialog("Enter Reference number");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCandidateList_ctl00_ctl04_btnViewAllComponents")).click();
		Thread.sleep(5000);
		driver.findElement(By.id("btnApplyChanges_input")).click();
		Thread.sleep(2500);
		for(int i=0;i<=27;i++)
		{
			try
			{
				Thread.sleep(1500);
		String Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
		System.out.println("Check="+Checkname);		
		if(Checkname.equals("Basic Details"))
		{
			driver.switchTo().frame(0);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtFatherLastName")).clear();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtFatherLastName")).sendKeys("Arun");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSaveNext_input")).click();
			Thread.sleep(2500);
		}
		if(Checkname.equals("Address"))
		{
			 driver.switchTo().frame(0);
			String comname=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblComponentName")).getText();
			System.out.println("comname"+comname);
			if(comname.equals("Permanent"))
			{
				
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressAddress")).clear();
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressAddress")).sendKeys("No:1, Sakthi Nagar");
				driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkAddressInsuff")).click();
				Thread.sleep(1000);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressInsuffRemark")).sendKeys("Not sufficient data");
				Thread.sleep(500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit_input")).click();
				Thread.sleep(2500);
			}
			else if(comname.equals("Current Address"))
			{
				
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCity_Input")).clear();
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCity_Input")).sendKeys("Pudukkottai");
				driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkAddressInsuff")).click();
				Thread.sleep(2000);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressInsuffRemark")).sendKeys("Not sufficient data");
				Thread.sleep(500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit_input")).click();
				Thread.sleep(2500);
			}
			else
			{
				
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit_input")).click();
			Thread.sleep(2500);
			}
		}
		else if(Checkname.equals("Education"))
		{
			driver.switchTo().frame(0);
			String comname=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblComponent")).getText();
			if(comname.equals("10th")||comname.equals("Highest1"))
			{
				//driver.switchTo().frame(0);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).clear();
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys("karnataka");
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys(Keys.DOWN);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys(Keys.DOWN);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys(Keys.ENTER);
				Thread.sleep(2000);
				driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkEducationRaiseInsufficency")).click();
				Thread.sleep(1500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationRaiseInsufficency")).sendKeys("Insufficient data");
				Thread.sleep(1500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveContinue_input")).click();
				Thread.sleep(2500);
			}
			else if(comname.equals("12th")||comname.equals("Highest2"))
			{
				//driver.switchTo().frame(0);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCGPA")).sendKeys("80%");
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnUploadDocument_input")).click();
				Thread.sleep(1000);
				
				String str1=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_gviewEducationDocument_ctl00__0']/td[5]/table")).getText();
				if(str1.isEmpty())
				{
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_gviewEducationDocument_ctl00_ctl04_rauEducationComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/123.pdf");
					Thread.sleep(2000);
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_btnEducationAddDocument_input")).click();
					Thread.sleep(1500);
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_btnEducationDocumentCancel")).click();
					Thread.sleep(1500);
				}
				else
				{
					//driver.switchTo().frame(0);
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_btnEducationDocumentCancel")).click();
					Thread.sleep(2500);
				}
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveContinue_input")).click();
				Thread.sleep(1500);				
			}
			else
			{
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveContinue_input")).click();
				Thread.sleep(2500);	
			}
			}
		else if(Checkname.equals("Employment"))
		{
			Thread.sleep(2500);
			driver.switchTo().frame(0);
			
			String comname=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblEmploymentComponent")).getText();
			if(comname.equals("Current Employment"))
			{
				//driver.switchTo().frame(0);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).clear();
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys("infosys");
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys(Keys.DOWN);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys(Keys.ENTER);
			Thread.sleep(1000);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnUploadDocument_input")).click();
			Thread.sleep(2500);
			String str2=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_gviewEmploymentDocument_ctl00__1']/td[5]")).getText();
			if(str2.isEmpty())
			{
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_gviewEmploymentDocument_ctl00_ctl06_rauEmploymentComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/fgg.pdf");
			Thread.sleep(2000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_btnEmploymentAddDocument_input")).click();
			Thread.sleep(2500);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_btnEmploymentDocumentClose")).click();
			Thread.sleep(2500);
			}
			else
			{
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_btnEmploymentDocumentClose")).click();
				Thread.sleep(1500);
			}			
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSaveContinue_input")).click();
			Thread.sleep(3500);
			}
			else if(comname.equals("Previous Employment"))
			{
				//driver.switchTo().frame(0);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeDesignation")).clear();
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeDesignation")).sendKeys("Testing Engineer");
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentType_Input")).sendKeys("Contract");
				driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkEmploymentRaiseInsufficency")).click();
				Thread.sleep(1500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentInsuff")).sendKeys("Insufficient data");
				Thread.sleep(1500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSaveContinue_input")).click();
				Thread.sleep(2500);
			}
			else
			{
				//driver.switchTo().frame(0);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSaveContinue_input")).click();
				Thread.sleep(2500);
			}
		}
		else if(Checkname.equals("Identity"))
		{
			driver.switchTo().frame(0);
			String comname=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblIdentityComponentName")).getText();
			if(comname.equals("Voter ID"))
			{
				//driver.switchTo().frame(0);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdNumber")).sendKeys("VID098678J76");
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdAddDocuments_input")).click();
				Thread.sleep(1000);
				String str3=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rwmCaseIDDocuments_C_grdviewIdDocument_ctl00__1']/td[5]")).getText();
				if(str3.isEmpty())
				{
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseIDDocuments_C_grdviewIdDocument_ctl00_ctl06_rauIdComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/test2.pdf");
					Thread.sleep(2000);
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseIDDocuments_C_btnIDAddDocument_input")).click();
					Thread.sleep(2500);
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseIDDocuments_C_btnIdDocumentCancel")).click();
					Thread.sleep(2500);
				}
				else
				{
					//driver.switchTo().frame(0);
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseIDDocuments_C_btnIdDocumentCancel")).click();
					Thread.sleep(1500);
				}
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveAndNext_input")).click();
				Thread.sleep(2500);
			}
			else if(comname.equals("Driving License"))
			{
				//driver.switchTo().frame(0);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdIssueDate_dateInput")).sendKeys("12/12/2013");
				Thread.sleep(1500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdExpireDate_dateInput")).sendKeys("12/12/2016");
				Thread.sleep(1650);
				driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkIdRaiseInsufficency")).click();
				Thread.sleep(1500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdRaiseInsufficency")).sendKeys("Insufficient data");
				Thread.sleep(1500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveAndNext_input")).click();
				Thread.sleep(2500);
			}
			else
			{			
				//driver.switchTo().frame(0);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveAndNext_input")).click();
				Thread.sleep(2500);
			}
		}
		else if(Checkname.equals("Reference"))
		{
			Thread.sleep(2500);
			driver.switchTo().frame(0);
			Thread.sleep(1500);
			String comname=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblRefComponentName")).getText();
			if(comname.equals("Reference 2"))
			{
				//driver.switchTo().frame(0);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).clear();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).sendKeys("Karnataka");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).sendKeys(Keys.ENTER);
			Thread.sleep(1000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).clear();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).sendKeys("pune");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).sendKeys(Keys.ENTER);
			Thread.sleep(1000);
			driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_CheckRefReportInsuff")).click();
			Thread.sleep(1500);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefReportInsuff")).sendKeys("Insufficient data");
			Thread.sleep(1500);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
			Thread.sleep(2500);
			}
			else
			{
				//driver.switchTo().frame(0);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
				Thread.sleep(2500);
			}
				
		}
		else if(Checkname.equals("Gap Details"))
		{
			driver.switchTo().frame(0);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSaveNext_input")).click();
			Thread.sleep(3500);
		}
		else if(Checkname.equals("LOA"))	
		{
			//driver.switchTo().frame(0);
			driver.findElement(By.id("btnCaseDocument")).click();
			Thread.sleep(2000);
			String str=driver.findElement(By.xpath("//*[@id='rwCaseDocument_C_grdCaseDocument_ctl00__1']/td[5]")).getText();
			if(str.isEmpty())
			{
				driver.findElement(By.id("rwCaseDocument_C_grdCaseDocument_ctl00_ctl06_rauCaseDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/test7.pdf");
				Thread.sleep(2000);
				driver.findElement(By.id("rwCaseDocument_C_btnAddCaseDocument_input")).click();
				Thread.sleep(2500);
				driver.findElement(By.xpath("//*[@id='RadWindowWrapper_rwCaseDocument']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
				Thread.sleep(1500);
				}
			else
			{
				//driver.switchTo().frame(0);
				driver.findElement(By.xpath("//*[@id='RadWindowWrapper_rwCaseDocument']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
				Thread.sleep(1500);
			}
			String notify=JOptionPane.showInputDialog("Select"+"\n"+"1.Notify insuff submit CIF"+"\n"+"2.Notify insuff only");
				int no=Integer.parseInt(notify);
			if(no==1)
			{
					driver.findElement(By.id("btnSave")).click();
				Thread.sleep(2500);
				String option=JOptionPane.showInputDialog("Do you want set priority high Y/N");
				if(option.equals("Y")||option.equals("y"))
				{
				driver.findElement(By.id("rwVerificationReadyComponent_C_ddlPriority")).click();
				driver.findElement(By.id("rwVerificationReadyComponent_C_ddlPriority")).sendKeys(Keys.DOWN);
				driver.findElement(By.id("rwVerificationReadyComponent_C_ddlPriority")).sendKeys(Keys.ENTER);
				driver.findElement(By.id("rwVerificationReadyComponent_C_txtClientComments")).sendKeys("High concentrate");
				driver.findElement(By.id("rwVerificationReadyComponent_C_btnComponentSave")).click();
				Thread.sleep(2500);
				 Alert alert2 = driver.switchTo().alert();
				  alert2.accept();
				  Thread.sleep(2500);
				  driver.findElement(By.id("ctl00_lnkLogout")).click();
				}
				else
				{
					driver.findElement(By.id("rwVerificationReadyComponent_C_btnComponentSave")).click();
					Thread.sleep(2500);
					 Alert alert2 = driver.switchTo().alert();
					  alert2.accept();
				Thread.sleep(2500);
				driver.findElement(By.id("ctl00_lnkLogout")).click();
				}
			}
			else if(no==2)
			{
				driver.findElement(By.id("btnInsuffSave")).click();
				Thread.sleep(1500);
				driver.switchTo().alert().accept();
				Thread.sleep(1000);
				driver.switchTo().alert().accept();
				Thread.sleep(2500);
				driver.findElement(By.id("ctl00_lnkLogout")).click();
				}
			
		}
		}
			catch(NoSuchElementException e)
			{
				System.out.println(e);
			}
		}
		
	}

}