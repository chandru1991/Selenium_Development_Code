package Project_Flow;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import Files.Propertyfile;
import Files.Screenshots;
import Files.Text_Writer;
import Project_Path.Gmail_path;
import Project_Path.Loginpage_path;

public class Forgot_Password {
	static SoftAssert softAssert;
	public static void forgotFeatures() throws Exception
	{
		WebDriverWait wait=new WebDriverWait(Driver.instance,20);
		try
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Loginpage_path.forgotpassword))).click();
			System.out.println("Forgot Password link visible in Service Provider Login Screen");
			new Text_Writer("Forgot Password link visible in Service Provider Login Screen");
		}
		catch(Exception e)
		{
			new Screenshots("Forgot Password link not visible");
			System.out.println("Forgot Password link not visible"+e.getMessage());
			System.out.println("Forgot Password link not visible in Service Provider Login Screen");
			new Text_Writer("Forgot Password link not visible in Service Provider Login Screen");
		}
	}
	public static void SP_Forgot_Password() throws Exception
	{
		softAssert=new SoftAssert();
		//WebDriverWait wait=new WebDriverWait(Driver.instance,20);
		
		//without username and Password
		forgotFeatures();
		String nouseralert=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
		softAssert.assertEquals(nouseralert.trim(), Propertyfile.alert_emptyclick.trim(), "Click on Forgot Password without username and Password = "+"Fail");
		if(nouseralert.trim().equals(Propertyfile.alert_emptyclick.trim()))
		{
			System.out.println("Click on Forgot Password without username and Password = "+"Pass");
			new Text_Writer("Click on Forgot Password without username and Password = "+"Pass");
		}
		else
		{
			new Screenshots("Click on Forgot Password without username and Password");
			System.out.println("Click on Forgot Password without username and Password = "+"Fail");
			new Text_Writer("Click on Forgot Password without username and Password = "+"Fail");
		}
		Driver.instance.navigate().refresh();
		//with valid username when click on Yes
		System.out.println("---Forgot Password confirmation alert when clicked on 'Yes' with valid username---");
		new Text_Writer("---Forgot Password confirmation alert when clicked on 'Yes' with valid username---");
		Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
		Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(Propertyfile.owusername);
		Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
		forgotFeatures();
		String validuseralert=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
		softAssert.assertEquals(validuseralert.trim(), Propertyfile.alert_validuser.trim(), "Click on Forgot Password with valid username = "+"Fail");
		if(validuseralert.trim().equals(Propertyfile.alert_validuser.trim()))
		{
			System.out.println("Click on Forgot Password with valid username = "+"Pass");
			new Text_Writer("Click on Forgot Password with valid username = "+"Pass");
			//Driver.wait.until(ExpectedConditions.elementToBeClickable(Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_yes_button)))).click();
			Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_yes_button)).click();
			Thread.sleep(1000);
			String yes_button=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
			if(yes_button.trim().contains(Propertyfile.alert_validuser1.trim()))
			{
				System.out.println("Confirmation alert after request for change password = "+"Pass");
				new Text_Writer("Confirmation alert after request for change password = "+"Pass");
			}
			else
			{
				new Screenshots("Confirmation alert after request for change password");
				System.out.println("Confirmation alert after request for change password = "+"Fail");
				new Text_Writer("Confirmation alert after request for change password = "+"Fail");
			}
		}
		else
		{
			new Screenshots("Click on Forgot Password with valid username-yes");
			System.out.println("Click on Forgot Password with valid username-yes = "+"Fail");
			new Text_Writer("Click on Forgot Password with valid username-yes = "+"Fail");
		}
		Driver.instance.navigate().refresh();
		//with valid username when click on No
			System.out.println("---Forgot Password confirmation alert when clicked on 'No' with valid Username---");
			new Text_Writer("---Forgot Password confirmation alert when clicked on 'No' with valid Username---");
				Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
				Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(Propertyfile.owusername);
				Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
				forgotFeatures();
				String validuseralert1=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
				softAssert.assertEquals(validuseralert1.trim(), Propertyfile.alert_validuser.trim(), "Click on Forgot Password with valid username = "+"Fail");
				if(validuseralert1.trim().equals(Propertyfile.alert_validuser.trim()))
				{
					System.out.println("Click on Forgot Password with valid username = "+"Pass");
					new Text_Writer("Click on Forgot Password with valid username = "+"Pass");
					//Driver.wait.until(ExpectedConditions.elementToBeClickable(Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_yes_button)))).click();
					Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_no_button)).click();
				}
				else
				{
					new Screenshots("Click on Forgot Password with valid username-No");
					System.out.println("Click on Forgot Password with valid username-No = "+"Fail");
					new Text_Writer("Click on Forgot Password with valid username-No = "+"Fail");
				}
				Driver.instance.navigate().refresh();
		       //with invalid username when click on Yes
				System.out.println("---Forgot Password confirmation alert when clicked on 'Yes' with invalid username---");
				new Text_Writer("---Forgot Password confirmation alert when clicked on 'Yes' with invalid username---");
				Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
				Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(Propertyfile.owinvalidusername);
				Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
				forgotFeatures();
				String invaliduseralert=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
				softAssert.assertEquals(invaliduseralert.trim(), Propertyfile.alert_validuser.trim(), "Click on Forgot Password with invalid username = "+"Fail");
				if(invaliduseralert.trim().equals(Propertyfile.alert_validuser.trim()))
				{
					System.out.println("Click on Forgot Password with invalid username = "+"Pass");
					new Text_Writer("Click on Forgot Password with invalid username = "+"Pass");
					Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_yes_button)).click();
					Thread.sleep(1000);
					String yes_button1=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
					if(yes_button1.trim().equals(Propertyfile.alert_invaliduser.trim()))
					{
						System.out.println("Validation alert with invalid username = "+"Pass");
						new Text_Writer("Validation alert with invalid username = "+"Pass");
					}
					else
					{
						new Screenshots("Validation alert with invalid username");
						System.out.println("Validation alert with invalid username = "+"Fail");
						new Text_Writer("Validation alert with invalid username = "+"Fail");
					}
				}
				else
				{
					new Screenshots("Click on Forgot Password with invalid username-yes");
					System.out.println("Click on Forgot Password with invalid username-yes = "+"Fail");
					new Text_Writer("Click on Forgot Password with invalid username-yes = "+"Fail");
				}
				Driver.instance.navigate().refresh();
			       //with invalid username when click on No
					System.out.println("---Forgot Password confirmation alert when clicked on 'No' with invalid username---");
					new Text_Writer("---Forgot Password confirmation alert when clicked on 'No' with invalid username---");
					Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
					Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(Propertyfile.owinvalidusername);
					Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
					forgotFeatures();
					String invaliduseralert1=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
					softAssert.assertEquals(invaliduseralert1.trim(), Propertyfile.alert_validuser.trim(), "Click on Forgot Password with invalid username = "+"Fail");
					if(invaliduseralert1.trim().equals(Propertyfile.alert_validuser.trim()))
					{
						System.out.println("Click on Forgot Password with invalid username = "+"Pass");
						new Text_Writer("Click on Forgot Password with invalid username = "+"Pass");
						Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_no_button)).click();
					}
					else
					{
						new Screenshots("Click on Forgot Password with invalid username-No");
						System.out.println("Click on Forgot Password with invalid username-No = "+"Fail");
						new Text_Writer("Click on Forgot Password with invalid username-No = "+"Fail");
					}
					Thread.sleep(2500);	
	}
	public static void Client_Forgot_Password() throws Exception
	{
		softAssert=new SoftAssert();
		//WebDriverWait wait=new WebDriverWait(Driver.instance,20);
		
		//without username and Password
		forgotFeatures();
		String nouseralert=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
		softAssert.assertEquals(nouseralert.trim(), Propertyfile.alert_emptyclick.trim(), "Click on Forgot Password without username and Password = "+"Fail");
		if(nouseralert.trim().equals(Propertyfile.alert_emptyclick.trim()))
		{
			System.out.println("Click on Forgot Password without username and Password = "+"Pass");
			new Text_Writer("Click on Forgot Password without username and Password = "+"Pass");
		}
		else
		{
			new Screenshots("Click on Forgot Password without username and Password");
			System.out.println("Click on Forgot Password without username and Password = "+"Fail");
			new Text_Writer("Click on Forgot Password without username and Password = "+"Fail");
		}
		Driver.instance.navigate().refresh();
		//with valid username when click on Yes
		System.out.println("---Forgot Password confirmation alert when clicked on 'Yes' with valid username---");
		new Text_Writer("---Forgot Password confirmation alert when clicked on 'Yes' with valid username---");
		Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
		Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(Propertyfile.client_owusername);
		Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
		forgotFeatures();
		String validuseralert=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
		softAssert.assertEquals(validuseralert.trim(), Propertyfile.alert_validuser.trim(), "Click on Forgot Password with valid username = "+"Fail");
		if(validuseralert.trim().equals(Propertyfile.alert_validuser.trim()))
		{
			System.out.println("Click on Forgot Password with valid username = "+"Pass");
			new Text_Writer("Click on Forgot Password with valid username = "+"Pass");
			//Driver.wait.until(ExpectedConditions.elementToBeClickable(Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_yes_button)))).click();
			Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_yes_button)).click();
			Thread.sleep(1000);
			String yes_button=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
			if(yes_button.trim().contains(Propertyfile.alert_validuser1.trim()))
			{
				System.out.println("Confirmation alert after request for change password = "+"Pass");
				new Text_Writer("Confirmation alert after request for change password = "+"Pass");
			}
			else
			{
				new Screenshots("Confirmation alert after request for change password");
				System.out.println("Confirmation alert after request for change password = "+"Fail");
				new Text_Writer("Confirmation alert after request for change password = "+"Fail");
			}
		}
		else
		{
			new Screenshots("Click on Forgot Password with valid username-Yes");
			System.out.println("Click on Forgot Password with valid username-Yes = "+"Fail");
			new Text_Writer("Click on Forgot Password with valid username-Yes = "+"Fail");
		}
		Driver.instance.navigate().refresh();
		//with valid username when click on No
			System.out.println("---Forgot Password confirmation alert when clicked on 'No' with valid Username---");
			new Text_Writer("---Forgot Password confirmation alert when clicked on 'No' with valid Username---");
				Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
				Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(Propertyfile.client_owusername);
				Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
				forgotFeatures();
				String validuseralert1=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
				softAssert.assertEquals(validuseralert1.trim(), Propertyfile.alert_validuser.trim(), "Click on Forgot Password with valid username = "+"Fail");
				if(validuseralert1.trim().equals(Propertyfile.alert_validuser.trim()))
				{
					System.out.println("Click on Forgot Password with valid username = "+"Pass");
					new Text_Writer("Click on Forgot Password with valid username = "+"Pass");
					//Driver.wait.until(ExpectedConditions.elementToBeClickable(Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_yes_button)))).click();
					Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_no_button)).click();
				}
				else
				{
					new Screenshots("Click on Forgot Password with valid username-No");
					System.out.println("Click on Forgot Password with valid username-No = "+"Fail");
					new Text_Writer("Click on Forgot Password with valid username-No = "+"Fail");
				}
				Driver.instance.navigate().refresh();
		       //with invalid username when click on Yes
				System.out.println("---Forgot Password confirmation alert when clicked on 'Yes' with invalid username---");
				new Text_Writer("---Forgot Password confirmation alert when clicked on 'Yes' with invalid username---");
				Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
				Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(Propertyfile.owinvalidusername);
				Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
				forgotFeatures();
				String invaliduseralert=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
				softAssert.assertEquals(invaliduseralert.trim(), Propertyfile.alert_validuser.trim(), "Click on Forgot Password with invalid username = "+"Fail");
				if(invaliduseralert.trim().equals(Propertyfile.alert_validuser.trim()))
				{
					System.out.println("Click on Forgot Password with invalid username = "+"Pass");
					new Text_Writer("Click on Forgot Password with invalid username = "+"Pass");
					Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_yes_button)).click();
					Thread.sleep(1000);
					String yes_button1=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
					if(yes_button1.trim().equals(Propertyfile.alert_invaliduser.trim()))
					{
						System.out.println("Validation alert with invalid username = "+"Pass");
						new Text_Writer("Validation alert with invalid username = "+"Pass");
					}
					else
					{
						new Screenshots("Validation alert with invalid username");
						System.out.println("Validation alert with invalid username = "+"Fail");
						new Text_Writer("Validation alert with invalid username = "+"Fail");
					}
				}
				else
				{
					new Screenshots("Click on Forgot Password with invalid username-Yes");
					System.out.println("Click on Forgot Password with invalid username-Yes = "+"Fail");
					new Text_Writer("Click on Forgot Password with invalid username-Yes = "+"Fail");
				}
				Driver.instance.navigate().refresh();
			       //with invalid username when click on No
					System.out.println("---Forgot Password confirmation alert when clicked on 'No' with invalid username---");
					new Text_Writer("---Forgot Password confirmation alert when clicked on 'No' with invalid username---");
					Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
					Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(Propertyfile.owinvalidusername);
					Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
					forgotFeatures();
					String invaliduseralert1=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
					softAssert.assertEquals(invaliduseralert1.trim(), Propertyfile.alert_validuser.trim(), "Click on Forgot Password with invalid username = "+"Fail");
					if(invaliduseralert1.trim().equals(Propertyfile.alert_validuser.trim()))
					{
						System.out.println("Click on Forgot Password with invalid username = "+"Pass");
						new Text_Writer("Click on Forgot Password with invalid username = "+"Pass");
						Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_no_button)).click();
					}
					else
					{
						new Screenshots("Click on Forgot Password with invalid username-No");
						System.out.println("Click on Forgot Password with invalid username-No = "+"Fail");
						new Text_Writer("Click on Forgot Password with invalid username-No = "+"Fail");
					}
					Thread.sleep(2500);	
	}
	public static void Candidate_Forgot_Password() throws Exception
	{
		softAssert=new SoftAssert();
		//WebDriverWait wait=new WebDriverWait(Driver.instance,20);
		
		//without username and Password
		forgotFeatures();
		String nouseralert=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
		softAssert.assertEquals(nouseralert.trim(), Propertyfile.alert_emptyclick.trim(), "Click on Forgot Password without username and Password = "+"Fail");
		if(nouseralert.trim().equals(Propertyfile.alert_emptyclick.trim()))
		{
			System.out.println("Click on Forgot Password without username and Password = "+"Pass");
			new Text_Writer("Click on Forgot Password without username and Password = "+"Pass");
		}
		else
		{
			new Screenshots("Click on Forgot Password without username and Password");
			System.out.println("Click on Forgot Password without username and Password = "+"Fail");
			new Text_Writer("Click on Forgot Password without username and Password = "+"Fail");
		}
		Driver.instance.navigate().refresh();
		//with valid username when click on Yes
		System.out.println("	---Forgot Password confirmation alert when clicked on 'Yes' with valid username---");
		new Text_Writer("	---Forgot Password confirmation alert when clicked on 'Yes' with valid username---");
		Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
		Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(Propertyfile.candidate_owusername);
		Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
		forgotFeatures();
		String validuseralert=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
		softAssert.assertEquals(validuseralert.trim(), Propertyfile.alert_validuser.trim(), "Click on Forgot Password with valid username = "+"Fail");
		if(validuseralert.trim().equals(Propertyfile.alert_validuser.trim()))
		{
			System.out.println("Click on Forgot Password with valid username = "+"Pass");
			new Text_Writer("Click on Forgot Password with valid username = "+"Pass");
			//Driver.wait.until(ExpectedConditions.elementToBeClickable(Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_yes_button)))).click();
			Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_yes_button)).click();
			Thread.sleep(1000);
			String yes_button=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
			if(yes_button.trim().contains(Propertyfile.alert_validuser1.trim()))
			{
				System.out.println("Confirmation alert after request for change password = "+"Pass");
				new Text_Writer("Confirmation alert after request for change password = "+"Pass");
			}
			else
			{
				new Screenshots("Confirmation alert after request for change password");
				System.out.println("Confirmation alert after request for change password = "+"Fail");
				new Text_Writer("Confirmation alert after request for change password = "+"Fail");
			}
		}
		else
		{
			new Screenshots("Click on Forgot Password with valid username-Yes");
			System.out.println("Click on Forgot Password with valid username-Yes = "+"Fail");
			new Text_Writer("Click on Forgot Password with valid username-Yes = "+"Fail");
		}
		Driver.instance.navigate().refresh();
		//with valid username when click on No
			System.out.println("---Forgot Password confirmation alert when clicked on 'No' with valid Username---");
			new Text_Writer("---Forgot Password confirmation alert when clicked on 'No' with valid Username---");
				Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
				Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(Propertyfile.candidate_owusername);
				Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
				forgotFeatures();
				String validuseralert1=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
				softAssert.assertEquals(validuseralert1.trim(), Propertyfile.alert_validuser.trim(), "Click on Forgot Password with valid username = "+"Fail");
				if(validuseralert1.trim().equals(Propertyfile.alert_validuser.trim()))
				{
					System.out.println("Click on Forgot Password with valid username = "+"Pass");
					new Text_Writer("Click on Forgot Password with valid username = "+"Pass");
					//Driver.wait.until(ExpectedConditions.elementToBeClickable(Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_yes_button)))).click();
					Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_no_button)).click();
				}
				else
				{
					new Screenshots("Click on Forgot Password with valid username-No");
					System.out.println("Click on Forgot Password with valid username-No = "+"Fail");
					new Text_Writer("Click on Forgot Password with valid username-No = "+"Fail");
				}
				Driver.instance.navigate().refresh();
		       //with invalid username when click on Yes
				System.out.println("---Forgot Password confirmation alert when clicked on 'Yes' with invalid username---");
				new Text_Writer("---Forgot Password confirmation alert when clicked on 'Yes' with invalid username---");
				Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
				Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(Propertyfile.owinvalidusername);
				Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
				forgotFeatures();
				String invaliduseralert=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
				softAssert.assertEquals(invaliduseralert.trim(), Propertyfile.alert_validuser.trim(), "Click on Forgot Password with invalid username = "+"Fail");
				if(invaliduseralert.trim().equals(Propertyfile.alert_validuser.trim()))
				{
					System.out.println("Click on Forgot Password with invalid username = "+"Pass");
					new Text_Writer("Click on Forgot Password with invalid username = "+"Pass");
					Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_yes_button)).click();
					Thread.sleep(1000);
					String yes_button1=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
					if(yes_button1.trim().equals(Propertyfile.alert_invaliduser.trim()))
					{
						System.out.println("Validation alert with invalid username = "+"Pass");
						new Text_Writer("Validation alert with invalid username = "+"Pass");
					}
					else
					{
						new Screenshots("Validation alert with invalid username");
						System.out.println("Validation alert with invalid username = "+"Fail");
						new Text_Writer("Validation alert with invalid username = "+"Fail");
					}
				}
				else
				{
					new Screenshots("Click on Forgot Password with invalid username-Yes");
					System.out.println("Click on Forgot Password with invalid username-Yes = "+"Fail");
					new Text_Writer("Click on Forgot Password with invalid username-Yes = "+"Fail");
				}
				Driver.instance.navigate().refresh();
			       //with invalid username when click on No
					System.out.println("---Forgot Password confirmation alert when clicked on 'No' with invalid username---");
					new Text_Writer("---Forgot Password confirmation alert when clicked on 'No' with invalid username---");
					Driver.instance.findElement(By.xpath(Loginpage_path.username)).clear();
					Driver.instance.findElement(By.xpath(Loginpage_path.username)).sendKeys(Propertyfile.owinvalidusername);
					Driver.instance.findElement(By.xpath(Loginpage_path.password)).clear();
					forgotFeatures();
					String invaliduseralert1=Driver.instance.findElement(By.xpath(Loginpage_path.alertusernotavailable)).getText();
					softAssert.assertEquals(invaliduseralert1.trim(), Propertyfile.alert_validuser.trim(), "Click on Forgot Password with invalid username = "+"Fail");
					if(invaliduseralert1.trim().equals(Propertyfile.alert_validuser.trim()))
					{
						System.out.println("Click on Forgot Password with invalid username = "+"Pass");
						new Text_Writer("Click on Forgot Password with invalid username = "+"Pass");
						Driver.instance.findElement(By.xpath(Loginpage_path.forgotpassword_no_button)).click();
					}
					else
					{
						System.out.println("Click on Forgot Password with invalid username = "+"Fail");
						new Text_Writer("Click on Forgot Password with invalid username = "+"Fail");
					}
					Thread.sleep(2500);	
	}

}
