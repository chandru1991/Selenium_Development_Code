package check360_flow;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class CIF 
{
  @Test
  public void f() throws InterruptedException
  {
	 // System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe");
	 // ChromeOptions options = new ChromeOptions();
	  //options.addArguments("--start-maximized");
	 // WebDriver driver = new ChromeDriver( options );
	  WebDriver driver=new FirefoxDriver();
	  driver.manage().window().maximize();
	  String baseUrl = "http://192.168.2.17:86";
	  driver.get(baseUrl);
	  int wait;
	  String ParentWin=null;
	  String CanName;
	  
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
	  Thread.sleep(7500);
	  driver.findElement(By.id("btnActions")).click();
	  Thread.sleep(1000);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Arrow")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[contains(text(), 'View CIF as document')]")).click();
	  Thread.sleep(3000);
	  String RNO=JOptionPane.showInputDialog("Enter the Reference No:");
	  
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_TxtCaseRefNo")).clear();
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_TxtCaseRefNo")).sendKeys(RNO);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_BttnSearch")).click();
	  Thread.sleep(5000);
	  wait=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdNewCaseReportList_ctl00_ctl04_btnViewdoc']")).size();
	 // wait = driver.findElements(By.id("ctl00_ContentPlaceHolder1_grdNewCaseReportList_ctl00_ctl04_btnViewdoc_input")).size();
	  if(wait!=0)
	  {
	 // CanName=driver.findElement(By.xpath("/html/body/form/div[5]/div[3]/div[3]/div/div[3]/div/div[2]/table/tbody/tr/td[8]")).getText();
	  ParentWin=driver.getWindowHandle();// current window
	  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdNewCaseReportList_ctl00_ctl04_btnViewdoc']")).click();
	 // driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdNewCaseReportList_ctl00_ctl04_btnViewdoc_input")).click();
	  Thread.sleep(3000);
	 for (String winHandle : driver.getWindowHandles()) //new window
	  {
	  driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
	  //String newname=driver.getWindowHandle();
	  }
	  Thread.sleep(3000);
	  JavascriptExecutor jse=(JavascriptExecutor)driver;
      jse.executeScript("scroll(0, 250);");
	  String First = driver.findElement(By.xpath("//div[@id='pageContainer1']/div[2]/div[5]")).getText();
	  String Last = driver.findElement(By.xpath("//div[@id='pageContainer1']/div[2]/div[7]")).getText();
	  String Name = First +" "+Last;
	  System.out.println(""+Name);
	  CanName=JOptionPane.showInputDialog("Enter Candidate First Name to Verify");
	  if(CanName.equals(First))
	  {
	  System.out.println("CIF Document is available for given Reference No");
	  System.out.println("CIF Document is Correct");  }
	  else
	  {
	  System.out.println("CIF may be Incorrect . Since Candidate name are not matched with CIF");
	  }
	  }
	  else
	  {
	  System.out.println("No CIF Document is available for given Reference No");
	  }
	  driver.close();// close New Window
	  driver.switchTo().window(ParentWin);// switch back to current window
	  driver.close();// close current window
	  
  }
}
