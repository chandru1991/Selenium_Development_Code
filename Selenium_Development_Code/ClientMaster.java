package Project_Flow;

import org.openqa.selenium.By;
import org.testng.asserts.SoftAssert;

import Project_Path.ClientMaster_path;
import Project_Path.SP_Homepage_path;

public class ClientMaster {
	public ClientMaster()
	{
		SoftAssert softassert=new SoftAssert();
		String clientn=Driver.instance.findElement(By.xpath(SP_Homepage_path.client)).getText();
		softassert.assertEquals("Client", clientn);
		String clientmn=Driver.instance.findElement(By.xpath(SP_Homepage_path.client)).getText();
		softassert.assertEquals("Clients", clientmn);
		Driver.instance.findElement(By.xpath(SP_Homepage_path.client)).click();
		Driver.instance.findElement(By.xpath(ClientMaster_path.clientmaster)).click();
		softassert.assertAll();
	}

}
