package check360_flow;

import java.io.BufferedWriter;
import java.util.UUID;

import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Candidate_workflow 
{
  static WebDriver driver;
   static String Rno;
   
   @Before
	public String no() throws Exception
	{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
		String strDate = sdf.format(cal.getTime());
		return strDate;
	}
   
  @Test
  public static void main(String ar[]) throws Exception 
  {
	  driver= new FirefoxDriver();
	  driver.manage().window().maximize();
	 String baseUrl = "http://192.168.2.17:96/project1234";
	// String baseUrl = "http://192.168.2.17:96/dpc";
	//String baseUrl = "http://paws2.com/democlient12";
	  driver.get(baseUrl);
	  WebElement w1;
	  WebElement w2;
	  WebDriverWait wait1=new WebDriverWait(driver,20);
	  Candidate_workflow  m=new Candidate_workflow();
	  String mail=m.no();
	  WebElement sd;		
	  int temp=0;
	  String uuid = UUID.randomUUID().toString();
	  int uuid_len=uuid.replaceAll("[0-9]","").replaceAll("[-]","").length();	  
	  String firstname=uuid.replaceAll("[0-9]","").replaceAll("[-]","").substring(0,5);
	  String lastname=uuid.replaceAll("[0-9]","").replaceAll("[-]","").substring(uuid_len-4,uuid_len);
	  int TabCheck=0;
		int dcc =0;
		String Checkname= null;
	  int wait=0;
		   
	////////////// Candidate SignUP ///////////////////
		  		   	
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_lnkSingUP")).click();
	  driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtFirstName")).clear();
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtFirstName")).sendKeys(firstname);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtLastName")).sendKeys(lastname);
			
	  sd= driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlDate_Input"));
	  sd.sendKeys("20");
	  sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlMonth_Input"));
	  sd.sendKeys("May");
				
	  sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlYear_Input"));
	  sd.sendKeys("1991");
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmailID")).sendKeys(firstname+"@testmail.com");
	  
      driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPass")).sendKeys("Checks@123");
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPass2")).sendKeys("Checks@123");
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRegister")).click();
	  Thread.sleep(2500); 
	  Alert alert = driver.switchTo().alert();
	  alert.accept();
	  Thread.sleep(2000); 
	  
//////////////Candidate SignIn ////////////////////
		

	 // driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("Candidate1104161517@testmail.com");
  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys(firstname+"@testmail.com");
  Thread.sleep(1000);
  String username=driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).getText();
	   System.out.println(firstname+"@testmail.com"+"\n"+"Password= Checks@123");	 
	   FileWriter fileWriter = new FileWriter("Credentials/C"+firstname+".txt");
       BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
       bufferedWriter.write("Username= "+firstname+"@testmail.com"+"\n"+"Password= Checks@123");
       bufferedWriter.close();
   driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
	driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	Thread.sleep(5000);
	
	
//////////////// Component Selection ///////////////////
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl14_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl16_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl12_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl23_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl25_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl32_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl34_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl36_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl47_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl49_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl43_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl56_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl58_chkSelectComponent")).click();
//	driver.findElement(By.id("_rfdSkinnedgvComponentList_ctl00_ctl60_chkSelectComponent")).click();
	
	
	driver.findElement(By.id("btnApplyChanges")).click();  
	Thread.sleep(9000); 
	//for(int i=0;i<=27;i++)
	//{
	//	try
		//{
	//String checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
	//System.out.println("Check="+Checkname);	
	
	for(int i=0;i<=350;i++)
	{
		try
		{
	String Checkname1=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
	System.out.println("Check="+Checkname1);	
/////////////// Basic Details /////////////////////////
	if(Checkname1.equals("Basic Details"))
	{
	driver.switchTo().frame(0);
	driver.findElement(By.name("ctl00$ContentPlaceHolder1$txtFatherFirstName")).sendKeys(firstname);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtFatherLastName")).sendKeys("Father");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtMobileNumber1")).sendKeys("9513578526");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtLandLine")).sendKeys("04526547895");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmergencyContact")).sendKeys("9874563322");

	
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlGender_Input"));
	sd.sendKeys("Male");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlMaritalStatus_Input"));
	sd.sendKeys("Single");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtNationality")).sendKeys("Indian");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIDType_Input"));
	sd.sendKeys("Candidate ID");
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtClientCandidateID")).sendKeys("CID"+mail.substring(6));
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSaveNext")).click();
	driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);  
	Thread.sleep(5000);
	}
///////////////////// Address ////////////////////////
	
	if(Checkname1.equals("Address"))
	{
		driver.switchTo().frame(0);
		String comname=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblComponentName")).getText();
		System.out.println("comname"+comname);
		if(comname.equals("Permanent"))
		{
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressAddress")).sendKeys("No-81, EVP puram , Thathaneri post ");
	
// Country-State-City Drop Down
	
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCountry_Input")).click();
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCountry_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCountry_Input")).sendKeys("India");
	Thread.sleep(1500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCountry_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressState_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressState_Input")).sendKeys("Tamil Nadu");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressState_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(2500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCity_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCity_Input")).sendKeys("Chennai");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCity_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(2000);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressPincode")).sendKeys("625018");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressLandMark")).sendKeys("ESI Hospital Back Side");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressFromDate_dateInput")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressFromDate_dateInput")).sendKeys("Nov 1990");
	Thread.sleep(1500);
	driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkTillDate")).click();
	Thread.sleep(2000);
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressDocument")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmAddressDocument_C_grdDocumentList_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/123.pdf");
	Thread.sleep(2000);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmAddressDocument_C_btnAddDocument")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath("/html/body/form/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit")).click();
	Thread.sleep(5000);
	}
//////////////////////////Same As Address  /////////////////////
	else if(comname.equals("Current Address"))
	{
driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlSameAs_Input")).click();
Thread.sleep(500);
driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlSameAs_Input")).sendKeys(Keys.DOWN);
driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlSameAs_Input")).sendKeys(Keys.DOWN);
Thread.sleep(500);
//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlSameAs_Input")).sendKeys(Keys.DOWN);
//Thread.sleep(500);
//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlSameAs_Input")).sendKeys(Keys.DOWN);
//Thread.sleep(500);
driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlSameAs_Input")).sendKeys(Keys.ENTER);
Thread.sleep(2000);
driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit_input")).click();
Thread.sleep(7000);

	}

	
//// Address With Landlord with New Country/State/City
	else
	{
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressAddress")).sendKeys("No:24, Dharmapuram, Thathaneri post ");
	
// Country-State-City Drop Down
	
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCountry_Input")).click();
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCountry_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCountry_Input")).sendKeys("India");
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCountry_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressState_Input")).click();
	 driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCountry_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressState_Input")).sendKeys("Tamil Nadu");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressState_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCity_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCity_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCity_Input")).sendKeys("Chennai");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressCity_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressPincode")).sendKeys("625018");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressLandMark")).sendKeys("ESI Side");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressFromDate_dateInput")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressFromDate_dateInput")).sendKeys("Nov 1991");
	Thread.sleep(500);
	driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkTillDate")).click();
	Thread.sleep(2000);
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressDocument")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmAddressDocument_C_grdDocumentList_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/8745.pdf");
	Thread.sleep(2000);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmAddressDocument_C_btnAddDocument")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath("/html/body/form/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit")).click();
	Thread.sleep(7000);
	}
	}
/////////////////////////   Address Submit Later //////////////////
//	
//	driver.switchTo().frame(0);	
//	driver.findElement(By.id("ctl00_ContentPlaceHolder1_chkCheckLater")).click();
//	Thread.sleep(2500);
//	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit")).click();
//	Thread.sleep(3000);

//////////////////////   10th  ///////////////////////////////////
	if(Checkname1.equals("Education"))
	{
		driver.switchTo().frame(0);
		String comname=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblComponent")).getText();
		if(comname.equals("10th")||comname.equals("Highest1"))
		{
	Thread.sleep(2000);
	if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).isEnabled())
	{
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).sendKeys(Keys.DOWN);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1500);
	}
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys("kar");
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys(Keys.DOWN);
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(2000);
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationNameOfCourse")).sendKeys("10th");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationTypeOfMajor")).sendKeys("Science");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationTypeOfProgramName_Input"));
	sd.sendKeys("Full Time ");

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCandidateNameInCertificate")).sendKeys("Shalini");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationEnrollmentRegisterNo")).sendKeys("9653258");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCGPA")).sendKeys("81.07%");
	Thread.sleep(750);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCourseCommenceDate_dateInput")).sendKeys("04/2002");
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCourseCompletionDate_dateInput")).sendKeys("04/2006");
	
		
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnUploadDocument")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_gviewEducationDocument_ctl00_ctl04_rauEducationComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/1.pdf");
	Thread.sleep(3500);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_btnEducationAddDocument_input")).click();
	Thread.sleep(3500);
	driver.findElement(By.xpath("/html/body/form/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveContinue_input")).click();
	Thread.sleep(7000); 
	}
	
////////////////////// 12th   ///////////////////////////////////
		else if(comname.equals("12th")||comname.equals("Highest2"))
		{
	Thread.sleep(2000);
	if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).isEnabled())
	{
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).click();
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).sendKeys(Keys.DOWN);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(5000);
	}
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys("kar");
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys(Keys.DOWN);
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(2000);
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationNameOfCourse")).sendKeys("12th");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationTypeOfMajor")).sendKeys("CSE");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationTypeOfProgramName_Input"));
	sd.sendKeys("Full Time ");


	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationTypeOfMajor")).sendKeys("Computer");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCandidateNameInCertificate")).sendKeys("Gayathri");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationEnrollmentRegisterNo")).sendKeys("655666");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCGPA")).sendKeys("78.81%");
	Thread.sleep(750);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCourseCommenceDate_dateInput")).sendKeys("04/2004");
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCourseCompletionDate_dateInput")).sendKeys("04/2008");
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveContinue_input")).click();
	Thread.sleep(7000); 
	}
	///////////////////////////////////UG3/////////////////////////////////////
	else
	{
	if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).isEnabled())
	{
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).sendKeys(Keys.DOWN);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1500);
	}
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys("del");
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys(Keys.DOWN);
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationInstitute_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(2000);
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationNameOfCourse")).sendKeys("BE");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationTypeOfMajor")).sendKeys("CSE");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationTypeOfProgramName_Input"));
	sd.sendKeys("Full Time ");

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCandidateNameInCertificate")).sendKeys("Shalini");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationEnrollmentRegisterNo")).sendKeys("9653258");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCGPA")).sendKeys("81.07%");
	Thread.sleep(750);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCourseCommenceDate_dateInput")).sendKeys("04/2002");
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationCourseCompletionDate_dateInput")).sendKeys("04/2006");
	
		
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnUploadDocument")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_gviewEducationDocument_ctl00_ctl04_rauEducationComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/1.pdf");
	Thread.sleep(3500);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_btnEducationAddDocument_input")).click();
	Thread.sleep(3500);
	driver.findElement(By.xpath("/html/body/form/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveContinue_input")).click();
	Thread.sleep(7000); 

	}
	}
//////////////////////// Submit Later   ///////////////////////////////////
//	
//	driver.switchTo().frame(0);
//	driver.findElement(By.id("ctl00_ContentPlaceHolder1_chkComponentDetailLater")).click();
//	Thread.sleep(2500);
//	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveContinue")).click();
//	Thread.sleep(3000); 

////////////////////// Employment - Current   ///////////////////////////////////
	else if(Checkname1.equals("Employment"))
	{
		driver.switchTo().frame(0);
		String comname=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblEmploymentComponent")).getText();
		if(comname.equals("Current Employment"))
		{
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).clear();
	Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys("TCS");
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys(Keys.DOWN);
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeID")).sendKeys("554545");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeDesignation")).sendKeys("Tester");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeDepartment")).sendKeys("Testing");
		
//From - Till date
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtFromDate_dateInput")).sendKeys("12/05/2013");
	driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkTillDate")).click();
	Thread.sleep(2500);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtLastSalaryDrawn")).sendKeys("4 Lakhs");
	
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCurrencyType_Input"));
	sd.sendKeys("USD");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentType_Input"));
	sd.sendKeys("Permanent");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmployeeSalaryType_Input"));
	sd.sendKeys("Per Month");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRelationship1_Input"));
	sd.sendKeys("HR");
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1Name")).sendKeys("Ravi");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1Designation")).sendKeys("HR");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1ContactNo1")).sendKeys("9632587401");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1EmailID1")).sendKeys("ravi@gmail.com");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtReasonOfLeaving")).sendKeys("Salary");
		
// CEP //
	
	/*driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rbtnLstEmploymentContactNow_1")).click();
	Thread.sleep(2500);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentContactDate_popupButton")).click();
	//Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentContactDate_dateInput")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentContactDate_dateInput")).sendKeys("20/11/2017");
	//driver.findElement(By.linkText("Feb")).click();
	//driver.findElement(By.linkText("2015")).click();
	//driver.findElement(By.id("rcMView_OK")).click();*/

	//driver.findElement(By.linkText("2")).click();

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnUploadDocument_input")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_gviewEmploymentDocument_ctl00_ctl04_rauEmploymentComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/5.pdf");
	Thread.sleep(2000);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_btnEmploymentAddDocument")).click();
	Thread.sleep(3500);
	driver.findElement(By.xpath("/html/body/form/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSaveContinue_input")).click();
	Thread.sleep(7000);
	}
/////////////////////  Employment 2 ///////////////////
		else if(comname.equals("Previous Employment"))
		{
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys("infosys");
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys(Keys.DOWN);
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeID")).sendKeys("554545");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeDesignation")).sendKeys("developer");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeDepartment")).sendKeys("developement");
		
//From - Till date
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtFromDate_dateInput")).sendKeys("12/05/2013");
	driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkTillDate")).click();
	Thread.sleep(2500);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtLastSalaryDrawn")).sendKeys("4 Lakhs");
	
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCurrencyType_Input"));
	sd.sendKeys("USD");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentType_Input"));
	sd.sendKeys("Permanent");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmployeeSalaryType_Input"));
	sd.sendKeys("Per Month");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRelationship1_Input"));
	sd.sendKeys("HR");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1Name")).sendKeys("Ravi");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1Designation")).sendKeys("HR");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1ContactNo1")).sendKeys("9632587401");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1EmailID1")).sendKeys("raji@gmail.com");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtReasonOfLeaving")).sendKeys("Salary");
		
// CEP //
	
	/*driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rbtnLstEmploymentContactNow_1")).click();
	Thread.sleep(2500);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentContactDate_popupButton")).click();
	//Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentContactDate_dateInput")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentContactDate_dateInput")).sendKeys("20/11/2017");
	//driver.findElement(By.linkText("Feb")).click();
	//driver.findElement(By.linkText("2015")).click();
	//driver.findElement(By.id("rcMView_OK")).click();*/

	//driver.findElement(By.linkText("2")).click();

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnUploadDocument_input")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_gviewEmploymentDocument_ctl00_ctl04_rauEmploymentComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/5.pdf");
	Thread.sleep(2000);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_btnEmploymentAddDocument")).click();
	Thread.sleep(3500);
	driver.findElement(By.xpath("/html/body/form/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSaveContinue_input")).click();
	Thread.sleep(7000);
	}
////////////////////// Employment 3 without CEP //////////////////////////
	else
	{
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys("dell");
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys(Keys.DOWN);
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentCompany_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(2000);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeID")).sendKeys("554545");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeDesignation")).sendKeys("Bussiness Analyst");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmployeeDepartment")).sendKeys("BA");
		
//From - Till date
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtFromDate_dateInput")).sendKeys("12/05/2013");
	driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_chkTillDate")).click();
	Thread.sleep(2500);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtLastSalaryDrawn")).sendKeys("4 Lakhs");
	
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCurrencyType_Input"));
	sd.sendKeys("USD");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentType_Input"));
	sd.sendKeys("Permanent");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmployeeSalaryType_Input"));
	sd.sendKeys("Per Month");
	sd = driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRelationship1_Input"));
	sd.sendKeys("HR");
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1Name")).sendKeys("Raji");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1Designation")).sendKeys("HR");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1ContactNo1")).sendKeys("9632587401");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactPerson1EmailID1")).sendKeys("teja@gmail.com");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtReasonOfLeaving")).sendKeys("Salary");
		
// CEP //
	
	/*driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rbtnLstEmploymentContactNow_1")).click();
	Thread.sleep(2500);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentContactDate_popupButton")).click();
	//Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentContactDate_dateInput")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentContactDate_dateInput")).sendKeys("20/11/2017");
	//driver.findElement(By.linkText("Feb")).click();
	//driver.findElement(By.linkText("2015")).click();
	//driver.findElement(By.id("rcMView_OK")).click();*/

	//driver.findElement(By.linkText("2")).click();

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnUploadDocument_input")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_gviewEmploymentDocument_ctl00_ctl04_rauEmploymentComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/5.pdf");
	Thread.sleep(2000);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_btnEmploymentAddDocument")).click();
	Thread.sleep(3500);
	driver.findElement(By.xpath("/html/body/form/div/table/tbody/tr/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSaveContinue_input")).click();
	Thread.sleep(7000);
	}	
	}
////// Emp - Submit Later
//	
//	driver.switchTo().frame(0);
//	driver.findElement(By.id("ctl00_ContentPlaceHolder1_chkEmploymentDetailLater")).click();
//	Thread.sleep(3000); 
//	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSaveContinue")).click();
//	Thread.sleep(3000); */
		
///////////////////////// ID //////////////////////////////////
	
//////////////////////// Voter ID ////////////////////////////
	else if(Checkname1.equals("Identity"))
	{
		driver.switchTo().frame(0);
		String comname=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblIdentityComponentName")).getText();
		if(comname.equals("Voter ID")||comname.equals("ID Check 1"))
		{
	  	Thread.sleep(2000);
	  	String id=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblIdentityComponentName")).getText();
	  	if(id.equals("ID Check 1")||id.equals("ID Check 2")||id.equals("ID Check 3")||id.equals("ID Check 4")||id.equals("ID Check 5")||id.equals("ID Check 6"))
	  	{
	  		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdCheckComponent_Input")).click();
	  		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdCheckComponent_Input")).sendKeys(Keys.DOWN);
	  		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdCheckComponent_Input")).sendKeys(Keys.ENTER);
	  		Thread.sleep(1000);
	  	}
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdName")).sendKeys("Jack");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdNumber")).sendKeys("VID9569793");
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdIssueDate_dateInput")).sendKeys("11/11/2015");
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdExpireDate_dateInput")).sendKeys("11/11/2017");
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCountry_Input")).click();
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCountry_Input")).sendKeys("India");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).sendKeys("Tamil Nadu");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).sendKeys("Chennai");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdAddDocuments_input")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseIDDocuments_C_grdviewIdDocument_ctl00_ctl04_rauIdComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/4.pdf");
	Thread.sleep(2000);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseIDDocuments_C_btnIDAddDocument_input")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseIDDocuments_C_btnIdDocumentCancel")).click();
	Thread.sleep(2000);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveAndNext_input")).click();
	Thread.sleep(3000);
	}
//////////////////////// Driving License ////////////////////////////
	else if(comname.equals("Driving License")||comname.equals("ID Check 2"))
	{
	String id=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblIdentityComponentName")).getText();
	if(id.equals("ID Check 1")||id.equals("ID Check 2")||id.equals("ID Check 3")||id.equals("ID Check 4")||id.equals("ID Check 5")||id.equals("ID Check 6"))
	{
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdCheckComponent_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdCheckComponent_Input")).sendKeys(Keys.DOWN);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdCheckComponent_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	}
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdName")).sendKeys("Jackl");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdNumber")).sendKeys("TN59 L848586669");
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdIssueDate_dateInput")).sendKeys("11/11/2015");
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdExpireDate_dateInput")).sendKeys("11/11/2017");
		

	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCountry_Input")).click();
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).clear();
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCountry_Input")).sendKeys("India");
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCountry_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).sendKeys("Tamil Nadu");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).sendKeys("Chennai");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveAndNext_input")).click();
	Thread.sleep(7000);
	}
//////////////////////// PAN Card ////////////////////////////
	else
	{
	String id=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblIdentityComponentName")).getText();
	if(id.equals("ID Check 1")||id.equals("ID Check 2")||id.equals("ID Check 3")||id.equals("ID Check 4")||id.equals("ID Check 5")||id.equals("ID Check 6"))
	{
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdCheckComponent_Input")).click();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdCheckComponent_Input")).sendKeys(Keys.DOWN);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdCheckComponent_Input")).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
	}
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdName")).sendKeys("Jack");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdNumber")).sendKeys("PDJ7585589K");
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdIssueDate_dateInput")).sendKeys("11/11/2015");
	Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdExpireDate_dateInput")).sendKeys("11/11/2017");
	Thread.sleep(1000);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCountry_Input")).click();
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).clear();
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCountry_Input")).sendKeys("India");
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCountry_Input")).sendKeys(Keys.ENTER);
	//Thread.sleep(1000);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).sendKeys("Tamil Nadu");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).sendKeys("Chennai");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveAndNext_input")).click();
	
	Thread.sleep(7000);
	}
	}
/*//////////////////////////  PassPort   ////////////////////////////

	driver.switchTo().frame(0);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdName")).sendKeys("Jack");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdNumber")).sendKeys("PDJ7585589K");
				
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdExpireDate_dateInput")).sendKeys("2025-12-01");
	

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCountry_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCountry_Input")).sendKeys("India");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCountry_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).sendKeys("Tamil Nadu");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueState_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).sendKeys("Chennai");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdIssueCity_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveAndNext")).click();
	Thread.sleep(3000);*/
	////////////////////////// Reference Academic //////////////////////////
	else if(Checkname1.equals("Reference"))
	{
		driver.switchTo().frame(0);
		String comname=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblRefComponentName")).getText();
		if(comname.equals("Reference 2")||comname.equals("Reference 4"))
		{
	driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rbtReferenceType_0")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtReferenceName")).sendKeys("Arun");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefDes")).sendKeys("Friend");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactNo")).sendKeys("9632587401");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefEmailId1")).sendKeys("Arun@gmail.com");
				
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).sendKeys("India");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).sendKeys("Tamil Nadu");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).sendKeys("Chennai");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).sendKeys("cbse");
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).sendKeys(Keys.DOWN);
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
		
	//driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rbtCEP_0")).click();
	//Thread.sleep(2500);
	
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtExpectedRelievingDate_dateInput")).sendKeys("18/12/2016");
	Thread.sleep(1000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefDocument_input")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwRefAddDocument_C_grdDocumentList_Ref_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/896.pdf");
	Thread.sleep(2000);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwRefAddDocument_C_btnAddDocument_Ref_input")).click();
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwRefAddDocument_C_btnDocumentClose_Ref")).click();
	Thread.sleep(4000);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
	Thread.sleep(3000);   
	}
	////////////////////////// Reference Employment //////////////////////////
	if(comname.equals("Reference 1")||Checkname1.equals("Reference 3"))
	{
			driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rbtReferenceType_1")).click();
			Thread.sleep(4500);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtReferenceName")).sendKeys("Kishore");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefDes")).sendKeys("Collegue");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactNo")).sendKeys("9632587401");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefEmailId1")).sendKeys("kishore@gmail.com");
						
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).click();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).clear();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).sendKeys("India");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCountry_Input")).sendKeys(Keys.ENTER);
			Thread.sleep(1000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).click();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).clear();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).sendKeys("Tamil Nadu");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererState_Input")).sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).click();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).clear();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).sendKeys("Chennai");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefererCity_Input")).sendKeys(Keys.ENTER);
			Thread.sleep(1000);
			
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).click();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).sendKeys("Glade Technologies");
			Thread.sleep(2000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).sendKeys(Keys.DOWN);
			Thread.sleep(1000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlOrgName_Input")).sendKeys(Keys.ENTER);
			Thread.sleep(1000);
				
			//driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rbtCEP_0")).click();
			//Thread.sleep(2500);
			
			//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtExpectedRelievingDate_dateInput")).sendKeys("18/12/2015");
			Thread.sleep(1000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefDocument_input")).click();
			Thread.sleep(3000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwRefAddDocument_C_grdDocumentList_Ref_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/2.pdf");
			Thread.sleep(2000);
			//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwRefAddDocument_C_btnAddDocument_Ref_input")).click();
			Thread.sleep(3000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwRefAddDocument_C_btnDocumentClose_Ref")).click();
			Thread.sleep(2000);

			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
			Thread.sleep(7000);   
	}
			
	////////////////////////// Reference Personal ////////////////////////// 
	else
	{
			driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_rbtReferenceType_2")).click();
			Thread.sleep(3000);
			
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtReferenceName")).sendKeys("Siva");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefDes")).sendKeys("Cousin");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtContactNo")).sendKeys("8965320147");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
			Thread.sleep(3000); 
	}
	}
		
//	////////////////////////// Reference Submit later //////////////////////////
//
//			driver.switchTo().frame(0);
//			driver.findElement(By.id("ctl00_ContentPlaceHolder1_chkCheckLater")).click();
//			Thread.sleep(2500);
//			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit")).click();
//			Thread.sleep(3000); */
			
	//////////////////////////  GAP Check  //////////////////////////
			
	////////////// Between Education and Employment  //////////////////
	else if(Checkname1.equals("Gap Details"))
	{
			driver.switchTo().frame(0);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlGapType_Input")).click();
			Thread.sleep(1500);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlGapType_Input")).sendKeys(Keys.ENTER);
			Thread.sleep(1500);
			
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtGapFromDate_dateInput")).sendKeys("05/2012");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtGapToDate_dateInput")).sendKeys("07/2012");
			
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtGapReason")).sendKeys("Education and Employment GAP Comments");
			Thread.sleep(1500);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSaveGapHistory_input")).click();
			Thread.sleep(2000);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSaveNext_input")).click();
			Thread.sleep(7000);
	}
/*/////////////////////  Education GAP ////////////////////////////
			
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlGapType_Arrow")).click();
	Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlGapType_Input")).sendKeys(Keys.DOWN);
	Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlGapType_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtGapFromDate_dateInput")).sendKeys("02-2011");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtGapToDate_dateInput")).sendKeys("03-2011");
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtGapReason")).sendKeys("Education GAP Comments");
	Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSaveGapHistory_input")).click();
	Thread.sleep(2000);

////////////////////  Employment GAP  ///////////////////////////
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlGapType_Arrow")).click();
	Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlGapType_Input")).sendKeys(Keys.DOWN);
	Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlGapType_Input")).sendKeys(Keys.DOWN);
	Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlGapType_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(500);
	// driver.findelement(By.id())
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtGapFromDate_dateInput")).sendKeys("08-2012");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtGapToDate_dateInput")).sendKeys("09-2012");
	
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtGapReason")).sendKeys("Employment GAP Comments");
	Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSaveGapHistory_input")).click();
	Thread.sleep(500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSaveNext_input")).click();
	Thread.sleep(3000);*/
	
	
//////////////////// Disclaimer  ///////////////////////////
	else if(Checkname1.equals("LOA"))	
	{
	driver.findElement(By.id("_rfdSkinnedchkAgree")).click();
	Thread.sleep(2500);
	driver.findElement(By.id("txtCandidateComment")).sendKeys("I have provided all my details as per my certifcates");
	//driver.findElement(By.id("btnCaseDocument")).click();
	Thread.sleep(1500);
	//driver.findElement(By.id("rwCaseDocument_C_grdCaseDocument_ctl00_ctl06_rauCaseDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/3.pdf");
	//driver.findElement(By.id("rwCaseDocument_C_btnAddCaseDocument_input")).click();
	Thread.sleep(1000);
	//driver.findElement(By.xpath("//*[@id='RadWindowWrapper_rwCaseDocument']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
	Thread.sleep(1000);
	driver.findElement(By.id("btnSave")).click();
	Thread.sleep(5000);
	driver.findElement(By.id("cnsubmit")).click();
	Thread.sleep(5000);
	}
		}
		catch(Exception e)
		{
			continue;
		}
	}
	////////////////Client/////////////////////////////////////////
	int confirm = JOptionPane.showConfirmDialog(null, "Do You Want to Open Client Screen", "Alert", JOptionPane.YES_NO_OPTION);
	if(confirm==JOptionPane.YES_OPTION)
	{
	driver.get("http://192.168.2.17:96/clientlogin.aspx");
	//driver.get("http://paws2.com/clientlogin.aspx");
	driver.manage().window().maximize();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demo");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@360");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
	Thread.sleep(2000);
	Rno=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdCandidateList_ctl00__0']/td[8]")).getText();
	Thread.sleep(1500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdCandidateList_ctl00_ctl04_btnSubmitAllComponents")).click();
	Thread.sleep(2000);
	/*driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl19_ddlCreditComponent_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl19_ddlCreditComponent_Input")).sendKeys(Keys.DOWN);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl19_ddlCreditComponent_Input")).sendKeys(Keys.DOWN);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl19_ddlCreditComponent_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl21_ddlCreditComponent_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl21_ddlCreditComponent_Input")).sendKeys(Keys.DOWN);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl21_ddlCreditComponent_Input")).sendKeys(Keys.DOWN);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl21_ddlCreditComponent_Input")).sendKeys(Keys.DOWN);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl21_ddlCreditComponent_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1500);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl23_ddlCreditComponent_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl23_ddlCreditComponent_Input")).sendKeys(Keys.DOWN);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl23_ddlCreditComponent_Input")).sendKeys(Keys.DOWN);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl23_ddlCreditComponent_Input")).sendKeys(Keys.DOWN);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl23_ddlCreditComponent_Input")).sendKeys(Keys.DOWN);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_rgVerificationComponent_ctl00_ctl23_ddlCreditComponent_Input")).sendKeys(Keys.ENTER);*/
	Thread.sleep(1500);
	JPanel mypanel=new JPanel();
	JLabel label=new JLabel("Do you want to set Priority as high");
	mypanel.add(label);
	int priority=JOptionPane.showConfirmDialog(null,mypanel,"Alert", JOptionPane.YES_NO_OPTION);
	//String priority=JOptionPane.showInputDialog("Do you want to set Priority as high Y/N");
	if(priority == JOptionPane.YES_OPTION)
	{
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_ddlPriority_Input")).sendKeys("High");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_txtClientComments")).sendKeys("Do it as soon possible before the date 24/09/2017 candidate must be verified before the given date make it ASAP take it as high priority and complete it???Thank you");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_btnComponentSave")).click();
	WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
	  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
	  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
	  Thread.sleep(3000);
	}
	else
	{
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_txtClientComments")).sendKeys("24.09.2017 Limited period!!!");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwVerificationReadyComponent_C_btnComponentSave")).click();
	WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
	  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
	  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
	  Thread.sleep(3000);
}
	driver.findElement(By.id("ctl00_lnkLogout")).click();
	Thread.sleep(4000);
  
	}
	else
	driver.close();
	/////////////////////////////////////Service Provider/////////////////////////////
	int confirm1 = JOptionPane.showConfirmDialog(null, "Do You Want to Open Service Provider Screen", "Alert", JOptionPane.YES_NO_OPTION);
	if(confirm1==JOptionPane.YES_OPTION)
	{
	driver.get("http://192.168.2.17:96/");
	}
	else
		driver.close();
	//driver.get("http://paws2.com/v2/");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demo1");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
	//driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
	
	 //String baseUrl = "http://192.168.1.22/client123";
	//  String baseUrl = "http://paws2.com/client123";
	//  driver.get(baseUrl);
	 
	//String rno=JOptionPane.showInputDialog("Enter Reference number:\n");
	//String WF=JOptionPane.showInputDialog("Enter the Workflow No"+"\n"+"1.Employee"+"\n"+"2.Client"+"\n"+"3.Candidate"+"\n"+"4.Bulk"+"\n"+"5.IVerify");  
	  //String W=JOptionPane.showInputDialog("Enter the TL Stage to reserve case to TM"+"\n"+"1.Data Entry TL"+"\n"+"2.Data Entry QC TL"+"\n"+"3.Verification TL"+"\n"+"4.Report TL"+"\n"+"5.Report QC TL");
	 // int WW= Integer.parseInt(W);
	 // int WFF = Integer.parseInt(WF);
	   w1= wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("ddlAct")));
	   
	 // if(WW==1)
	 //{	
	   Thread.sleep(2000);
			  Select s= new Select(w1);
			  Thread.sleep(1500);
			  s.selectByValue("3.");
			  Thread.sleep(1000);
			  driver.findElement(By.id("txtCaserefNo")).clear();
			  driver.findElement(By.id("txtCaserefNo")).sendKeys(Rno);
			  driver.findElement(By.id("btnsearch")).click();
			  Thread.sleep(2000);
			  wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/thead/tr/td[2]")).size();
			  //System.out.println(wait);
	if(wait!=0)
	{
		Thread.sleep(3000); 
		String pri=new Select(driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[10]/select"))).getFirstSelectedOption().getText();
		System.out.println(pri);
		if(pri.equals("Normal"))
		{
		new Select(driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[10]/select"))).selectByValue("1");
		Thread.sleep(500);
		driver.findElement(By.xpath("html/body/div[3]/div/div/table/tbody/tr[3]/td/button[1]")).click();
		}
		try
		{
	//String tm= JOptionPane.showInputDialog("Enter Team Member name");
	WebElement w= driver.findElement(By.id("Reserverfor"));
	if(driver.findElement(By.id("Reserverfor")).isDisplayed())
	{
	Select s1= new Select(w);
	s1.selectByValue("0");
	Thread.sleep(1000);
	}
	}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	Thread.sleep(4000);
	Select s1= new Select(w1);
	s1.selectByValue("4");
	int count=driver.findElements(By.id("btnGetNext")).size();
	do
	{
		count=driver.findElements(By.id("btnGetNext")).size();
	}
	while(count==0);	
	/*
	driver.findElement(By.id("txtCaserefNo")).clear();
	driver.findElement(By.id("txtCaserefNo")).sendKeys(Rno);
	driver.findElement(By.id("btnsearch")).click();
	Thread.sleep(4000);
	wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/thead/tr/td[2]")).size();
	//System.out.println(wait);
	 if(wait!=0)
	 {
	  Thread.sleep(1000);
	  driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).click();*/
	 driver.findElement(By.id("btnGetNext")).click();
	  Thread.sleep(5000);
	  TabCheck = driver.findElements(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).size();
	  System.out.println("No of Checks in the Case:"+TabCheck);	
	  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
	  System.out.println(Checkname);

	 
	for(int tc=1;tc<=TabCheck+20;tc++)
	{
	  try
	  {
	////////////////////////////// Address //////////////////////////////
	  
	 if(Checkname.equals("Address"))
	  {
		  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div/iframe")); 
		  driver.switchTo().frame(element);
		Thread.sleep(1500);
		  
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlComponent_Input")).click();
		  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlComponent_DropDown']/div/ul/li")).size();
		  System.out.println("No of Copmonent in Address check = "+dc);
		  for(int i=1;i<dc;i++)
		  {
			  System.out.println("i="+i);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlComponent_Input")).click();
			  Thread.sleep(2000);
			  String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlComponent_DropDown']/div/ul/li")).getText();
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlComponent_DropDown']/div/ul/li")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressComments")).sendKeys(""+ComName+" Comments in DEQCADDRESS");
			 // String se=JOptionPane.showInputDialog("Press 1 to upload document"+"\n"+"Press 2 to Save & submit");
			  //int sei=Integer.parseInt(se);
			  //if(sei==1)
			  //{
			  ///////////DocumentUpload////////////////////////////////////////
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressDocument_input")).click();
			  Thread.sleep(2000);
			  String str=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rwmAddressDocument_C_grdDocumentList_ctl00__0']/td[5]")).getText();
			 System.out.println("doc="+str);
			 //String str1=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rwmAddressDocument_C_grdDocumentList_ctl00__1']/td[5]")).getText();
			// System.out.println("doc1="+str1);
			 if(str.isEmpty())
			  {
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmAddressDocument_C_grdDocumentList_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/14.pdf");
			  Thread.sleep(2000);
			  //driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmAddressDocument_C_btnAddDocument_input")).click();
			  Thread.sleep(3000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmAddressDocument_C_btnDocumentCancel_input")).click();
			  Thread.sleep(2000);
			  }
			  else
			  {
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmAddressDocument_C_btnDocumentCancel_input")).click();  
			  Thread.sleep(2000);
			  }
			  /////////////////////////////////////////////////////////////////////////
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit_input")).click();
			Thread.sleep(5000);
			  dcc=dc-1;
			  if(i==dcc && tc==TabCheck)
			  {
			  Thread.sleep(4000);
			  Alert alert1 = driver.switchTo().alert();
			  alert1.accept();
			  }
			  else if (i==dcc)
			  {
			  Thread.sleep(4000);
			  driver.navigate().refresh(); 
			  Thread.sleep(5000);
			  }
			  else
			  {
			  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
			  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
			  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			  Thread.sleep(3000);
			  }
			  }
			  System.out.println("No of Components in the Address Check :"+dcc);	
			  if(tc!=TabCheck)
			  {
			  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
			  }
			  }
	  
	 if (Checkname.equals("Education"))
	  {
	  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[2]/iframe")); 
	  driver.switchTo().frame(element);
	  Thread.sleep(3000);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationComponent_Input")).click();
	  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationComponent_DropDown']/div/ul/li")).size();
	  //driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationComponent_Arrow")).click();
	  System.out.println("dc = "+dc);
	  for(int i=1;i<dc;i++)
	  {
		  System.out.println("i="+i);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationComponent_Input")).click();
	  Thread.sleep(2000);
	  String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationComponent_DropDown']/div/ul/li")).getText();
	  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationComponent_DropDown']/div/ul/li")).click();
	  Thread.sleep(3000);	  
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationAdditionalComments")).sendKeys(""+ComName+" Comments in DEQCEDU");
	  ////////////////////documentupload////////////////////////////////////////////
	 
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationAddDocuments_input")).click();
	  Thread.sleep(2000);
	  String str=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_gviewEducationDocument_ctl00__0']/td[5]")).getText();
	  System.out.println("edoc="+str);
	  if(str.isEmpty())
	  {
		  Thread.sleep(1500);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_gviewEducationDocument_ctl00_ctl04_rauEducationComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/147.pdf");
	 // driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_btnEducationAddDocument_input")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_btnEducationDocumentCancel_input")).click();
	  Thread.sleep(2000);
	  }
	  else
	  {
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseEducationDocuments_C_btnEducationDocumentCancel_input")).click();
		  Thread.sleep(2000);
	  }
	  ///////////////////////////////////documentupload////////////////////////////////////
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveSubmit_input")).click();
	//  Thread.sleep(4000);
	  
	  dcc=dc-1;
	  if(i==dcc && tc==TabCheck)
	  {
	  Thread.sleep(4000);
	  Alert alert1 = driver.switchTo().alert();
	  alert1.accept();
	  }
	  else if (i==dcc)
	  {
	  Thread.sleep(4000);
	  driver.navigate().refresh(); 
	  Thread.sleep(5000);
	  }
	  else
	  {
	  WebDriverWait await = new WebDriverWait(driver, 15);
	  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
	  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
	  Thread.sleep(3000);
	  }
	  }
	  System.out.println("No of Components in the Education Check :"+dcc);
	  if(tc!=TabCheck)
	  {
	  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
	  }
	  }
		/////////////////////////////////////////////Employment///////////////////////////////////////////// 
	  if (Checkname.equals("Employment"))
	  {
	  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[3]/iframe")); 
	  driver.switchTo().frame(element);
	  Thread.sleep(3000);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentComponent_Input")).click();
	  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentComponent_DropDown']/div/ul/li")).size();
	  //driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentComponent_Arrow")).click();
	  for(int i=1;i<dc;i++)
	  {
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentComponent_Input")).click();
	  Thread.sleep(2000);
	  String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentComponent_DropDown']/div/ul/li")).getText();
	  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentComponent_DropDown']/div/ul/li")).click();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCommentsIfAny")).sendKeys(""+ComName+" Comments in DEQCEMP");
	  ////////////////////documentupload////////////////////////////////////////////
	 
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentDocumentUpload_input")).click();
	  Thread.sleep(2000);
	  String str=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_grdviewEmploymentDocument_ctl00__0']/td[5]/table")).getText();
	  System.out.println("emdoc="+str);
	  if(str.isEmpty())
	  {
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_grdviewEmploymentDocument_ctl00_ctl04_rauEmploymentComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/874.pdf");
	  //driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_btnEmploymentAddDocument_input")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_btnEmploymentDocumentClose_input")).click();
	  Thread.sleep(2000);
	  }
	  else
	  {
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwEmploymentAddDocument_C_btnEmploymentDocumentClose_input")).click();
		  Thread.sleep(2000);
	  }
	  ///////////////////////////////////documentuploadEnd////////////////////////////////////
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSubmit_input")).click();
	//  Thread.sleep(4000);
	  dcc=dc-1;
	  if(i==dcc && tc==TabCheck)
	  {
	  Thread.sleep(5000);
	  Alert alert1 = driver.switchTo().alert();
	  alert1.accept();
	  }
	  else if (i==dcc)
	  {
	  Thread.sleep(4000);
	  driver.navigate().refresh(); 
	  Thread.sleep(5000);
	  }
	  else
	  {
	  WebDriverWait await = new WebDriverWait(driver, 15);
	  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
	  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
	  Thread.sleep(3000);
	  }
	  }
	  System.out.println("No of Components in the Employment Check :"+dcc);
	  if(tc!=TabCheck)
	  {
	  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
	  }
	  }
		  
		  ////////////////////////////////////////////////////ID///////////////////////////////////////////////////
	  if (Checkname.equals("ID"))
	  {
	  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[10]/iframe")); 
	  driver.switchTo().frame(element);
	  Thread.sleep(3000);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdComponent_Input")).click();
	  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdComponent_DropDown']/div/ul/li")).size();
	 //driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdComponent_Input")).click();
	  for(int i=1;i<dc;i++)
	  {
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdComponent_Input")).click();
	  Thread.sleep(2000);
	  String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdComponent_DropDown']/div/ul/li")).getText();
	  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdComponent_DropDown']/div/ul/li")).click();
	  Thread.sleep(3000);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdComments")).sendKeys(""+ComName+" Comments in DEQCID");
	  ////////////////////documentupload////////////////////////////////////////////
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdAddDocuments_input")).click();
	  Thread.sleep(2000);
	  String str=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rwmCaseIdDocuments_C_grdviewIdDocument_ctl00__0']/td[5]")).getText();
	  if(str.isEmpty())
	  {
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseIdDocuments_C_grdviewIdDocument_ctl00_ctl04_rauIdComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/896.pdf");
	 // driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseIdDocuments_C_btnIdAddDocument_input")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseIdDocuments_C_btnIdDocumentCancel_input")).click();
	  Thread.sleep(2000);
	  }
	  else
	  {
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseIdDocuments_C_btnIdDocumentCancel_input")).click();
		  Thread.sleep(2000);
	  }
	  ///////////////////////////////////documentupload////////////////////////////////////
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveSubmit_input")).click();
	  dcc=dc-1;
	  if(i==dcc && tc==TabCheck)
	  {
	  Thread.sleep(4000);
	  Alert alert1 = driver.switchTo().alert();
	  alert1.accept();
	  }
	  else if (i==dcc)
	  {
	  Thread.sleep(4000);
	  driver.navigate().refresh(); 
	  Thread.sleep(5000);
	  }
	  else
	  {
	  WebDriverWait await = new WebDriverWait(driver, 15);
	  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
	  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
	  Thread.sleep(3000);
	  }
	  }
	  System.out.println("No of Components in the ID Check :"+dcc);
	  if(tc!=TabCheck)
	  {
	  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
	  }
	  }
	  
	  ////////////////////////////////Reference/////////////////////////////////////////////////////////
	  if (Checkname.equals("Reference"))
	  {
	  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[4]/iframe")); 
	  driver.switchTo().frame(element);
	  Thread.sleep(3000);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefType_Input")).click();
	  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefType_DropDown']/div/ul/li")).size();
	  //driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefType_Arrow")).click();
	  for(int i=1;i<dc;i++)
	  {
		  Thread.sleep(1500);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefType_Input")).click();
	  Thread.sleep(2000);
	  String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefType_DropDown']/div/ul/li")).getText();
	  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefType_DropDown']/div/ul/li")).click();
	  Thread.sleep(3000);  
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefComments")).sendKeys(""+ComName+" Comments in DEQCREF");
	  ////////////////////documentupload////////////////////////////////////////////
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefDocument_input")).click();
	  Thread.sleep(2000);
	  String str=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rdwRefAddDocument_C_grdDocumentList_Ref_ctl00__0']/td[5]")).getText();
	  if(str.isEmpty())
	  {
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwRefAddDocument_C_grdDocumentList_Ref_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/2586.pdf");
	  //driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwRefAddDocument_C_btnAddDocument_Ref_input")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwRefAddDocument_C_btnDocumentClose_Ref_input")).click();
	  Thread.sleep(2000);
	  }
	  else
	  {
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwRefAddDocument_C_btnDocumentClose_Ref_input")).click();
		  Thread.sleep(2000);
	  }
	  ///////////////////////////////////documentupload////////////////////////////////////
	  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();	  
	  
	  Thread.sleep(2000);
	 
	  dcc=dc-1;
	  if(i==dcc && tc==TabCheck)
	  {
	  Thread.sleep(4000);
	  Alert alert1 = driver.switchTo().alert();
	  alert1.accept();
	  
	  }
	  else if (i==dcc)
	  {
	  Thread.sleep(4000);
	  driver.navigate().refresh(); 
	  Thread.sleep(5000);
	  }
	  else
	  {
	  WebDriverWait await = new WebDriverWait(driver, 15);
	  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
	  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
	  Thread.sleep(3000);
	  }
	  }
	  System.out.println("No of Components in the Reference Check :"+dcc);
	  if(tc!=TabCheck)
	  {
	  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
	  }
	  }
	  
	  //////////////////////////////////////Credit//////////////////////////////////////
	  else if (Checkname.equals("Credit"))
		{
		WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[7]/iframe")); 
	    driver.switchTo().frame(element);
		Thread.sleep(3000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlcreditComponent_Input")).click();
		int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlcreditComponent_DropDown']/div/ul/li")).size();
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlcreditComponent_Arrow")).click();
		for(int i=1;i<dc;i++)
		{
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlcreditComponent_Input")).click();
		Thread.sleep(2000);
		String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlcreditComponent_DropDown']/div/ul/li")).getText();
		driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlcreditComponent_DropDown']/div/ul/li")).click();
		Thread.sleep(3000);
		if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).isEnabled())
		{
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).click();
			String com=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCreditId_DropDown']/div/ul/li[5]")).getText();
			if(com=="Passport")
			{
			driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCreditId_DropDown']/div/ul/li[5]")).click();
			Thread.sleep(1500);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditIdName")).click();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditIdName")).sendKeys("Arun");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditIdNumber")).click();
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditIdNumber")).sendKeys("Pass2541P");
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditSaveSubmit_input")).click();
			Thread.sleep(4000);
			}
			else
			{
				driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCreditId_DropDown']/div/ul/li[6]")).click();
				Thread.sleep(1500);
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditIdName")).click();
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditIdName")).sendKeys("Arun");
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditIdNumber")).click();
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditIdNumber")).sendKeys("PRation2541P");
				driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditSaveSubmit_input")).click();
				Thread.sleep(4000);
				}
		}
		else if(!driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).isEnabled())
		{
			///////////DocumentUpload////////////////////////////////////////
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditAddDocuments_input")).click();
		 Thread.sleep(2000);
		 String str=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rwmCaseCreditDocuments_C_grdviewCreditDocument_ctl00__0']/td[5]")).getText();
		  if(str.isEmpty())
		  {
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseCreditDocuments_C_grdviewCreditDocument_ctl00_ctl04_rauCreditComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/147.pdf");

		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseCreditDocuments_C_btnCreditAddDocument_input")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseCreditDocuments_C_btnCreditDocumentCancel_input")).click();
		Thread.sleep(2000);
		  }
		  else
		  {
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCaseCreditDocuments_C_btnCreditDocumentCancel_input")).click();
				Thread.sleep(2000);
		  }
		 ///////////DocumentUpload////////////////////////////////////////
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditSaveSubmit_input")).click();
		Thread.sleep(4000);
		}
		dcc=dc-1;
		if(i==dcc && tc==TabCheck)
		  {
		  Thread.sleep(4000);
		  Alert alert1 = driver.switchTo().alert();
		  alert1.accept();
		  }
		  else if (i==dcc)
		  {
		  Thread.sleep(4000);
		  driver.navigate().refresh(); 
		  Thread.sleep(5000);
		  }
		  else
		  {
		  WebDriverWait await = new WebDriverWait(driver, 15);
		  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
		  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		  Thread.sleep(3000);
		  }
		}
		System.out.println("No of Components in the Credit Check :"+dcc);
		if(tc!=TabCheck)
		{
		Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
		}
		}		  
	/////////////////////////// Database ///////////////////////////////
	  
	else if (Checkname.equals("DataBase"))
	{

	WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[5]/iframe")); 
	driver.switchTo().frame(element);
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIDComponent_Input")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIDComponent_Input")).sendKeys(Keys.DOWN);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIDComponent_Input")).sendKeys(Keys.DOWN);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIDComponent_Input")).sendKeys(Keys.DOWN);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIDComponent_Input")).sendKeys(Keys.ENTER);
	Thread.sleep(1000);
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_txtDataBaseNameOnID")).sendKeys("Arjun");
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_txtDataBaseIDNumber")).sendKeys("SHG8789");
//    driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIssueCountry_Input")).clear();
//    driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIssueCountry_Input")).sendKeys("India");
//    driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIssueCountry_Input")).sendKeys(Keys.ENTER);
//    Thread.sleep(1500);
//    driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIssueState_Input")).clear();
//    driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIssueState_Input")).sendKeys("Tamil Nadu");
//    driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIssueState_Input")).sendKeys(Keys.ENTER);
//    Thread.sleep(1500);
//    driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIssueCity_Input")).clear();
//    driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIssueCity_Input")).sendKeys("Chennai");
//    driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIssueCity_Input")).sendKeys(Keys.ENTER);
//    Thread.sleep(1500);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_btnDataBaseSaveIDCheckInformation")).click();
//	Thread.sleep(2500);
//	WebDriverWait await = new WebDriverWait(driver, 15);
//	await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
//	driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
//	Thread.sleep(2000);
//	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnDataBaseDocument_input")).click();
//	Thread.sleep(3000);		
//	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmDataBaseDocuments_C_gviewDataBaseDocuments_ctl00_ctl04_rauDataBaseComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/866.pdf");
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ctl00_ContentPlaceHolder1_rwmDataBaseDocuments_C_btnDataBaseAddDocumentsPanel")).click();
//	Thread.sleep(1000);
//	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmDataBaseDocuments_C_btnDataBaseDocumentCancels_input")).click();
//	Thread.sleep(1500);
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_gviewDataBaseQuestionDetails_ctl00_ctl04_txtDataBaseQuestionandAnswer")).sendKeys("DOB Answer");
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_gviewDataBaseQuestionDetails_ctl00_ctl06_txtDataBaseQuestionandAnswer")).sendKeys("DB First Name");
	
    driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnDataBaseSaveSubmit_input")).click();
	//Thread.sleep(4000);
	if(tc==TabCheck)
	{
	Thread.sleep(4000);
	Alert alert1 = driver.switchTo().alert();
	alert1.accept();
	}
	else 
	{
	Thread.sleep(4000);
	driver.navigate().refresh(); 
	Thread.sleep(5000);
	} 
	System.out.println("Presence of DB Check" );
	if(tc!=TabCheck)
	{
	Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
	}
	}
	/////////////////////////// Criminal ///////////////////////////////
	  
	else if (Checkname.equals("Criminal"))
	{
	WebElement element1 = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[6]/iframe")); 
	driver.switchTo().frame(element1);
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCriminalComponent_Input")).click();
	int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCriminalComponent_DropDown']/div/ul/li")).size();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCriminalComponent_Input")).click();
	for(int i=1;i<dc;i++)
	{
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCriminalComponent_Input")).click();
	Thread.sleep(2000);
	String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCriminalComponent_DropDown']/div/ul/li")).getText();
	driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCriminalComponent_DropDown']/div/ul/li")).click();
	Thread.sleep(3000);


	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPoliceStation")).sendKeys(ComName+" Police Station");
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCriminalDocument_input")).click();
	Thread.sleep(1500);
	String str1=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rwmCriminalDocument_C_grdCriminalDocumentList_ctl00__0']/td[5]")).getText();
	  if(str1.isEmpty())
	  {
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCriminalDocument_C_grdCriminalDocumentList_ctl00_ctl04_rauCriminalComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/852.pdf");
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCriminalDocument_C_btnCriminalAddDocument_input")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCriminalDocument_C_btnCriminalDocumentCancel_input")).click();
	Thread.sleep(2000);
	  }
	  else
	  {
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCriminalDocument_C_btnCriminalDocumentCancel_input")).click();
			Thread.sleep(2000);
	  }
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCriminalSubmit_input")).click();
	Thread.sleep(4000);
	dcc=dc-1;
	if(i==dcc && tc==TabCheck)
	{
	Alert alert1 = driver.switchTo().alert();
	alert1.accept();
	}
	else if (i==dcc)
	{
	driver.navigate().refresh(); 
	Thread.sleep(5000);   
	}
	else
	{
	WebDriverWait await1 = new WebDriverWait(driver, 15);
	await1.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
	driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
	Thread.sleep(5000);
	}
	}
	System.out.println("No of Components in the Criminal Check :"+dcc);
	if(tc!=TabCheck)
	{
	Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
	}		  
	}


	/////////////////////////// Court ///////////////////////////////

	else if (Checkname.equals("Court"))
	{
	WebElement element2 = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[8]/iframe")); 
	driver.switchTo().frame(element2);
	Thread.sleep(3000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtComponent_Input")).click();
	int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCourtComponent_DropDown']/div/ul/li")).size();
	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtComponent_Arrow")).click();
	for(int i=1;i<dc;i++)
	{
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtComponent_Input")).click();
	Thread.sleep(2000);
	String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCourtComponent_DropDown']/div/ul/li")).getText();
	driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCourtComponent_DropDown']/div/ul/li")).click();
	Thread.sleep(3000);

	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtDocument_input")).click();
	Thread.sleep(2500);
	String str2=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_rwmCourtDocument_C_grdCourtDocumentList_ctl00__0']/td[5]")).getText();
	  if(str2.isEmpty())
	  {
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtDocument_C_grdCourtDocumentList_ctl00_ctl04_rauCourtComponentDocumentfile0")).sendKeys("/home/ubuntu/Desktop/Ram/Test doc/valid/25.pdf");

	//driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtDocument_C_btnCourtAddDocument_input")).click();
	Thread.sleep(2000);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtDocument_C_btnCourtDocumentCancel_input")).click();
	Thread.sleep(2000);
	  }
	  else
	  {
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtDocument_C_btnCourtDocumentCancel_input")).click();
			Thread.sleep(2000);
	  }
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtComments")).click();
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtComments")).sendKeys(ComName);
	driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtSubmit_input")).click();
	//Thread.sleep(4000);
	dcc=dc-1;
	if(i==dcc && tc==TabCheck)
	{
	Thread.sleep(4000);
	Alert alert1 = driver.switchTo().alert();
	alert1.accept();
	}
	else if (i==dcc)
	{
	Thread.sleep(4000);
	driver.navigate().refresh(); 
	Thread.sleep(5000);
	}
	else
	{
	WebDriverWait await2 = new WebDriverWait(driver, 15);
	await2.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
	driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
	Thread.sleep(3000);
	}
	}
	System.out.println("No of Components in the Court Check :"+dcc);
	if(tc!=TabCheck)
	{
	Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
	}
	}
	  }
	  catch(Exception e)
	  {
		  System.out.println(e);
		  continue;
	  }
	}	///////////////////////////////////////Verification////////////////////////////////////
	driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
	w1= driver.findElement(By.id("ddlAct"));	  
	Select s12= new Select(w1);
			s12.selectByValue("5");
			int countv=driver.findElements(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[4]/select")).size();
			do
			{
				countv=driver.findElements(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[4]/select")).size();	
			}
			while(countv==0);
			//WebElement w22= driver.findElement(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[4]/select"));
			 Select s11=new Select(driver.findElement(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[4]/select")));
			s11.selectByValue("2");			 
			Thread.sleep(3000);
			  driver.findElement(By.id("txtCaserefNo")).clear();
			  driver.findElement(By.id("txtCaserefNo")).sendKeys(Rno);
			  driver.findElement(By.id("btnsearch")).click();
			  Thread.sleep(3000);
			  wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[4]/span")).size();
			  System.out.println(wait);
 if(wait!=0)
 {
	Thread.sleep(1000); 
	try
	{
		//Thread.sleep(3000); 
		String priority1=new Select(driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[10]/select"))).getFirstSelectedOption().getText();
		System.out.println(priority1);
		if(priority1.equals("Normal"))
		{
		new Select(driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[10]/select"))).selectByValue("1");
		Thread.sleep(500);
		driver.findElement(By.xpath("html/body/div[3]/div/div/table/tbody/tr[3]/td/button[1]")).click();
		}
		Thread.sleep(1000);
	WebElement w= driver.findElement(By.id("Reserverfor"));
	if(driver.findElement(By.id("Reserverfor")).isDisplayed())
	{
	Select s2= new Select(w);
	s2.selectByValue("0");
	Thread.sleep(1000);
	}
	}
	catch(Exception e)
	{
		System.out.println(e);
	}
	 }
 else
 {
	 System.out.println("Given Reference No doesnt exists in this Stage");   
 }
 	Thread.sleep(2500);	
 	w1= driver.findElement(By.id("ddlAct"));
 	Select s3= new Select(w1);
	s3.selectByValue("6");
	int count1=driver.findElements(By.id("btnGetNext")).size();
	do
	{
		count1=driver.findElements(By.id("btnGetNext")).size();
	}while(count1==0);
	
	 //WebElement w3= driver.findElement(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[6]/select"));
	//Select s4=new Select(w3);
	// s4.selectByValue("2");
	driver.findElement(By.id("btnGetNext")).click();
//		driver.findElement(By.id("txtCaserefNo")).clear();
//		  driver.findElement(By.id("txtCaserefNo")).sendKeys(Rno);
//		  driver.findElement(By.id("Button3")).click();
//		  Thread.sleep(10000);
//		 wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[4]/span")).size();
//		  System.out.println(wait);
//	if(wait!=0)
//	{
//	 driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).click();
//	}
		 Thread.sleep(5000);
	 TabCheck = driver.findElements(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).size();
	  System.out.println("No of Checks in the Case:"+TabCheck);	
	  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
	  System.out.println(Checkname);

	  for(int tc=1;tc<=TabCheck+100;tc++)
		{
			  try
			  {
		//////////////////////////////Address //////////////////////////////
			  
			  if(Checkname.equals("Address")||Checkname.equals("Address New"))
			  {
				  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div/iframe")); 
				  driver.switchTo().frame(element);
				Thread.sleep(1500);
				 driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlComponent_Input")).click();
				 Thread.sleep(2000);
				  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlComponent_DropDown']/div/ul/li")).size();
				 System.out.println("No of Copmonent in Address check = "+dc);
				  for(int i=1;i<dc;i++)
				  {				  
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_Input")).click();
						Thread.sleep(750);
						driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_DropDown']/div/ul/li[2]")).click();
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnInitiate_input")).click();
						Thread.sleep(2500);
						driver.navigate().refresh(); 
						  Thread.sleep(3000);
				 dcc=dc-1;
				 if(i==dcc && tc==TabCheck)
				  {
				  Thread.sleep(1000);
				  Alert alert19 = driver.switchTo().alert();
				  alert19.accept();
				  }
				  else if (i==dcc)
				  {
				  driver.navigate().refresh(); 
				  Thread.sleep(3000);
				  }
				  else
				  {
				  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
				  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
				  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
				  Thread.sleep(3000);
				  }
				  }
				  System.out.println("No of Components in the Address Check :"+dcc);	
				  if(tc!=TabCheck)
				  {
				  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
				  }
				  }
			  if (Checkname.equals("Education")||Checkname.equals("Education New"))
			  {
			  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[2]/iframe")); 
			  driver.switchTo().frame(element);
			  Thread.sleep(3000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationComponent_Input")).click();
			  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationComponent_DropDown']/div/ul/li")).size();
			  //driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationComponent_Arrow")).click();
			  System.out.println("dc = "+dc);
			  for(int i=1;i<dc;i++)
			  {
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_Input")).click();
					Thread.sleep(750);
					driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_DropDown']/div/ul/li[2]")).click();
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnInitiate_input")).click();
					Thread.sleep(2500);
					driver.navigate().refresh(); 
					  Thread.sleep(3000);
					 dcc=dc-1;
					 if(i==dcc && tc==TabCheck)
					  {
					  Alert alert18 = driver.switchTo().alert();
					  alert18.accept();
					  }
					  else if (i==dcc)
					  {
					  driver.navigate().refresh(); 
					  Thread.sleep(3000);
					  }
					  else
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
					  Thread.sleep(3000);
					  }
					  }
					  System.out.println("No of Components in the Education Check :"+dcc);	
					  if(tc!=TabCheck)
					  {
					  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
					  }
					  }
		
			  if (Checkname.equals("Employment")||Checkname.equals("Employment New"))
			  {
			  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[3]/iframe")); 
			  driver.switchTo().frame(element);
			  Thread.sleep(3000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentComponent_Input")).click();
			  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentComponent_DropDown']/div/ul/li")).size();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentComponent_Arrow")).click();
			  for(int i=1;i<dc;i++)
			  {
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_Input")).click();
				  Thread.sleep(1750);
				  driver.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_DropDown']/div/ul/li[1]")).click();
				  String mode= driver.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_DropDown']/div/ul/li[1]")).getText();
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnInitiate_input")).click();
				  if(mode.contains("Email"))
					{
						Thread.sleep(2000);
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_rw_VrInititiate_Preview_C_txt_vr_recipentMailid")).clear();
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_rw_VrInititiate_Preview_C_txt_vr_recipentMailid")).sendKeys("ramachandran@kadambatechnologies.com");
						driver.findElement(By.id("ctl00_ContentPlaceHolder1_rw_VrInititiate_Preview_C_btnSendmailid_input")).click();
						//Thread.sleep(3000);
					}
					  Thread.sleep(3000);
					 dcc=dc-1;
					 if(i==dcc && tc==TabCheck)
					  {
					  Alert alert17 = driver.switchTo().alert();
					  alert17.accept();
					  }
					  else if (i==dcc)
					  {
					  driver.navigate().refresh(); 
					  Thread.sleep(3000);
					  }
					  else
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
					  Thread.sleep(3000);
					  }
					  }
					  System.out.println("No of Components in the Employment Check :"+dcc);	
					  if(tc!=TabCheck)
					  {
					  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
					  }
					  }
			  if (Checkname.equals("ID")||Checkname.equals("ID New"))
			  {
			  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[10]/iframe")); 
			  driver.switchTo().frame(element);
			  Thread.sleep(3000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdComponent_Input")).click();
			  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdComponent_DropDown']/div/ul/li")).size();
			 // driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdComponent_Arrow")).click();
			  for(int i=1;i<dc;i++)
			  {
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_Input")).click();
					Thread.sleep(750);
					driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_DropDown']/div/ul/li[2]")).click();
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnInitiate_input")).click();
					Thread.sleep(2500);
					driver.navigate().refresh(); 
					  Thread.sleep(3000);
					 dcc=dc-1;
					 if(i==dcc && tc==TabCheck)
					  {
					  Alert alert16 = driver.switchTo().alert();
					  alert16.accept();
					  }
					  else if (i==dcc)
					  {
					  driver.navigate().refresh(); 
					  Thread.sleep(3000);
					  }
					  else
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
					  Thread.sleep(3000);
					  }
					  }
					  System.out.println("No of Components in the ID Check :"+dcc);	
					  if(tc!=TabCheck)
					  {
					  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
					  }
					  }
			  if (Checkname.equals("Reference")||Checkname.equals("Reference New"))
			  {
			  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[4]/iframe")); 
			  driver.switchTo().frame(element);
			  Thread.sleep(3000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefType_Input")).click();
			  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefType_DropDown']/div/ul/li")).size();
			  //driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefType_Arrow")).click();
			  for(int i=1;i<dc;i++)
			  {
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_Input")).click();
					Thread.sleep(750);
					driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_DropDown']/div/ul/li[2]")).click();
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnInitiate_input")).click();
					Thread.sleep(2500);
					driver.navigate().refresh(); 
					  Thread.sleep(3000);
					 dcc=dc-1;
					 if(i==dcc && tc==TabCheck)
					  {
					  Alert alert15 = driver.switchTo().alert();
					  alert15.accept();
					  }
					  else if (i==dcc)
					  {
					  driver.navigate().refresh(); 
					  Thread.sleep(3000);
					  }
					  else
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
					  Thread.sleep(3000);
					  }
					  }
					  System.out.println("No of Components in the Reference Check :"+dcc);	
					  if(tc!=TabCheck)
					  {
					  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
					  }
					  }
			  if (Checkname.equals("DataBase")||Checkname.equals("DataBase New"))
			  {
			  
			  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[5]/iframe")); 
			  driver.switchTo().frame(element);
			  Thread.sleep(3000);
			  new Candidate_Verification();
			  if(tc==TabCheck)
			  {
			  Alert alert14 = driver.switchTo().alert();
			  alert14.accept();
			  }
			  else 
			  {
			  driver.navigate().refresh(); 
			  Thread.sleep(3000);
			  } 
			  System.out.println("Presence of DB Check" );
			  if(tc!=TabCheck)
			  {
			  Checkname=driver.findElement(By.xpath("/html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div/div/ul/li/a/span/span/span")).getText();
			  }
			  }
			  else if (Checkname.equals("Credit")||Checkname.equals("Credit New"))
			  {
			  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[7]/iframe")); 
			  driver.switchTo().frame(element);
			  Thread.sleep(3000);
			  if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).isEnabled())
			  {
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).click();
			  String press=Keys.chord(Keys.DOWN,Keys.ENTER);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).sendKeys(press);
			  }
			  Thread.sleep(2000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlcreditComponent_Input")).click();
			  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlcreditComponent_DropDown']/div/ul/li")).size();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlcreditComponent_Input")).click();
			  for(int i=1;i<dc;i++)
			  {
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_Input")).click();
					Thread.sleep(750);
					driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_DropDown']/div/ul/li[2]")).click();
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnInitiate_input")).click();
					Thread.sleep(2500);
					driver.navigate().refresh(); 
					  Thread.sleep(3000);
					 dcc=dc-1;
					 if(i==dcc && tc==TabCheck)
					  {
					  Alert alert13 = driver.switchTo().alert();
					  alert13.accept();
					  }
					  else if (i==dcc)
					  {
					  driver.navigate().refresh(); 
					  Thread.sleep(3000);
					  }
					  else
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
					  Thread.sleep(3000);
					  }
					  }
					  System.out.println("No of Components in the Credit Check :"+dcc);	
					  if(tc!=TabCheck)
					  {
					  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
					  }
					  }
			  else if (Checkname.equals("Court")||Checkname.equals("Court New"))
			  {
			  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[8]/iframe")); 
			  driver.switchTo().frame(element);
			  Thread.sleep(3000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtComponent_Input")).click();
			  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCourtComponent_DropDown']/div/ul/li")).size();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtComponent_Input")).click();
			  for(int i=1;i<dc;i++)
			  {
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_Input")).click();
					Thread.sleep(750);
					driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_DropDown']/div/ul/li[2]")).click();
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnInitiate_input")).click();
					Thread.sleep(2500);
					driver.navigate().refresh(); 
					  Thread.sleep(3000);
					 dcc=dc-1;
					 if(i==dcc && tc==TabCheck)
					  {
					  Alert alert12 = driver.switchTo().alert();
					  alert12.accept();
					  }
					  else if (i==dcc)
					  {
					  driver.navigate().refresh(); 
					  Thread.sleep(3000);
					  }
					  else
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
					  Thread.sleep(3000);
					  }
					  }
					  System.out.println("No of Components in the Court Check :"+dcc);	
					  if(tc!=TabCheck)
					  {
					  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
					  }
					  }
			  else if (Checkname.equals("Criminal")||Checkname.equals("Criminal New"))
			  {
			  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[6]/iframe")); 
			  driver.switchTo().frame(element);
			  Thread.sleep(3000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCriminalComponent_Input")).click();
			  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCriminalComponent_DropDown']/div/ul/li")).size();
			  //driver.findElement(By.id("//*[@id='ctl00_ContentPlaceHolder1_ddlCriminalComponent_DropDown']/div/ul/li")).click();
			  for(int i=1;i<dc;i++)
			  {
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_Input")).click();
					Thread.sleep(750);
					driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlBeforeVerificationInitiationMode_DropDown']/div/ul/li[2]")).click();
					driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnInitiate_input")).click();
					Thread.sleep(2500);
					driver.navigate().refresh(); 
					  Thread.sleep(3000);
					 dcc=dc-1;
					 if(i==dcc && tc==TabCheck)
					  {
					  Alert alert11 = driver.switchTo().alert();
					  alert11.accept();
					  }
					  else if (i==dcc)
					  {
					  driver.navigate().refresh(); 
					  Thread.sleep(3000);
					  }
					  else
					  {
					  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
					  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
					  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
					  Thread.sleep(3000);
					  }
					  }
					  System.out.println("No of Components in the Criminal Check :"+dcc);	
					  if(tc!=TabCheck)
					  {
					  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
					  }
					  }
			  else
			  {
			  System.out.println("Given Reference No doesnt exists in this Stage");
			  }
			  Thread.sleep(1000);
			  }
				catch(Exception e)
				{
					continue;				
				}
			  }
			//WebElement w31= driver.findElement(By.xpath("//*[@id='srcpnl']/table/tbody/tr[2]/td[6]/select"));
			//Select s41=new Select(w31);
			//s41.selectByValue("2");			
	  Thread.sleep(5500);
	  int si=driver.findElements(By.xpath(".//*[@id='srcpnl']/table/tbody/tr[5]/td[2]/select")).size();
	  do
	  {
		  si=driver.findElements(By.xpath(".//*[@id='srcpnl']/table/tbody/tr[5]/td[2]/select")).size();
	  }while(si==0);
	new Select(driver.findElement(By.xpath(".//*[@id='srcpnl']/table/tbody/tr[5]/td[2]/select"))).selectByValue("1");
	Thread.sleep(750);
	driver.findElement(By.id("txtCaserefNo")).clear();
	driver.findElement(By.id("txtCaserefNo")).sendKeys(Rno);
	driver.findElement(By.id("Button3")).click();
	Thread.sleep(2500);
	wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[4]/span")).size();
	System.out.println(wait);
	if(wait!=0)
	{
	driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).click();
	}
	Thread.sleep(5000);
	TabCheck = driver.findElements(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).size();
	System.out.println("No of Checks in the Case:"+TabCheck);	
	Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
	System.out.println(Checkname);

	for(int tc=1;tc<=TabCheck+60;tc++)
	{
	try
	{
	//////////////////////////////Address //////////////////////////////

		if(Checkname.equals("Address")||Checkname.equals("Address New"))
		{
		  WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div/iframe")); 
		  driver.switchTo().frame(element);
		Thread.sleep(1500);
		 driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlComponent_Input")).click();
		  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlComponent_DropDown']/div/ul/li")).size();
		 System.out.println("No of Copmonent in Address check = "+dc);
		  for(int i=1;i<dc;i++)
		  {
			  
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlComponent_Input")).click();
			  Thread.sleep(2000);
			  String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlComponent_DropDown']/div/ul/li")).getText();
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlComponent_DropDown']/div/ul/li")).click();
			  Thread.sleep(3000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressRespondentName")).sendKeys("Raviteja");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressRespondentDesignation")).sendKeys("Friend");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierName")).sendKeys("Rahulkumar");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierDesignation")).sendKeys("HR");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierPhoneNumber")).sendKeys("+91-44-25874512");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierEmailID")).sendKeys("rahu@gmail.com");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierComments")).sendKeys(ComName);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressVerifierTypeofRevert_Input")).click();
			  Thread.sleep(500);
			  if(i==1)
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAddressVerifierTypeofRevert_DropDown']/div/ul/li[3]")).click();
			  else if(i==2)
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAddressVerifierTypeofRevert_DropDown']/div/ul/li[2]")).click();
			  else
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAddressVerifierTypeofRevert_DropDown']/div/ul/li[1]")).click();
			  Thread.sleep(500);
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressModeOfInitiation_Input")).click();
//			  Thread.sleep(500);
//			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAddressModeOfInitiation_DropDown']/div/ul/li[2]")).click();
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressDateOfInitiation")).sendKeys("20");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressModeOfVerification_Input")).click();
//			  Thread.sleep(500);
//			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAddressModeOfVerification_DropDown']/div/ul/li[4]")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressDateOfVerification")).sendKeys("25");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressAddComments_input")).click();
			  Thread.sleep(1000);
			  String rc=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedAddress")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse1']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(rc);
			  driver.findElement(By.xpath(".//*[@id='accordion']/div[2]/div[1]/h6/a/b")).click();
			  Thread.sleep(750);
			  String rc1=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifierAddressFromDate")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse2']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(rc1);
			  driver.findElement(By.xpath(".//*[@id='accordion']/div[3]/div[1]/h6/a/b")).click();
			  Thread.sleep(750);
			  String rc2=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifierAddressToDate")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse3']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(rc2);
			  //driver.findElement(By.xpath("html/body")).sendKeys("Valid information");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ModalFooterClose_input")).click();
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit_input")).click();
			  Thread.sleep(2500);
//			  else if(i==2)
//			  {
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlComponent_Input")).click();
//			  Thread.sleep(2000);
//			  String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlComponent_DropDown']/div/ul/li")).getText();
//			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlComponent_DropDown']/div/ul/li")).click();
//			  Thread.sleep(3000);
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressRespondentName")).sendKeys("Raviteja");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressRespondentDesignation")).sendKeys("Friend");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierName")).sendKeys("Rahulkumar");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierDesignation")).sendKeys("HR");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierPhoneNumber")).sendKeys("+91-44-25874512");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierEmailID")).sendKeys("rahu@gmail.com");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierComments")).sendKeys(ComName);
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressVerifierTypeofRevert_Input")).click();
//			  Thread.sleep(500);
//			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAddressVerifierTypeofRevert_DropDown']/div/ul/li[2]")).click();
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressModeOfInitiation_Input")).click();
//			  Thread.sleep(500);
//			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAddressModeOfInitiation_DropDown']/div/ul/li[4]")).click();
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressDateOfInitiation")).sendKeys("20");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressModeOfVerification_Input")).click();
//			  Thread.sleep(500);
//			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAddressModeOfVerification_DropDown']/div/ul/li[4]")).click();
//			  Thread.sleep(550);
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_Label2")).click();
//			  driver.findElement(By.id("'ctl00_ContentPlaceHolder1_txtAddressDateOfVerification")).sendKeys("25");
////			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressAddComments_input")).click();
////			  Thread.sleep(1500);	
////			 // driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmAddressComments_C_rEditorAddressComponentCommentsCenter"));
////			 // driver.findElement(By.xpath("html/body")).sendKeys("Some information are missing	");
////			  driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rwmAddressComments']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
//			  Thread.sleep(1000);
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit_input")).click();
//			  Thread.sleep(2500);
//			  }
//			  else
//			  {
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlComponent_Input")).click();
//			  Thread.sleep(2000);
//			  String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlComponent_DropDown']/div/ul/li")).getText();
//			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlComponent_DropDown']/div/ul/li")).click();
//			  Thread.sleep(3000);
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressRespondentName")).sendKeys("Raviteja");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressRespondentDesignation")).sendKeys("Friend");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierName")).sendKeys("Rahulkumar");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierDesignation")).sendKeys("HR");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierPhoneNumber")).sendKeys("+91-44-25874512");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierEmailID")).sendKeys("rahu@gmail.com");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressVerifierComments")).sendKeys(ComName);
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressVerifierTypeofRevert_Input")).click();
//			  Thread.sleep(500);
//			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAddressVerifierTypeofRevert_DropDown']/div/ul/li[1]")).click();
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressModeOfInitiation_Input")).click();
//			  Thread.sleep(500);
//			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAddressModeOfInitiation_DropDown']/div/ul/li[5]")).click();
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtAddressDateOfInitiation")).sendKeys("20");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAddressModeOfVerification_Input")).click();
//			  Thread.sleep(500);			 
//			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAddressModeOfVerification_DropDown']/div/ul/li[4]")).click();
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_Label2")).click();
//			  driver.findElement(By.id("'ctl00_ContentPlaceHolder1_txtAddressDateOfVerification")).sendKeys("25");
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressAddComments_input")).click();
//			  Thread.sleep(1500);			  
//			 // driver.findElement(By.id("//*[@id='ctl00_ContentPlaceHolder1_rwmAddressComments_C']/div[4]"));
//			  //driver.findElement(By.xpath("html/body")).sendKeys("Fake information");
//			  driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rwmAddressComments']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
//			  Thread.sleep(1000);
//			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnAddressSubmit_input")).click();
//			  Thread.sleep(2500);
//			  }
		  dcc=dc-1;
		  if(i==dcc && tc==TabCheck)
		  {
		  Thread.sleep(4000);
		  Alert alert9 = driver.switchTo().alert();
		  alert9.accept();
		  }
		  else if (i==dcc)
		  {
		  Thread.sleep(4000);
		  driver.navigate().refresh(); 
		  Thread.sleep(5000);
		  }
		  else
		  {
		  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
		  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
		  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		  Thread.sleep(3000);
		  }
		  }
		  System.out.println("No of Components in the Address Check :"+dcc);	
		  if(tc!=TabCheck)
		  {
		  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
		  }
		  }		
		//////////////////////////////////////////////Education/////////////////////////////////////////////////////
		if (Checkname.equals("Education")||Checkname.equals("Education New"))
		{
		WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[2]/iframe")); 
		driver.switchTo().frame(element);
		Thread.sleep(3000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationComponent_Input")).click();
		int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationComponent_DropDown']/div/ul/li")).size();
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationComponent_Arrow")).click();
		System.out.println("dc = "+dc);
		for(int i=1;i<dc;i++)
		{
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationComponent_Input")).click();
		  Thread.sleep(2000);
		  String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationComponent_DropDown']/div/ul/li")).getText();
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationComponent_DropDown']/div/ul/li")).click();
		  Thread.sleep(3000);
		  
		  if(ComName.equals("10th")||ComName.equals("Highest 1"))
		  {
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationNameOftheRespondent")).sendKeys("Arun");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationRespondentDesignation")).sendKeys("HR");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationRespondentContactNoEmailID")).sendKeys("8124462621");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierName")).sendKeys("Lokesh");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierDesignation")).sendKeys("TM");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierContactNo")).sendKeys("8125575412");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierEmailID")).sendKeys("jacklin@test.com");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierRemarks")).sendKeys(ComName);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationVerifierMark_Input")).click();
			  Thread.sleep(500);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationVerifierMark_Input")).click();
			  Thread.sleep(500);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationVerifierMark_DropDown']/div/ul/li[3]")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationModeOfInitiation_Input")).click();
			  Thread.sleep(500);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationModeOfInitiation_DropDown']/div/ul/li[2]")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationDateOfInitiation")).sendKeys("28");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationModeOfVerification_Input")).click();
			  Thread.sleep(500);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationModeOfVerification_DropDown']/div/ul/li[6]")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationDateOfVerification")).sendKeys("30");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationAddComments_input")).click();
			  Thread.sleep(1000);
			  String str=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblEducationCommentsVerificationInstituteName")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse1']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str);
			  driver.findElement(By.xpath(".//*[@id='accordion']/div[2]/div[1]/h6/a/b")).click();
			  Thread.sleep(750);
			  String str1=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblEducationCommentsVerificationBoardName")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse2']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str1);
			  driver.findElement(By.xpath(".//*[@id='accordion']/div[3]/div[1]/h6/a/b")).click();
			  Thread.sleep(750);
			  String str2=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblEducationCommentsVerificationInstituteAddress")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse3']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str2);
			  driver.findElement(By.xpath(".//*[@id='accordion']/div[4]/div[1]/h6/a/b")).click();
			  Thread.sleep(750);
			  String str3=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblEducationCommentsVerificationNameOfCourse")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse4']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str3);
			  driver.findElement(By.xpath(".//*[@id='accordion']/div[5]/div[1]/h6/a/b")).click();
			  Thread.sleep(750);
			  String str4=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblEducationCommentsVerificationMajorSubjects")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse5']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str4);
			  driver.findElement(By.xpath(".//*[@id='accordion']/div[6]/div[1]/h6/a/b")).click();
			  Thread.sleep(750);
			  String str5=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblEducationCommentsVerificationTypeOfProgram")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse6']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str5);
			  driver.findElement(By.xpath(".//*[@id='accordion']/div[7]/div[1]/h6/a/b")).click();
			  Thread.sleep(750);
			  String str6=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblEducationCommentsVerificationCandidateNameInCertificate")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse7']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str6);
			  driver.findElement(By.xpath(".//*[@id='accordion']/div[8]/div[1]/h6/a/b")).click();
			  Thread.sleep(750);
			  String str7=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblEducationCommentsVerificationEnrollmentRegistrationNo")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse8']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str7);
			  driver.findElement(By.xpath(".//*[@id='accordion']/div[9]/div[1]/h6/a/b")).click();
			  Thread.sleep(750);
			  String str8=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblEducationCommentsVerificationCGPA")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse9']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str8);
			  driver.findElement(By.xpath(".//*[@id='accordion']/div[10]/div[1]/h6/a/b")).click();
			  Thread.sleep(750);
			  String str9=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblEducationCommentsVerificationCourseCommenceCompletionDate")).getText();
			  driver.findElement(By.xpath(".//*[@id='collapse10']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str9);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ModalFooterClose_input")).click();
			  Thread.sleep(500);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveSubmit_input")).click();
		  }
			  else if (ComName.equals("12th")||ComName.equals("Highest 2"))
			  {
				  if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).isEnabled())
				  {
					  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).click();
					  Thread.sleep(2000);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_DropDown']/div/ul/li[contains(text(), '12th')]")).click();
			  Thread.sleep(3000); 	 
				  }
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationNameOftheRespondent")).sendKeys("Arun");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationRespondentDesignation")).sendKeys("HR");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationRespondentContactNoEmailID")).sendKeys("8124462621");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierName")).sendKeys("Lokesh");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierDesignation")).sendKeys("TM");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierContactNo")).sendKeys("8125575412");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierEmailID")).sendKeys("jacklin@test.com");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierRemarks")).sendKeys(ComName);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationVerifierMark_Input")).click();
				  Thread.sleep(500);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationVerifierMark_Input")).click();
				  Thread.sleep(500);
				  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationVerifierMark_DropDown']/div/ul/li[1]")).click();
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationModeOfInitiation_Input")).click();
				  Thread.sleep(500);
				  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationModeOfInitiation_DropDown']/div/ul/li[2]")).click();
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationDateOfInitiation")).sendKeys("28");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationModeOfVerification_Input")).click();
				  Thread.sleep(500);
				  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationModeOfVerification_DropDown']/div/ul/li[6]")).click();
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationDateOfVerification")).sendKeys("30");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationAddComments_input")).click();
				  Thread.sleep(1000);
				  driver.findElement(By.xpath(".//*[@id='accordion']/div[12]/div[1]/h6/a/b")).click();
				  Thread.sleep(750);
				  driver.findElement(By.xpath(".//*[@id='collapse12']/div/table/tbody/tr/td/div/div/div[3]/div[3]")).sendKeys("Fake information");
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ModalFooterClose_input")).click();
				  Thread.sleep(500);
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveSubmit_input")).click();
			  }
			  else 
			  {
			 // if(ComName.equals("UG1"))
			  //{
			 
			  if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).isEnabled())
			  {
				  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_Input")).click();
				  Thread.sleep(2000);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationHighestComponent_DropDown']/div/ul/li[contains(text(), 'UG1')]")).click();
			  Thread.sleep(3000); 	  
			  }
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationNameOftheRespondent")).sendKeys("Arun");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationRespondentDesignation")).sendKeys("HR");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationRespondentContactNoEmailID")).sendKeys("8124462621");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierName")).sendKeys("Lokesh");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierDesignation")).sendKeys("TM");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierContactNo")).sendKeys("8125575412");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierEmailID")).sendKeys("jacklin@test.com");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationVerifierRemarks")).sendKeys(ComName);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationVerifierMark_Input")).click();
			  Thread.sleep(500);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationVerifierMark_Input")).click();
			  Thread.sleep(500);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationVerifierMark_DropDown']/div/ul/li[2]")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationModeOfInitiation_Input")).click();
			  Thread.sleep(500);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationModeOfInitiation_DropDown']/div/ul/li[2]")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationDateOfInitiation")).sendKeys("28");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEducationModeOfVerification_Input")).click();
			  Thread.sleep(500);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEducationModeOfVerification_DropDown']/div/ul/li[6]")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEducationDateOfVerification")).sendKeys("30");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationAddComments_input")).click();
			  Thread.sleep(1000);
			  driver.findElement(By.xpath(".//*[@id='accordion']/div[12]/div[1]/h6/a/b")).click();
			  Thread.sleep(750);
			  driver.findElement(By.xpath(".//*[@id='collapse12']/div/table/tbody/tr/td/div/div/div[3]/div[3]")).sendKeys("Some information are missing");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ModalFooterClose_input")).click();
			  Thread.sleep(500);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEducationSaveSubmit_input")).click();
			  }	  
		  dcc=dc-1;
		  if(i==dcc && tc==TabCheck)
		  {
		  Thread.sleep(4000);
		  Alert alert8 = driver.switchTo().alert();
		  alert8.accept();
		  }
		  else if (i==dcc)
		  {
		  Thread.sleep(4000);
		  driver.navigate().refresh(); 
		  Thread.sleep(5000);
		  }
		  else
		  {
		  WebDriverWait await = new WebDriverWait(driver, 15);//explicit wait
		  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
		  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		  Thread.sleep(3000);
		  }
		  }
		  System.out.println("No of Components in the Education Check :"+dcc);	
		  if(tc!=TabCheck)
		  {
		  Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
		  }
		}	  
		if (Checkname.equals("Employment")||Checkname.equals("Employment New"))
		{
		WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[3]/iframe")); 
		driver.switchTo().frame(element);
		Thread.sleep(3000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentComponent_Input")).click();
		int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentComponent_DropDown']/div/ul/li")).size();
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentComponent_Arrow")).click();
		for(int i=1;i<dc;i++)
		{
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentComponent_Input")).click();
		Thread.sleep(2000);
		String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentComponent_DropDown']/div/ul/li")).getText();
		driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentComponent_DropDown']/div/ul/li")).click();
		Thread.sleep(3000);

		if(ComName.equals("Current Employment"))
		{
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentRespondentName")).sendKeys("Hari");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentRespondentDesignation")).sendKeys("Case Owner");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentRespondentContactNoEmailID")).sendKeys("8235569871");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierName")).sendKeys("Karthik");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierDesignation")).sendKeys("TM");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierContactNo")).sendKeys("9854412147");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierEmailID")).sendKeys("laoke@test.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierRemarks")).sendKeys(ComName);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentVerifierMark_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentVerifierMark_DropDown']/div/ul/li[3]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentModeOfInitiation_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentModeOfInitiation_DropDown']/div/ul/li[5]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentDateOfInitiation")).sendKeys("20");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentModeOfVerification_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentModeOfVerification_DropDown']/div/ul/li[4]")).click();
		  String press=Keys.chord("20",Keys.TAB);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentDateOfVerification")).sendKeys("20");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentAddComments")).click();
		  Thread.sleep(1000);
		  String str=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedCompanyName")).getText();
		  driver.findElement(By.xpath(".//*[@id='collapse1']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str);
		  driver.findElement(By.xpath(".//*[@id='accordion']/div[2]/div[1]/h6/a/b")).click();
		  Thread.sleep(750);
		  String str1=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedOfficeDetails")).getText();
		  driver.findElement(By.xpath(".//*[@id='collapse2']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str1);
		  driver.findElement(By.xpath(".//*[@id='accordion']/div[3]/div[1]/h6/a/b")).click();
		  Thread.sleep(750);
		  String str2=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedEmployeeCode")).getText();
		  driver.findElement(By.xpath(".//*[@id='collapse3']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str2);
		  driver.findElement(By.xpath(".//*[@id='accordion']/div[4]/div[1]/h6/a/b")).click();
		  Thread.sleep(750);
		  String str3=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedPeriodOfEmployment")).getText();
		  driver.findElement(By.xpath(".//*[@id='collapse4']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str3);
		  driver.findElement(By.xpath(".//*[@id='accordion']/div[5]/div[1]/h6/a/b")).click();
		  Thread.sleep(750);
		  String str4=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedDesignation")).getText();
		  driver.findElement(By.xpath(".//*[@id='collapse5']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str4);
		  driver.findElement(By.xpath(".//*[@id='accordion']/div[6]/div[1]/h6/a/b")).click();
		  Thread.sleep(750);
		  String str5=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedLastDrawnSalary")).getText();
		  driver.findElement(By.xpath(".//*[@id='collapse6']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str5);
		  driver.findElement(By.xpath(".//*[@id='accordion']/div[7]/div[1]/h6/a/b")).click();
		  Thread.sleep(750);
		  String str6=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedContactPerson1")).getText();
		  driver.findElement(By.xpath(".//*[@id='collapse7']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str6);
		  driver.findElement(By.xpath(".//*[@id='accordion']/div[8]/div[1]/h6/a/b")).click();
		  Thread.sleep(750);
		  String str7=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedContactPerson1ContactDetails")).getText();
		  driver.findElement(By.xpath(".//*[@id='collapse8']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str7);
		  driver.findElement(By.id("ModalFooterClose")).click();
		  Thread.sleep(500);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSubmit_input")).click();
		}
		else if(ComName.equals("Employment 2"))
		{
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentRespondentName")).sendKeys("Hari");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentRespondentDesignation")).sendKeys("Case Owner");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentRespondentContactNoEmailID")).sendKeys("8235569871");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierName")).sendKeys("Karthik");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierDesignation")).sendKeys("TM");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierContactNo")).sendKeys("9854412147");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierEmailID")).sendKeys("laoke@test.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierRemarks")).sendKeys(ComName);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentVerifierMark_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentVerifierMark_DropDown']/div/ul/li[2]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentModeOfInitiation_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentModeOfInitiation_DropDown']/div/ul/li[5]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentDateOfInitiation")).sendKeys("20");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentModeOfVerification_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentModeOfVerification_DropDown']/div/ul/li[4]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentDateOfVerification")).sendKeys("29");
		 driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentAddComments")).click();
		 Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='accordion']/div[12]/div[1]/h6/a/b")).click();
		Thread.sleep(750);
		driver.findElement(By.xpath(".//*[@id='collapse12']/div/table/tbody/tr/td/div/div/div[3]/div[3]")).sendKeys("Some Information are missing");
		driver.findElement(By.id("ModalFooterClose")).click();
		  Thread.sleep(500);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSubmit_input")).click();
		}
		else 
		{
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentRespondentName")).sendKeys("Hari");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentRespondentDesignation")).sendKeys("Case Owner");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentRespondentContactNoEmailID")).sendKeys("8235569871");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierName")).sendKeys("Karthik");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierDesignation")).sendKeys("TM");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierContactNo")).sendKeys("9854412147");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierEmailID")).sendKeys("laoke@test.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentVerifierRemarks")).sendKeys(ComName);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentVerifierMark_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentVerifierMark_DropDown']/div/ul/li[1]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentModeOfInitiation_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentModeOfInitiation_DropDown']/div/ul/li[5]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentDateOfInitiation")).sendKeys("20");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlEmploymentModeOfVerification_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlEmploymentModeOfVerification_DropDown']/div/ul/li[4]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtEmploymentDateOfVerification")).sendKeys("29");
		 driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentAddComments")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath(".//*[@id='accordion']/div[12]/div[1]/h6/a/b")).click();
		Thread.sleep(750);
		driver.findElement(By.xpath(".//*[@id='collapse12']/div/table/tbody/tr/td/div/div/div[3]/div[3]")).sendKeys("Some Information are missing");
		driver.findElement(By.id("ModalFooterClose")).click();
		Thread.sleep(500);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnEmploymentSubmit_input")).click();
		}
		dcc=dc-1;
		if(i==dcc && tc==TabCheck)
		{
		Thread.sleep(5000);
		Alert alert7 = driver.switchTo().alert();
		alert7.accept();
		}
		else if (i==dcc)
		{
		Thread.sleep(4000);
		driver.navigate().refresh(); 
		Thread.sleep(5000);
		}
		else
		{
		WebDriverWait await = new WebDriverWait(driver, 15);
		await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
		driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		Thread.sleep(3000);
		}
		}
		System.out.println("No of Components in the Employment Check :"+dcc);
		if(tc!=TabCheck)
		{
		Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
		}
		}
		if (Checkname.equals("ID")||Checkname.equals("ID New"))
		{
		WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[10]/iframe")); 
		driver.switchTo().frame(element);
		Thread.sleep(3000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdComponent_Input")).click();
		int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdComponent_DropDown']/div/ul/li")).size();
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdComponent_Arrow")).click();
		for(int i=1;i<dc;i++)
		{
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdComponent_Input")).click();
		Thread.sleep(2000);
		String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdComponent_DropDown']/div/ul/li")).getText();
		driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdComponent_DropDown']/div/ul/li")).click();
		Thread.sleep(3000);

		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerifierName")).sendKeys("Dinesh");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerifierDesignation")).sendKeys("TM00");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerifierNo")).sendKeys("852145871");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerifierEmail")).sendKeys("karnagi@test.com");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerfierComments")).sendKeys(ComName);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdVerifierMark_Input")).click();
		Thread.sleep(500);
		if(i==1)
		driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdVerifierMark_DropDown']/div/ul/li[3]")).click();
		else if(i==2)
		driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdVerifierMark_DropDown']/div/ul/li[2]")).click();
		else
		driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdVerifierMark_DropDown']/div/ul/li[1]")).click();	
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdModeOfInitiation_Input")).click();
		Thread.sleep(500);
		//driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdModeOfInitiation_DropDown']/div/ul/li[4]")).click();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdModeOfVerification_Input")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdModeOfVerification_DropDown']/div/ul/li[5]")).click();
		Thread.sleep(750);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdAddComments")).click();
		Thread.sleep(1000);
		String str=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedNameOnID")).getText();
		driver.findElement(By.xpath(".//*[@id='collapse1']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str);
		driver.findElement(By.xpath(".//*[@id='accordion']/div[2]/div[1]/h6/a/b")).click();
		Thread.sleep(750);
		String str2=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedIdAddress")).getText();
		driver.findElement(By.xpath(".//*[@id='collapse2']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str2);
		driver.findElement(By.xpath(".//*[@id='accordion']/div[3]/div[1]/h6/a/b")).click();
		Thread.sleep(750);
		String str3=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedIDNumber")).getText();
		driver.findElement(By.xpath(".//*[@id='collapse3']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str3);
		driver.findElement(By.xpath(".//*[@id='accordion']/div[4]/div[1]/h6/a/b")).click();
		Thread.sleep(750);
		String str4=driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblVerifiedIdIssueandExpiryDate")).getText();
		driver.findElement(By.xpath(".//*[@id='collapse4']/div/table/tbody/tr/td[3]/div/div/div[3]/div[3]")).sendKeys(str4);
		driver.findElement(By.id("ModalFooterClose")).click();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveSubmit_input")).click();
		//else if(i==2)
		//{
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerifierName")).sendKeys("Dinesh");
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerifierDesignation")).sendKeys("TM00");
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerifierNo")).sendKeys("852145871");
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerifierEmail")).sendKeys("karnagi@test.com");
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerfierComments")).sendKeys(ComName);
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdVerifierMark_Input")).click();
		//Thread.sleep(500);
		//driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdVerifierMark_DropDown']/div/ul/li[2]")).click();
		////driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdModeOfInitiation_Input")).click();
		////Thread.sleep(500);
		////driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdModeOfInitiation_DropDown']/div/ul/li[3]")).click();
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblIdDateOfInitiation")).click();
		//Thread.sleep(500);
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdModeOfVerification_Input")).click();
		//Thread.sleep(500);
		//driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdModeOfVerification_DropDown']/div/ul/li[5]")).click();
		//Thread.sleep(300);
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdAddComments_input")).click();
		//Thread.sleep(1500);
		////driver.findElement(By.id("ctl00_ContentPlaceHolder1_ctl00_ContentPlaceHolder1_rdwIdAddComments_C_rEditorIdComponentCommentsPanel"));
		////driver.findElement(By.xpath("html/body")).sendKeys("Some Information Missing");
		//driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwIdAddComments']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveSubmit_input")).click();
		//}
		//else
		//{
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerifierName")).sendKeys("Dinesh");
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerifierDesignation")).sendKeys("TM00");
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerifierNo")).sendKeys("852145871");
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerifierEmail")).sendKeys("karnagi@test.com");
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtIdVerfierComments")).sendKeys(ComName);
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdVerifierMark_Input")).click();
		//Thread.sleep(500);
		//driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdVerifierMark_DropDown']/div/ul/li[1]")).click();
		////driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdModeOfInitiation_Input")).click();
		////Thread.sleep(500);
		////driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdModeOfInitiation_DropDown']/div/ul/li[4]")).click();
		////Thread.sleep(300);
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlIdModeOfVerification_Input")).click();
		//Thread.sleep(500);
		//driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlIdModeOfVerification_DropDown']/div/ul/li[5]")).click();
		//Thread.sleep(300);
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdAddComments_input")).click();
		//Thread.sleep(1500);
		////driver.findElement(By.id("ctl00_ContentPlaceHolder1_ctl00_ContentPlaceHolder1_rdwIdAddComments_C_rEditorIdComponentCommentsPanel"));
		////driver.findElement(By.xpath("html/body")).sendKeys("Fake Information");
		//driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwIdAddComments']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnIdSaveSubmit_input")).click();
		//}
		dcc=dc-1;
		if(i==dcc && tc==TabCheck)
		{
		Thread.sleep(5000);
		Alert alert6 = driver.switchTo().alert();
		alert6.accept();
		}
		else if (i==dcc)
		{
		Thread.sleep(4000);
		driver.navigate().refresh(); 
		Thread.sleep(5000);
		}
		else
		{
		WebDriverWait await = new WebDriverWait(driver, 15);
		await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
		driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		Thread.sleep(3800);
		}
		}
		System.out.println("No of Components in the ID Check :"+dcc);

		if(tc!=TabCheck)
		{
		Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
		}
		}
		if(Checkname.equals("Reference")||Checkname.equals("Reference New"))
		{
		WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[4]/iframe")); 
		driver.switchTo().frame(element);
		Thread.sleep(3000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefType_Input")).click();
		int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefType_DropDown']/div/ul/li")).size();
		//driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefType_Arrow")).click();
		for(int i=1;i<dc;i++)
		{
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefType_Input")).click();
		Thread.sleep(2000);
		String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefType_DropDown']/div/ul/li")).getText();
		driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefType_DropDown']/div/ul/li")).click();
		Thread.sleep(3000);

		if(ComName.equals("Reference 1"))
		{
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefNameOfRespondent")).sendKeys("Elavarasan");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefRespondentDesignation")).sendKeys("TL");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefRespondentContactNoEmail")).sendKeys("8255547841");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierName")).sendKeys("Lokesh");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierDesignation")).sendKeys("TM");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierContactNo")).sendKeys("8520010210");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierEmailId")).sendKeys("lokesh@test.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierRemarks")).sendKeys(ComName);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefVerifierMark_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefVerifierMark_DropDown']/div/ul/li[3]")).click();
		//  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefModeOfInitiation_Input")).click();
		//  Thread.sleep(500);
		//  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefModeOfInitiation_DropDown']/div/ul/li[4]")).click();
		//  driver.findElement(By.xpath("ctl00_ContentPlaceHolder1_lblModeOfVerificationDate")).click();
		//  Thread.sleep(1500);
		//  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlModeOfVerification_Input")).click();
		//  Thread.sleep(500);
		//  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlModeOfVerification_DropDown']/div/ul/li[6]")).click();
			  Thread.sleep(1000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblModeOfVerificationDate")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
		  }
		else if(ComName.equals("Reference 2"))
		{
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefNameOfRespondent")).sendKeys("Elavarasan");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefRespondentDesignation")).sendKeys("TL");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefRespondentContactNoEmail")).sendKeys("8255547841");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierName")).sendKeys("Lokesh");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierDesignation")).sendKeys("TM");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierContactNo")).sendKeys("8520010210");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierEmailId")).sendKeys("lokesh@test.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierRemarks")).sendKeys(ComName);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefVerifierMark_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefVerifierMark_DropDown']/div/ul/li[2]")).click();
		//  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefModeOfInitiation_Input")).click();
		//  Thread.sleep(500);
		//  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefModeOfInitiation_DropDown']/div/ul/li[3]")).click();
		//  driver.findElement(By.xpath("ctl00_ContentPlaceHolder1_lblModeOfVerificationDate")).click();
		//  Thread.sleep(1500);
		//  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlModeOfVerification_Input")).click();
		//  Thread.sleep(500);
		//  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlModeOfVerification_DropDown']/div/ul/li[6]")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblModeOfVerificationDate")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
		}
		else
		{
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefNameOfRespondent")).sendKeys("Elavarasan");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefRespondentDesignation")).sendKeys("TL");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefRespondentContactNoEmail")).sendKeys("8255547841");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierName")).sendKeys("Lokesh");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierDesignation")).sendKeys("TM");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierContactNo")).sendKeys("8520010210");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierEmailId")).sendKeys("lokesh@test.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRefVerifierRemarks")).sendKeys(ComName);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefVerifierMark_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefVerifierMark_DropDown']/div/ul/li[1]")).click();
		//  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlRefModeOfInitiation_Input")).click();
		//  Thread.sleep(500);
		//  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlRefModeOfInitiation_DropDown']/div/ul/li[3]")).click();
		//  driver.findElement(By.xpath("ctl00_ContentPlaceHolder1_lblModeOfVerificationDate")).click();
		//  Thread.sleep(1500);
		//  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlModeOfVerification_Input")).click();
		//  Thread.sleep(500);
		//  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlModeOfVerification_DropDown']/div/ul/li[6]")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_lblModeOfVerificationDate")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnRefSaveSubmit_input")).click();
		}
		Thread.sleep(4000);
		dcc=dc-1;
		if(i==dcc && tc==TabCheck)
		{
		Thread.sleep(4000);
		Alert alert5 = driver.switchTo().alert();
		alert5.accept();
		}
		else if (i==dcc)
		{
		Thread.sleep(4000);
		driver.navigate().refresh(); 
		Thread.sleep(5000);
		}
		else
		{
		WebDriverWait await = new WebDriverWait(driver, 15);
		await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
		driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		Thread.sleep(3000);
		}
		}
		System.out.println("No of Components in the Reference Check :"+dcc);
		if(tc!=TabCheck)
		{
		Checkname=driver.findElement(By.xpath("//*[@id='tabStrip']/div/ul/li/a/span/span/span")).getText();
		}
		}
		if (Checkname.equals("DataBase New")||Checkname.equals("DataBase"))
		{	  
		WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[5]/iframe")); 
		driver.switchTo().frame(element);
		Thread.sleep(3000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDataBaseVerifierName")).sendKeys("Lokesh");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDataBaseVerifierDesignation")).sendKeys("TM");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDataBaseVerifierContactNo")).sendKeys("8125547854");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDataBaseVerifierEmailID")).sendKeys("chandru765@gmal.com");
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDataBaseComments")).sendKeys("Database comment");
		driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_dockIDDetails_T']/ul/li/a/span")).click();
		Thread.sleep(250);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIDComponent_Input")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_dockIDDetails_C_ddlDataBaseIDComponent_DropDown']/div/ul/li[2]")).click();
		Thread.sleep(1500);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnDataBaseSaveSubmit_input")).click();
		if(tc==TabCheck)
		{
		Thread.sleep(4000);
		Alert alert4 = driver.switchTo().alert();
		alert4.accept();
		}
		else 
		{
		Thread.sleep(4000);
		driver.navigate().refresh(); 
		Thread.sleep(5000);
		} 
		System.out.println("Presence of DB Check" );
		if(tc!=TabCheck)
		{
		Checkname=driver.findElement(By.xpath("/html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div/div/ul/li/a/span/span/span")).getText();
		}
		}
		else if (Checkname.equals("Credit")||Checkname.equals("Credit New"))
		{
		WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[7]/iframe")); 
		driver.switchTo().frame(element);
		Thread.sleep(3000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlcreditComponent_Input")).click();
		int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlcreditComponent_DropDown']/div/ul/li")).size();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlcreditComponent_Input")).click();
		for(int i=1;i<dc;i++)
		{
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlcreditComponent_Input")).click();
		Thread.sleep(2000);
		String ComName= driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlcreditComponent_DropDown']/div/ul/li")).getText();
		driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlcreditComponent_DropDown']/div/ul/li")).click();
		Thread.sleep(3000);
		if(ComName.equals("Credit Check 1"))
		{
			if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).isEnabled())
			{
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).click();
			String press=Keys.chord(Keys.DOWN,Keys.ENTER);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).sendKeys(press);
			}
			Thread.sleep(2000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreidtVerifierName")).sendKeys("Gayathri");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditVerifierDesignation")).sendKeys("TL");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditVerifierNo")).sendKeys("8124452154");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCredtiVerifierEmail")).sendKeys("dinesh@test.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditVerfierComments")).sendKeys("Credit check 1");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditVerifierMark_Input")).click();
		  Thread.sleep(300);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCreditVerifierMark_DropDown']/div/ul/li[3]")).click();
		  Thread.sleep(350);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditModeOfInitiation_Input")).click();
		  Thread.sleep(300);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCreditModeOfInitiation_DropDown']/div/ul/li[5]")).click();
		  Thread.sleep(500);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditModeOfVerification_Input")).click();
		  Thread.sleep(250);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCreditModeOfVerification_DropDown']/div/ul/li[5]")).click();
		  Thread.sleep(275);
		//  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditAddComments_input")).click();
		//  Thread.sleep(1000);
		//  String str=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwCreditAddComments_C_lblVerifiedNameOnID")).getText();
		//  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwCreditAddComments_C_txtNameOnIDComments")).sendKeys(str);
		//  String str1=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwCreditAddComments_C_lblVerifiedCreditAddress")).getText();
		//  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwCreditAddComments_C_txtCreditAddressComments")).sendKeys(str1);
		//  String str2=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwCreditAddComments_C_lblVerifiedIDNumber")).getText();
		//  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rdwCreditAddComments_C_txtCreditIDNumberComments")).sendKeys(str2);
		//  Thread.sleep(250);
		//  driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwCreditAddComments']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditSaveSubmit_input")).click();
		}
		else if(ComName.equals("Credit Check 2"))
		{
			if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).isEnabled())
			{
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).click();
			String press=Keys.chord(Keys.DOWN,Keys.DOWN,Keys.ENTER);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).sendKeys(press);
			}
			Thread.sleep(2000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreidtVerifierName")).sendKeys("Gayathri");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditVerifierDesignation")).sendKeys("TL");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditVerifierNo")).sendKeys("8124452154");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCredtiVerifierEmail")).sendKeys("dinesh@test.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditVerfierComments")).sendKeys("Credit check 2");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditVerifierMark_Input")).click();
		  Thread.sleep(300);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCreditVerifierMark_DropDown']/div/ul/li[1]")).click();
		  Thread.sleep(350);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditModeOfInitiation_Input")).click();
		  Thread.sleep(300);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCreditModeOfInitiation_DropDown']/div/ul/li[5]")).click();
		  Thread.sleep(500);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditModeOfVerification_Input")).click();
		  Thread.sleep(250);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCreditModeOfVerification_DropDown']/div/ul/li[5]")).click();
		  Thread.sleep(275);
		//  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditAddComments_input")).click();
		//  Thread.sleep(1000);
		// // driver.findElement(By.id("ctl00_ContentPlaceHolder1_ctl00_ContentPlaceHolder1_rdwCreditAddComments_C_rEditorCreditComponentCommentsPanel"));
		//  //driver.findElement(By.xpath("html/body")).sendKeys("Fake Information");
		//  driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwCreditAddComments']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditSaveSubmit_input")).click();
		}
		else
		{
			if(driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).isEnabled())
			{
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).click();
			String press=Keys.chord(Keys.DOWN,Keys.DOWN,Keys.DOWN,Keys.ENTER);
			driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditId_Input")).sendKeys(press);
			}
			Thread.sleep(2000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreidtVerifierName")).sendKeys("Gayathri");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditVerifierDesignation")).sendKeys("TL");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditVerifierNo")).sendKeys("8124452154");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCredtiVerifierEmail")).sendKeys("dinesh@test.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCreditVerfierComments")).sendKeys("Credit check 2");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditVerifierMark_Input")).click();
		  Thread.sleep(300);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCreditVerifierMark_DropDown']/div/ul/li[2]")).click();
		  Thread.sleep(350);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditModeOfInitiation_Input")).click();
		  Thread.sleep(300);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCreditModeOfInitiation_DropDown']/div/ul/li[5]")).click();
		  Thread.sleep(500);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCreditModeOfVerification_Input")).click();
		  Thread.sleep(250);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCreditModeOfVerification_DropDown']/div/ul/li[5]")).click();
		  Thread.sleep(275);
		//  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditAddComments_input")).click();
		//  Thread.sleep(1000);
		//  //driver.findElement(By.id("ctl00_ContentPlaceHolder1_ctl00_ContentPlaceHolder1_rdwCreditAddComments_C_rEditorCreditComponentCommentsPanel"));
		//  //driver.findElement(By.xpath("html/body")).sendKeys("Can't verify Information");
		//  driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwCreditAddComments']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCreditSaveSubmit_input")).click();
		}
		dcc=dc-1;
		if(i==dcc && tc==TabCheck)
		{
		Thread.sleep(4000);
		Alert alert3 = driver.switchTo().alert();
		alert3.accept();
		}
		else if (i==dcc)
		{
		Thread.sleep(4000);
		driver.navigate().refresh(); 
		Thread.sleep(5000);
		}
		else
		{
		WebDriverWait await = new WebDriverWait(driver, 15);
		await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
		driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		Thread.sleep(3000);
		}
		}
		System.out.println("No of Components in the Credit Check :"+dcc);
		if(tc!=TabCheck)
		{
		Checkname=driver.findElement(By.xpath("/html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div/div/ul/li/a/span/span/span")).getText();
		}
		}		 
		else if (Checkname.equals("Court")||Checkname.equals("Court New"))
		{
		WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[8]/iframe")); 
		driver.switchTo().frame(element);
		Thread.sleep(3000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtComponent_Input")).click();
		int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCourtComponent_DropDown']/div/ul/li")).size();
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtComponent_Input")).click();
		Thread.sleep(1500);
		for(int i=1;i<dc;i++)
		{
		  if(i==1)
		  {
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlProceeding_Input")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlProceeding_DropDown']/div/ul/li[3]")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDateofSearch_dateInput")).sendKeys("16/03/2016");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtName")).sendKeys("Section Court");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtType")).sendKeys("Juridic");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtJurisdiction")).sendKeys("Chennai");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtResult_Input")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCourtResult_DropDown']/div/ul/li[2]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtResult_input")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtRespondentName")).sendKeys("Arun");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtRespondentDesignation")).sendKeys("Friend");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierName")).sendKeys("Dinesh");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierDesignation")).sendKeys("HR");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierPhoneNumber")).sendKeys("8124458752");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierEmailID")).sendKeys("dinesh@test.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierComments")).sendKeys("Comments Court");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtVerifierTypeofRevert_Input")).click();
		  Thread.sleep(300);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCourtVerifierTypeofRevert_DropDown']/div/ul/li[3]")).click();
		 /* driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtAddComments_input")).click();
		  Thread.sleep(1500);
		  String str=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_lblVerifiedCourtAddress")).getText();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_txtCommentsForCourtAddress")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_txtCommentsForCourtAddress")).sendKeys(str);
		  String str1=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_lblVerifierCourtFromDate")).getText();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_txtCommentsForFromDate")).sendKeys(str1);
		  String str2=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_lblVerifierCourtToDate")).getText();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_txtCommentsForToDate")).sendKeys(str2);
		  Thread.sleep(1000);
		  driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rwmCourtComments']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();*/
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtSubmit_input")).click();
		  }
		  else if(i==2)
		  {
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlProceeding_Input")).click();
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlProceeding_DropDown']/div/ul/li[3]")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDateofSearch_dateInput")).sendKeys("10/03/2016");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtName")).sendKeys("Section Court");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtType")).sendKeys("Juridic");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtJurisdiction")).sendKeys("Chennai");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtResult_Input")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCourtResult_DropDown']/div/ul/li[3]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtResult_input")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtRespondentName")).sendKeys("Arun");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtRespondentDesignation")).sendKeys("Friend");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierName")).sendKeys("Dinesh");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierDesignation")).sendKeys("HR");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierPhoneNumber")).sendKeys("8124458752");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierEmailID")).sendKeys("dinesh@test.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierComments")).sendKeys("Comments Check Court");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtVerifierTypeofRevert_Input")).click();
		  Thread.sleep(300);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCourtVerifierTypeofRevert_DropDown']/div/ul/li[1]")).click();
		  /*driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtAddComments_input")).click();
		  Thread.sleep(1500);
		  String str=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_lblVerifiedCourtAddress")).getText();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_txtCommentsForCourtAddress")).sendKeys(str);
		  String str1=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_lblVerifierCourtFromDate")).getText();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_txtCommentsForFromDate")).sendKeys(str1);
		  String str2=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_lblVerifierCourtToDate")).getText();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_txtCommentsForToDate")).sendKeys(str2);
		  driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rwmCourtComments']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();*/
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtSubmit_input")).click();
		  }
		  else
		  {
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlProceeding_Input")).click();
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlProceeding_DropDown']/div/ul/li[3]")).click();
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDateofSearch_dateInput")).sendKeys("10/03/2016");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtName")).sendKeys("Section Court");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtType")).sendKeys("Juridic");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtJurisdiction")).sendKeys("Chennai");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtResult_Input")).click();
			  Thread.sleep(1000);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCourtResult_DropDown']/div/ul/li[2]")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtResult_input")).click();
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtRespondentName")).sendKeys("Arun");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtRespondentDesignation")).sendKeys("Friend");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierName")).sendKeys("Dinesh");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierDesignation")).sendKeys("HR");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierPhoneNumber")).sendKeys("8124458752");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierEmailID")).sendKeys("dinesh@test.com");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCourtVerifierComments")).sendKeys("Comments Check Court");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCourtVerifierTypeofRevert_Input")).click();
			  Thread.sleep(300);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCourtVerifierTypeofRevert_DropDown']/div/ul/li[2]")).click();
			  /*driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtAddComments_input")).click();
			  Thread.sleep(1500);
			  String str=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_lblVerifiedCourtAddress")).getText();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_txtCommentsForCourtAddress")).sendKeys(str);
			  String str1=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_lblVerifierCourtFromDate")).getText();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_txtCommentsForFromDate")).sendKeys(str1);
			  String str2=driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_lblVerifierCourtToDate")).getText();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_rwmCourtComments_C_txtCommentsForToDate")).sendKeys(str2);
			  driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rwmCourtComments']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();*/
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCourtSubmit_input")).click();
		  }
		  
		  dcc=dc-1;
		  if(i==dcc && tc==TabCheck)
		  {
		  Thread.sleep(4000);
		  Alert alert2 = driver.switchTo().alert();
		  alert2.accept();
		  }
		  else if (i==dcc)
		  {
		  Thread.sleep(4000);
		  driver.navigate().refresh(); 
		  Thread.sleep(5000);
		  }
		  else
		  {
		  WebDriverWait await = new WebDriverWait(driver, 15);
		  await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
		  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		  Thread.sleep(3000);
		  }
		  }
		  System.out.println("No of Components in the Court Check :"+dcc);
		  if(tc!=TabCheck)
		  {
		  Checkname=driver.findElement(By.xpath("/html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div/div/ul/li/a/span/span/span")).getText();
		  }
		  }
		else if (Checkname.equals("Criminal")||Checkname.equals("Criminal New"))
		{
		WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[6]/iframe")); 
		driver.switchTo().frame(element);
		Thread.sleep(3000);
		driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCriminalComponent_Input")).click();
		int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCriminalComponent_DropDown']/div/ul/li")).size();
		//driver.findElement(By.id("//*[@id='ctl00_ContentPlaceHolder1_ddlCriminalComponent_DropDown']/div/ul/li")).click();
		for(int i=1;i<dc;i++)
		{
		if(i==1)
		{
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalRespondentName")).sendKeys("Ganesh");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierName")).sendKeys("Abdul");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierDesignation")).sendKeys("Team Leader");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierPhoneNumber")).sendKeys("9841214588");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierEmailID")).sendKeys("chandru123@testmail.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierComments")).sendKeys("Criminal Check Verified");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCriminalVerifierTypeofRevert_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCriminalVerifierTypeofRevert_DropDown']/div/ul/li[2]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCriminalSubmit_input")).click();
		}
		else if(i==2)
		{
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalRespondentName")).sendKeys("Ganesh");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierName")).sendKeys("Abdul");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierDesignation")).sendKeys("Team Leader");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierPhoneNumber")).sendKeys("9841214588");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierEmailID")).sendKeys("chandru123@testmail.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierComments")).sendKeys("Criminal Check Verified");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCriminalVerifierTypeofRevert_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCriminalVerifierTypeofRevert_DropDown']/div/ul/li[1]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCriminalSubmit_input")).click();
		}
		else
		{
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalRespondentName")).sendKeys("Ganesh");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierName")).sendKeys("Abdul");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierDesignation")).sendKeys("Team Leader");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierPhoneNumber")).sendKeys("9841214588");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierEmailID")).sendKeys("chandru123@testmail.com");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtCriminalVerifierComments")).sendKeys("Criminal Check Verified");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlCriminalVerifierTypeofRevert_Input")).click();
		  Thread.sleep(500);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlCriminalVerifierTypeofRevert_DropDown']/div/ul/li[3]")).click();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnCriminalSubmit_input")).click();
		}
		dcc=dc-1;
		if(i==dcc && tc==TabCheck)
		{
		Alert alert1 = driver.switchTo().alert();
		alert1.accept();
		}
		else if (i==dcc)
		{
		driver.navigate().refresh(); 
		Thread.sleep(5000);   
		}
		else
		{
		WebDriverWait await = new WebDriverWait(driver, 15);
		await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
		driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		Thread.sleep(3000);
		}
		}
		System.out.println("No of Components in the Criminal Check :"+dcc);
		if(tc!=TabCheck)
		{
		Checkname=driver.findElement(By.xpath("/html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div/div/ul/li/a/span/span/span")).getText();
		}		  
		}	 
		if(Checkname.equals("Drug & Medical")||Checkname.equals("Drug & Medical New")) 
		{
		 WebElement element = driver.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div[9]/iframe")); 
		  driver.switchTo().frame(element);
		  Thread.sleep(3000);
		 // driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlDrugMedicalComponent_Input")).click();
		  int dc=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlDrugMedicalComponent_DropDown']/div/ul/li")).size();
		  System.out.println(dc);
		  //driver.findElement(By.id("//*[@id='ctl00_ContentPlaceHolder1_ddlCriminalComponent_DropDown']/div/ul/li")).click();
		  for(int i=2;i<=dc;i++)
		  {
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlDrugMedicalComponent_Arrow")).click();
		  Thread.sleep(1500);
		  String str1=driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlDrugMedicalComponent_DropDown']/div/ul/li["+i+"]")).getText();
		  System.out.println(str1);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlDrugMedicalComponent_DropDown']/div/ul/li["+i+"]")).click();
		  Thread.sleep(2000);
		  if(str1.equals("Medical Test"))
		  {
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtFitnessStatusoftheSubject")).sendKeys("positive");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtConsultationwiththephysician")).sendKeys("Anbu selvam");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalComments")).sendKeys("Verified positive");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalRespondentName")).sendKeys("Jacklin");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalRespondentDesignation")).sendKeys("Team Analyst");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRespondentContactNoEmail")).sendKeys("8124458745");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalVerifierName")).sendKeys("Arun");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalVerifierDesignation")).sendKeys("Team Analyst");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnDrugMedicalSaveSubmit_input")).click();			  
		  }
		  else if(str1.equals("Panel1")||str1.equals("Panel3")||str1.equals("Panel4"))
		  {
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlDrugMedicalElement_Input")).click();
			  Thread.sleep(1000);
			  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlDrugMedicalElement_DropDown']/div/ul/li[2]")).click();
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlDrugMedicalResult_Input")).click();
			  Thread.sleep(1000);
			 driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_dvDrug']/ul/li[2]/table/tbody/tr/td[1]")).click();
			 Thread.sleep(1500);
			  //driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlDrugMedicalResult_Input")).click();
			 // driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlDrugMedicalResult_DropDown']/div/ul/li[2]")).click();
			  //driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_dvDrug']/ul/li[3]/table/tbody/tr/td[2]"));
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalResultComments")).click();
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalResultComments")).sendKeys("Verified Positive");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnDrugMedicalAdd_input")).click();
			  Thread.sleep(1000);
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalRespondentName")).sendKeys("Jacklin");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalRespondentDesignation")).sendKeys("Team Analyst");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtRespondentContactNoEmail")).sendKeys("8124458745");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalVerifierName")).sendKeys("Arun");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtDrugMedicalVerifierDesignation")).sendKeys("Team Analyst");
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnDrugMedicalSaveSubmit_input")).click();			  
		    }
		  dcc=dc-1;
		  if(driver.findElement(By.cssSelector("span.rwInnerSpan")).isDisplayed())
			 {
				 driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
			 }
			 else
				 driver.navigate().refresh();
		  Thread.sleep(3500);
		//  if(i==dcc && tc==TabCheck)
		//  {
		//  Alert alert = driver.switchTo().alert();
		//  alert.accept();
		//  }
		//  else if (i==dcc)
		//  {
		//  driver.navigate().refresh(); 
		//  Thread.sleep(5000);   
		//  }
		//  else
		//  {
		// // WebDriverWait await = new WebDriverWait(driver, 15);
//			  Thread.sleep(2500);
		//  //await.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan")));
		//  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		//  Thread.sleep(3000);
		//  }
		  i=i-1;
		  }
		  System.out.println("No of Components in the Drug & Medical Check :"+dcc);
		  driver.switchTo().defaultContent();
		  if(tc!=TabCheck)
		  {
		  Checkname=driver.findElement(By.xpath("/html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div/div/ul/li/a/span/span/span")).getText();
		  }		  
		  }	  
		}
		catch(Exception e)
		{
		  System.out.println(e);
		  continue;
		}
		}
	///////////////////////////////////////////////REPORT GENERATION////////////////////////////
	
	Thread.sleep(5000);
	w1= driver.findElement(By.id("ddlAct"));
	Select s2= new Select(w1);
	s2.selectByValue("7");
	Thread.sleep(1000);	

	driver.findElement(By.id("txtCaserefNo")).clear();
	driver.findElement(By.id("txtCaserefNo")).sendKeys(Rno);
	driver.findElement(By.id("btnsearch")).click();
	Thread.sleep(4000);
	WebElement w= driver.findElement(By.id("Reserverfor"));
	Select s31= new Select(w);
	s31.selectByValue("0");
	Thread.sleep(2000);
	Select s21= new Select(w1);
	s21.selectByValue("8");
	Thread.sleep(3000);	
	driver.findElement(By.id("btnGetNext")).click();
	Thread.sleep(5000);
	/*wait = driver.findElements(By.xpath("//*[@id='grdTaskList']/thead/tr/td[2]")).size();
	//System.out.println(wait);
	if(wait!=0)
	{
	Thread.sleep(1000);
	driver.findElement(By.xpath("//*[@id='grdTaskList']/tbody/tr[1]/td[3]")).click();
	Thread.sleep(5000);*/
	driver.findElement(By.id("btnReportGenerate")).click();

	Thread.sleep(6000);
	driver.findElement(By.xpath("//*[@id='_rfdSkinnedrwReportComponent_C_grdReportComponent_ctl00_ctl02_ctl00_chkReportSelectSelectCheckBox']")).click();
	//driver.findElement(By.xpath("//*[@id='_rfdSkinnedrwReportComponent_C_grdReportComponent_ctl00_ctl14_chkReportSelectSelectCheckBox']")).click();
	Thread.sleep(500);
	driver.findElement(By.xpath("//*[@id='rwReportComponent_C']/table/tbody/tr[2]/td[2]")).sendKeys("Report completed");

	driver.findElement(By.id("rwReportComponent_C_ddlTemplate_Input")).click();
	Thread.sleep(2000);
	driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlTemplate_DropDown']/div/ul/li[contains(text(), 'GMR Final Report')]")).click();
	Thread.sleep(2000);

	driver.findElement(By.id("rwReportComponent_C_ddlReportHeader_Input")).click();
	Thread.sleep(2000);
	driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlReportHeader_DropDown']/div/ul/li[contains(text(), 'Final')]")).click();
	Thread.sleep(2000);

	driver.findElement(By.id("rwReportComponent_C_ddlCaseStatus_Input")).click();
	Thread.sleep(2000);
	driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlCaseStatus_DropDown']/div/ul/li[contains(text(), 'Clear')]")).click();
	Thread.sleep(2000);
	   
	 // driver.findElement(By.id("rwReportComponent_C_btnRptPreview_input")).click();
	  Thread.sleep(1000);
	  driver.findElement(By.id("rwReportComponent_C_btnSubmitReport_input")).click();
	  Thread.sleep(10000);
	  Alert alert2 = driver.switchTo().alert();
		alert2.accept();
		Thread.sleep(5000);
		System.out.println("Report Previwed and submitted");

	////////////////////////////////////Report Generation End//////////////////////////////////////
		
		///////////////////////////////Report Validation///////////////////////////////////
		
		Thread.sleep(7000);
		w1= driver.findElement(By.id("ddlAct"));
		Select s5= new Select(w1);
		s5.selectByValue("9");
		Thread.sleep(1000);	

		driver.findElement(By.id("txtCaserefNo")).clear();
		driver.findElement(By.id("txtCaserefNo")).sendKeys(Rno);
		driver.findElement(By.id("btnsearch")).click();
		Thread.sleep(4000);
		w= driver.findElement(By.id("Reserverfor"));
		Select s6= new Select(w);
		s6.selectByValue("0");
		Thread.sleep(2000);
		Select s7= new Select(w1);
		s7.selectByValue("10");
		Thread.sleep(4000);	
		driver.findElement(By.id("btnGetNext")).click();
		Thread.sleep(5000);
		 driver.findElement(By.id("btnReportGenerate")).click();
	     
	     Thread.sleep(6000);
	    // WebElement e=driver.findElement(By.xpath("//*[@id='_rfdSkinnedrwReportComponent_C_grdReportComponent_ctl00_ctl02_ctl00_chkReportSelectSelectCheckBox']"));
	   
	    if(!driver.findElement(By.id("_rfdSkinnedrwReportComponent_C_grdReportComponent_ctl00_ctl02_ctl00_chkReportSelectSelectCheckBox")).isSelected())
	    {
	    driver.findElement(By.id("_rfdSkinnedrwReportComponent_C_grdReportComponent_ctl00_ctl04_chkReportSelectSelectCheckBox")).click();
	     //driver.findElement(By.xpath("//*[@id='_rfdSkinnedrwReportComponent_C_grdReportComponent_ctl00_ctl14_chkReportSelectSelectCheckBox']")).click();
	     Thread.sleep(6000);
	     driver.findElement(By.id("rwReportComponent_C_ddlTemplate_Input")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlTemplate_DropDown']/div/ul/li[contains(text(), 'PWC Report')]")).click();
		  Thread.sleep(2000);
		  
		  driver.findElement(By.id("rwReportComponent_C_ddlReportHeader_Input")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlReportHeader_DropDown']/div/ul/li[contains(text(), 'Final')]")).click();
		  Thread.sleep(2000);
		  
		  driver.findElement(By.id("rwReportComponent_C_ddlCaseStatus_Input")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlCaseStatus_DropDown']/div/ul/li[contains(text(), 'Clear')]")).click();
		  Thread.sleep(2000);

	     
	        
	     driver.findElement(By.id("rwReportComponent_C_btnRptPreview_input")).click();
	       Thread.sleep(5000);
	       driver.findElement(By.id("rwReportComponent_C_btnPublishReport_input")).click();
	       Thread.sleep(10000);
	       Alert alert4 = driver.switchTo().alert();
			alert4.accept();
			Thread.sleep(10000);
			System.out.println("Report Published");
			//driver.close();
	    }
	    else 
	    {	      
	     driver.findElement(By.id("rwReportComponent_C_ddlTemplate_Input")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlTemplate_DropDown']/div/ul/li[contains(text(), 'PWC Report')]")).click();
		  Thread.sleep(2000);
		  
		  driver.findElement(By.id("rwReportComponent_C_ddlReportHeader_Input")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlReportHeader_DropDown']/div/ul/li[contains(text(), 'Final')]")).click();
		  Thread.sleep(2000);
		  
		  driver.findElement(By.id("rwReportComponent_C_ddlCaseStatus_Input")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id='rwReportComponent_C_ddlCaseStatus_DropDown']/div/ul/li[contains(text(), 'Clear')]")).click();
		  Thread.sleep(2000);	     
	        
	     driver.findElement(By.id("rwReportComponent_C_btnRptPreview_input")).click();
	       Thread.sleep(5000);
	       driver.findElement(By.id("rwReportComponent_C_btnPublishReport_input")).click();
	       Thread.sleep(10000);
	       Alert alert3 = driver.switchTo().alert();
			alert3.accept();
			Thread.sleep(10000);
			System.out.println("Report Published");
	    }

	}
	}
	
	  



