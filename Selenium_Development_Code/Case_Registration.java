package Checks360;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileWriter;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.math.Fraction;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Case_Registration extends txt_write {
    WebDriverWait wait=new WebDriverWait(Driver.instance,50);
	private static String newcase="btnNewCase_Click";
	private static String addnew="ctl00_ContentPlaceHolder1_btnAddNext_input";
	private static String selectclient="ctl00_ContentPlaceHolder1_ddlClient_Arrow";
	private static String selectproject="ctl00_ContentPlaceHolder1_ddlProject_Arrow";
	private static String projectoption=".//*[@id='ctl00_ContentPlaceHolder1_ddlProject_DropDown']/div/ul/li";
	private static String clientoption="//*[@id='ctl00_ContentPlaceHolder1_ddlClient_DropDown']/div/ul/li";
	String uuid = UUID.randomUUID().toString();
	int uuid_len=uuid.replaceAll("[0-9]","").replaceAll("[-]","").length();
	int casesize,insuffsize,a=0,b=0;
	String compnamelist[]=new String[100];
	WebElement upload;
	String doc,insuffdoc,comdoc,casedocname,comdocname,dcasedocname,dcomdocname,compname;
	private static int clientsize;
	private static int projectsize;
	private static String clients;
	private static String projects;
	private static int gendersize;
	private static String genders;
	private static int Componentsize=150;
	private static String gender="Male";
	private static int maritalsize;
	private static String maritals;
	private static String marital="Married";
	private static int IDsize;
	private static String IDs;
	private static String ID="Employee ID";
	private static String firstnamepath="ctl00_ContentPlaceHolder1_txtFirstName";
	private static String lastnamepath="ctl00_ContentPlaceHolder1_txtLastName";
	private static String DOBpath="ctl00_ContentPlaceHolder1_txtDateofBirth_dateInput";
	private static String DOB="21/09/1992";
	private static String selectgender="ctl00_ContentPlaceHolder1_ddlGender_Arrow";
	private static String genderoption=".//*[@id='ctl00_ContentPlaceHolder1_ddlGender_DropDown']/div/ul/li";
	private static String emailpath="ctl00_ContentPlaceHolder1_txtEmail";
	private  String email=uuid.replaceAll("[0-9]","").replaceAll("[-]","").substring(0,5);
	private static String fathersfirstnamepath="ctl00_ContentPlaceHolder1_txtFatherFirstName";
	private static String fatherslastnamepath="ctl00_ContentPlaceHolder1_txtFatherLastName";
	private static String linkedinpath="ctl00_ContentPlaceHolder1_txtLinkedIn";
	private  String linkedin=uuid.replaceAll("[0-9]","").replaceAll("[-]","").substring(0,5); 
	private static String selectmarital="ctl00_ContentPlaceHolder1_ddlMaritalStatus_Arrow";
	private static String maritaloption=".//*[@id='ctl00_ContentPlaceHolder1_ddlMaritalStatus_DropDown']/div/ul/li";
	private static String nationalitypath="ctl00_ContentPlaceHolder1_txtNationality";
	private  String nationality=uuid.replaceAll("[0-9]","").replaceAll("[-]","").substring(0,5);
	private static String landlinenumberpath="ctl00_ContentPlaceHolder1_txtLandLine";
	private static String landlinenumber=RandomStringUtils.randomNumeric(10);
	private static String mobilenumberpath="ctl00_ContentPlaceHolder1_txtMobileNumberFirst";
	private static String mobilenumber=RandomStringUtils.randomNumeric(10);
	private static String emergencycontactnumberpath="ctl00_ContentPlaceHolder1_txtMobileNumberSecond";
	private static String emergencycontactnumber=RandomStringUtils.randomNumeric(10);
	private static String employeeid=RandomStringUtils.randomNumeric(5);
	private static String emergencycontactpersonpath="ctl00_ContentPlaceHolder1_txtContactPerson";
	private String emergencycontactperson=uuid.replaceAll("[0-9]","").replaceAll("[-]","").substring(0,5);
	private static String fresherpath="ctl00_ContentPlaceHolder1_rblFresher_0";
	private static String exppath="ctl00_ContentPlaceHolder1_rblFresher_1";
	private static String selectid="ctl00_ContentPlaceHolder1_ddlIDType_Arrow";
	private static String idoption=".//*[@id='ctl00_ContentPlaceHolder1_ddlIDType_DropDown']/div/ul/li";
	private static String idpath="ctl00_ContentPlaceHolder1_txtClientCandidateID";
	public String idvalue=uuid.replaceAll("[^0-9]","").replaceAll("[-]","").substring(0,5);
	private static String addcomponent="ctl00_ContentPlaceHolder1_btnAddComponent_input";
	private  String firstname=uuid.replaceAll("[0-9]","").replaceAll("[-]","").substring(0,5);
	private  String lastname=uuid.replaceAll("[0-9]","").replaceAll("[-]","").substring(uuid_len-4,uuid_len);
	public static String casedocview="ctl00_ContentPlaceHolder1_rwCaseDocument_C_grdCaseDocument_ctl00_ctl06_rauCaseDocumentrow0";
	public static String casedocbutton="ctl00_ContentPlaceHolder1_btnCaseDocument_input";
	public static String casedocupload="ctl00_ContentPlaceHolder1_rwCaseDocument_C_grdCaseDocument_ctl00_ctl06_rauCaseDocumentfile0";
	public static String adddocument="ctl00_ContentPlaceHolder1_rwCaseDocument_C_btnAddCaseDocument_input";
	public static String adddocumentcom="ctl00_ContentPlaceHolder1_rdmAddDoc_C_btnAddDocument_input";
	public static String docdownload="ctl00_ContentPlaceHolder1_rwCaseDocument_C_grdCaseDocument_ctl00_ctl06_lnkbtnDownload1";
	public static String docclose="ctl00_ContentPlaceHolder1_rwCaseDocument_C_btnCaseDocumentCancel";
	public static String savesubmit="ctl00_ContentPlaceHolder1_btnSaveSubmit_input";
	public static String home="ctl00_imgHome";
	public static String action="btnActions";
	public static String stagedropdown="ddlAct";
	public static String actionstage="ctl00_ContentPlaceHolder1_ddlAct_Arrow";
	static String viewcif="btnViewCIF";
	static String buttonview="btnView";
	String coinsuff="ctl00_ContentPlaceHolder1_ddlWorkflowType_Arrow";
	String spinsuff=".//*[@id='ctl00_ContentPlaceHolder1_ddlWorkflowType_DropDown']/div/ul/li[1]";
	static String workstage="ctl00_ContentPlaceHolder1_btnStages_input";
	static String actioncaserefsearch="ctl00_ContentPlaceHolder1_TextBoxCaseReference";
	static String actioncasesearch="ctl00_ContentPlaceHolder1_btnSearch";
	static String caseinsuff=".//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[5]";
	static String caseinsuffclear="ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl04_btnClearInsuff";
	static String clearinsuff="ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_btnClear_input";
	String insuffdocpath="//*[@id='ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00__0']/td[6]";
	String insuffadddoc="ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_btnSubmitAddedDocument";
	String insuffclosedoc="//*[@id='RadWindowWrapper_ctl00_ContentPlaceHolder1_rdwDocumentUpload']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a";
	static String workcaseref="txtCaserefNo";
	static String workcasesearch="btnsearch";
	String casename=".//*[@id='grdTaskList']/tbody/tr[1]/td[4]/span";
	String closedoc="ctl00_ContentPlaceHolder1_rdmAddDoc_C_btnDocumentCancel";
	public String casedocnameid="ctl00_ContentPlaceHolder1_rwCaseDocument_C_grdCaseDocument_ctl00_ctl06_lnkFileName";
	public String comdocnameid="ctl00_ContentPlaceHolder1_rdmAddDoc_C_grdDocumentList_ctl00_ctl04_lnkFileName";
	public String priority=".//*[@id='grdTaskList']/tbody/tr[1]/td[10]/select";
	public static String priorityes="html/body/div[3]/div/div/table/tbody/tr[3]/td/button[1]";
	public static String reservedfor="Reserverfor";
	public static String getnext="btnGetNext";
	public static String casedocedit="btnCaseSave_input";
	public static String casedoceditbutton="btnCaseDocument";
	public static String adddoceditbutton="rwmCaseDocuments_C_btnCaseAddDocuments";
	public static String casedoceditnameid="rwmCaseDocuments_C_gviewCaseDocuments_ctl00_ctl06_lnkCaseFileName";
	public static String casedoceditcancelid="rwmCaseDocuments_C_btnCaseDocumentCancels";	
	public static String comdoceditcancelid="ctl00_ContentPlaceHolder1_rwmAddressDocument_C_btnDocumentCancel_input";
	public static String cepstageid=".//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[4]";
	public static String cepstagesize=".//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00']/tbody/tr";
	public static String cepsubmitbutton="ctl00_ContentPlaceHolder1_rdwClearCEP_C_ButtonSubmit_input";
	public static String cepdocupload="ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00_ctl04_rauComponentDocumentfile0";
	public static String cepfilename=".//*[@id='ctl00_ContentPlaceHolder1_rdwClearCEP_C_grdDocumentList_ctl00__0']/td[6]";
	public static String cepdocsave="ctl00_ContentPlaceHolder1_rdwClearCEP_C_btnAddDocument";
	public static String cepraisecommentsid="ctl00_ContentPlaceHolder1_rdwClearCEP_C_LabelRaisedRemarks";
	public static String cepclearcommentsid="ctl00_ContentPlaceHolder1_rdwClearCEP_C_txtClearedRemarks";
	public static String clearcep="ctl00_ContentPlaceHolder1_rdwClearCEP_C_ButtonSubmit_input";
	private static String closeviewcomp="html/body/div[3]/div/div/div/button";
	private static String insuffstageid=".//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[1]";
	String fileupload="ctl00_ContentPlaceHolder1_rdmAddDoc_C_grdDocumentList_ctl00_ctl04_rauComponentDocumentfile0";
	String dataentryupload="ctl00_ContentPlaceHolder1_rwmAddressDocument_C_grdDocumentList_ctl00_ctl04_lnkFileName";
	Boolean bool=false;
	public void normalcaseregn()
	{
		try
		{
			Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(newcase)));
		if(Driver.instance.findElement(By.id(newcase)).isDisplayed())
		{
			bool=true;
			Driver.instance.findElement(By.id(newcase)).click();
		}
		Assert.assertTrue(bool);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(addnew)));
			Driver.instance.findElement(By.id(selectclient)).click();
			clientsize=Driver.instance.findElements(By.xpath(clientoption)).size();
		/*	List<WebElement> allclients = Driver.instance.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlClient_DropDown']/div/ul/li"));
			System.out.println("This is by for each starts");
			 for(WebElement s:allclients){  
			     System.out.println(s.getText());  
			   }  
			System.out.println("This is by for each ends");*/
			//System.out.println(clientsize);
			Boolean Clientval=false;
			for(int i=1;i<=clientsize;i++)
			{
				Thread.sleep(500);
				clients=Driver.instance.findElement(By.xpath(clientoption+"["+i+"]")).getText();
				//System.out.println(clients);
				if(clients.trim().equals(Base.client.trim()))
				{
					Driver.instance.findElement(By.xpath(clientoption+"["+i+"]")).click();
					//System.out.println("Client found");
					
					Clientval= true;
					break;
				}		
			}
//			if( Clientval==false)
//			{
//				System.out.println("Client not found");
//			}
//			else
//				System.out.println("Client  found");
			Assert.assertTrue(Clientval);
			Thread.sleep(1500);
			Driver.instance.findElement(By.id(selectproject)).click();
			projectsize=Driver.instance.findElements(By.xpath(projectoption)).size();
			Boolean Projectval=false;
			//System.out.println(projectsize);
			for(int i=1;i<=projectsize;i++)
			{
				Thread.sleep(500);
				projects=Driver.instance.findElement(By.xpath(projectoption+"["+i+"]")).getText();
				//System.out.println(projects);
				if(projects.trim().equals(Base.project.trim()))
				{
					Driver.instance.findElement(By.xpath(projectoption+"["+i+"]")).click();
					Projectval=true;					
				}								
			}		
//			if(Projectval=true)
//			{
//				System.out.println("Project Found");
//			}
//			else
//				System.out.println("Project Not Found");
			Assert.assertTrue(Projectval);
			Thread.sleep(500);
			Driver.instance.findElement(By.id(firstnamepath)).sendKeys(firstname);
			Driver.instance.findElement(By.id(lastnamepath)).sendKeys(lastname);
			Driver.instance.findElement(By.id(DOBpath)).sendKeys(DOB);
			Driver.instance.findElement(By.id(emailpath)).sendKeys(email+"@kadamba.com");
			Driver.instance.findElement(By.id(fathersfirstnamepath)).sendKeys(lastname);
			Driver.instance.findElement(By.id(fatherslastnamepath)).sendKeys(firstname);
			Driver.instance.findElement(By.id(linkedinpath)).sendKeys(linkedin);
			Driver.instance.findElement(By.id(nationalitypath)).sendKeys(nationality);
			Driver.instance.findElement(By.id(landlinenumberpath)).sendKeys(landlinenumber);
			Driver.instance.findElement(By.id(mobilenumberpath)).sendKeys(mobilenumber);
			Driver.instance.findElement(By.id(emergencycontactnumberpath)).sendKeys(emergencycontactnumber);
			Driver.instance.findElement(By.id(emergencycontactpersonpath)).sendKeys(emergencycontactperson);
			/*Driver.instance.findElement(By.id(firstnamepath)).sendKeys(Excel_Read.input[1][0]);
			Driver.instance.findElement(By.id(lastnamepath)).sendKeys(Excel_Read.input[1][1]);
			Driver.instance.findElement(By.id(DOBpath)).sendKeys(Excel_Read.input[1][2]);
			Driver.instance.findElement(By.id(emailpath)).sendKeys(Excel_Read.input[1][3]);
			Driver.instance.findElement(By.id(fathersfirstnamepath)).sendKeys(Excel_Read.input[1][4]);
			Driver.instance.findElement(By.id(fatherslastnamepath)).sendKeys(Excel_Read.input[1][5]);
			Driver.instance.findElement(By.id(linkedinpath)).sendKeys(Excel_Read.input[1][6]);
			Driver.instance.findElement(By.id(nationalitypath)).sendKeys(Excel_Read.input[1][7]);
			Driver.instance.findElement(By.id(landlinenumberpath)).sendKeys(Excel_Read.input[1][8]);
			Driver.instance.findElement(By.id(mobilenumberpath)).sendKeys(Excel_Read.input[1][9]);
			Driver.instance.findElement(By.id(emergencycontactnumberpath)).sendKeys(Excel_Read.input[1][10]);
			Driver.instance.findElement(By.id(emergencycontactpersonpath)).sendKeys(Excel_Read.input[1][11]);*/
			Driver.instance.findElement(By.id(exppath)).click();
			
			
			Driver.instance.findElement(By.id(selectgender)).click();			
			gendersize=Driver.instance.findElements(By.xpath(genderoption)).size();
			//System.out.println(projectsize);
			for(int i=1;i<=gendersize;i++)
			{
				Thread.sleep(500);
				genders=Driver.instance.findElement(By.xpath(genderoption+"["+i+"]")).getText();
				//System.out.println(projects);
				if(genders.equals(gender))
				{
					Driver.instance.findElement(By.xpath(genderoption+"["+i+"]")).click();
					break;
				}		
			}		
			
			
			Driver.instance.findElement(By.id(selectmarital)).click();			
			maritalsize=Driver.instance.findElements(By.xpath(maritaloption)).size();
			//System.out.println(projectsize);
			for(int i=1;i<=maritalsize;i++)
			{
				Thread.sleep(500);
				maritals=Driver.instance.findElement(By.xpath(maritaloption+"["+i+"]")).getText();
				//System.out.println(projects);
				if(maritals.equals(marital))
				{
					Driver.instance.findElement(By.xpath(maritaloption+"["+i+"]")).click();
					break;
				}			
			}	
			
			
			Driver.instance.findElement(By.id(selectid)).click();			
			IDsize=Driver.instance.findElements(By.xpath(idoption)).size();
			//System.out.println(projectsize);
			for(int i=1;i<=IDsize;i++)
			{
				Thread.sleep(500);
				IDs=Driver.instance.findElement(By.xpath(idoption+"["+i+"]")).getText();
				//System.out.println(projects);
				if(IDs.equals(ID))
				{
					Driver.instance.findElement(By.xpath(idoption+"["+i+"]")).click();
					Driver.instance.findElement(By.id(idpath)).sendKeys(employeeid);
					break;
				}
			}
			
			Driver.instance.findElement(By.id(addcomponent)).click();
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(casedocbutton))).click();
			//reg.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(adddocument)));
			Thread.sleep(6000);
			Driver.instance.findElement(By.id(casedocupload)).sendKeys(Base.documentuploadpath1);
			Thread.sleep(5000);
			Driver.instance.findElement(By.id(adddocument)).click();
			 try
			 {
			 WebElement download=wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(docdownload)));
			// System.out.println("Document uploaded successfully");
			 text("Document uploaded successfully");
			 Excel_write.inputExcel("Case Document upload", "Pass");
			 download.click();
			 casedocname=Driver.instance.findElement(By.id(casedocnameid)).getText();
			 Driver.instance.findElement(By.id(docclose)).click();
			 Thread.sleep(500);
			 }catch(Exception e)
			 {
				 //System.out.println("Issue in document upload"+e );	
				 text("Issue in document upload");
				 Excel_write.inputExcel("Case Document upload", "Fail");
			 }		
//			 new Robot().keyPress(KeyEvent.VK_DOWN);
//			 new Robot().keyRelease(KeyEvent.VK_DOWN);
//			 new Robot().keyPress(KeyEvent.VK_ENTER);
//			 new Robot().keyRelease(KeyEvent.VK_ENTER);
//			 Thread.sleep(500);
			 //Driver.instance.switchTo().frame(1);
				//
				//
				//Driver.instance.switchTo().defaultContent();
	}catch(Exception e)
		{
		System.out.println(e);
		}	
	}
	 public Logout selectcomponent() throws Exception
	 {
		//text("***************Case Registration with Normal Flow***************");
		normalcaseregn(); 
		Thread.sleep(2000);
		int doc=0;
	 for(int j=0;j<=Componentsize;j++)
		{
			try{
			
			if(j<=9)
			{
				Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_chkSelectSelectCheckBox")).click();
				try{
				Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_btnAdddoc_input")).click();
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(adddocumentcom)));
				upload=Driver.instance.findElement(By.id(fileupload));
				upload.sendKeys(Base.documentuploadpath4);
				Thread.sleep(2500);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(adddocumentcom))).click();
				Thread.sleep(3000);
				if(doc==0)
				comdocname=Driver.instance.findElement(By.id(comdocnameid)).getText();
				doc++;
				}catch(Exception e)
				{
					System.out.println(e);
				}
				Driver.instance.findElement(By.id(closedoc)).click();
			}
			else
			{
				Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_chkSelectSelectCheckBox")).click();
				/*try{
					Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_btnAdddoc_input")).click();
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(adddocumentcom)));
					upload=Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdmAddDoc_C_grdDocumentList_ctl00_ctl04_rauComponentDocumentfile0"));
					upload.sendKeys(Base.documentuploadpath1);
					Thread.sleep(2500);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(adddocumentcom))).click();
					Thread.sleep(2500);
					}catch(Exception e)
					{
						System.out.println(e);
					}
				Driver.instance.findElement(By.id(closedoc)).click();*/
			}
		}catch(Exception e)
		{
			//System.out.println(e);
			continue;
		}
		}
	 try
	 {
	  savesubmit();
	  text("Case Registered Successfully");
	  Excel_write.inputExcel("Case Registration", "Pass");
	  Driver.instance.findElement(By.id(home)).click();
	  wait.until(ExpectedConditions.elementToBeClickable(By.id(action)));
	  new Select(Driver.instance.findElement(By.id(stagedropdown))).selectByValue("1");
	  //wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(action)));
	  Thread.sleep(3000);
	  String name=Driver.instance.findElement(By.xpath(casename)).getText();
	  bool=false;
	  if(name.trim().replace(" ", "").equals((firstname.concat(lastname)).trim()))
	  {
		  bool=true;
		  text("Registered case Present in Data Entry Supervision Stage");
		  Excel_write.inputExcel("Registered case in Data Entry", "Pass");
	  }
	  Assert.assertTrue(bool);
	  Thread.sleep(3000);
	  new Select(Driver.instance.findElement(By.xpath(priority))).selectByVisibleText("High");
	  Thread.sleep(1000);
	  Driver.instance.findElement(By.xpath(priorityes)).click();
	  Thread.sleep(1000);
	  WebElement userid=Driver.instance.findElement(By.id("ctl00_lblUsername"));
	  String uname=userid.getText().replace("Welcome ","").trim();
	  new Select(Driver.instance.findElement((By.id(reservedfor)))).selectByVisibleText(uname);
	  new Select(Driver.instance.findElement(By.id(stagedropdown))).selectByValue("2");
	  Thread.sleep(3000);
	  Driver.instance.findElement(By.id(getnext)).click();
	  wait.until(ExpectedConditions.elementToBeClickable(By.id(casedocedit))).click();
	  wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan"))).click();
	  Thread.sleep(2000);
	  wait.until(ExpectedConditions.elementToBeClickable(By.id(casedoceditbutton))).click();
	  wait.until(ExpectedConditions.elementToBeClickable(By.id(adddoceditbutton)));
	  dcasedocname=Driver.instance.findElement(By.id(casedoceditnameid)).getText();
	  Driver.instance.findElement(By.id(casedoceditcancelid)).click();
	  wait.until(ExpectedConditions.elementToBeClickable(By.id(casedoceditbutton)));
	  WebElement checknameid=Driver.instance.findElement(By.xpath(".//*[@id='tabStrip']/div/ul/li[1]/a/span/span/span"));
	  String checkname=checknameid.getText();
	  Thread.sleep(2500);
	  //WebElement element = Driver.instance.findElement(By.xpath("//html/body/form/div[5]/div[2]/div[3]/div/div/div[4]/div/div[2]/div/iframe")); 
	  WebElement element = Driver.instance.findElement(By.xpath(".//*[@id='AllPageView']/div/iframe"));
	  
	  Driver.instance.switchTo().frame(element);
	  Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_btn"+checkname+"Document_input")).click();
	  Thread.sleep(3000);
	  WebElement doccomnameid=Driver.instance.findElement(By.id(dataentryupload));
	  dcomdocname=doccomnameid.getText();
	   Driver.instance.findElement(By.id(comdoceditcancelid)).click();
	  if(dcasedocname.trim().replace(" ", "").equals(casedocname.trim().replace(" ","")))
	  {
  		text("Case documents are matched");
  		Excel_write.inputExcel("Case Document match", "Pass");
	  }
	  else
	  {
	  text("Case Documents are not matched");
	  Excel_write.inputExcel("Case Document match", "Fail");
	  }
	  System.out.println("DataEntry Document= "+dcomdocname.trim().replace(" ", ""));
	  System.out.println("Registered Document= "+comdocname.trim().replace(" ", ""));
	  if(dcomdocname.trim().equals(comdocname.trim()))
			  {
		  		text("Component documents are matched");	
		  		Excel_write.inputExcel("Component Document match", "Pass");		  		
			  }
	  else
	  {
		  text("Component Documents are not matched");
	      Excel_write.inputExcel("Component Document match", "Fail");
	  }
	  
	 }catch(Exception e)
	 {
		 System.out.println(e);
		 text("Issue in Normal Case");
		 Excel_write.inputExcel("Normal Case Completion", "Fail");	
	 }
	// System.out.println("Case Registered Successfully");
	 Driver.instance.switchTo().defaultContent();
	 return new Logout();
	 }
	 public Logout insuffcheck() throws Exception
	 {
		// text("***************Case Registration with Insuff Flow***************");
		 normalcaseregn(); 		 
		 for(int j=0;j<=Componentsize;j++)
			{
				try{
				
				if(j<9)
				{
				Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_chkSelectSelectCheckBox")).click();
				Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_chkInsuff")).click();
				Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_txtInsuff")).sendKeys(nationality);
				}
				else
				{
					Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_chkSelectSelectCheckBox")).click();
				}
			}catch(Exception e)
			{
				//System.out.println(e);
				continue;
			}
			}
		  savesubmit();
		  //System.out.println("Case Registered Successfully with insuff check");
		  text("Case Registered Successfully with insuff check");
		  Excel_write.inputExcel("Case Registration for insuff", "Pass");
		  Driver.instance.findElement(By.id(home)).click();
		  wait.until(ExpectedConditions.elementToBeClickable(By.id(action)));
		  new Select(Driver.instance.findElement(By.id(stagedropdown))).selectByValue("1");
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(action)));
		  Thread.sleep(3000);
		  String ref=Driver.instance.findElement(By.xpath(Case_Search.DBref)).getText();
		  Thread.sleep(2000);
		  wait.until(ExpectedConditions.elementToBeClickable(By.id(action))).click();
		  wait.until(ExpectedConditions.elementToBeClickable(By.id(workstage)));
		  Driver.instance.findElement(By.id(coinsuff)).click();
		  Thread.sleep(1000);
		  Driver.instance.findElement(By.xpath(spinsuff)).click();
		  Driver.instance.findElement(By.id(actioncaserefsearch)).sendKeys(ref);
		//  Driver.instance.findElement((By.id(casesource))).click();
		 // Driver.instance.findElement(By.xpath(casesourcevalue)).click();
		  Driver.instance.findElement(By.id(actioncasesearch)).click();
		 insuffsize=Driver.instance.findElements(By.xpath(caseinsuff)).size();
		 	 bool=false;
		  if(insuffsize==1)
		  {
			  bool=true;
			   wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(caseinsuffclear)));
			  Thread.sleep(5000);
			  WebElement element = Driver.instance.findElement(By.id(caseinsuffclear));

			  Actions actions = new Actions(Driver.instance);

			  actions.moveToElement(element).click().perform();
			  
			 // Driver.instance.findElement(By.id(caseinsuffclear)).click();
			  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(clearinsuff)));
			  int s=0;
			  for(int i=4;i<=Componentsize;i=i+2)
			  {
				  Thread.sleep(1500);
				  try
				  {
					  for(int j=s;j<=s;j++)
					  {
						  String comment=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00__"+j+"']/td[27]")).getText();
						  if(comment.equals(nationality))
						  {
							  text("Insuff Comments matched");
							  Excel_write.inputExcel("insuff comment match", "Pass");
						  }
						  else
						  {
							  text("Insuff Comments are not matched");
							  Excel_write.inputExcel("insuff comment match", "Fail");
						  }
					  Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+i+"_chkInsuffComponentSelectCheckBox")).click();
					  Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+i+"_txtInsuffClearComments")).sendKeys(nationality+firstname);
					  Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+i+"_btnUploadInsuffDocuments")).click();
					  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(insuffadddoc)));
					   doc=Driver.instance.findElement(By.xpath(insuffdocpath)).getText();
						if(doc.isEmpty())
						{
						Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl0"+i+"_rauComponentDocumentfile0")).sendKeys(Base.documentuploadpath4);
						Thread.sleep(2500);
						try
						{
						Driver.instance.findElement(By.id(insuffadddoc)).click();
						Thread.sleep(2000);
						Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl0"+i+"_lnkbtnDownload")).click();
						insuffdoc=Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl0"+i+"_lnkFileName")).getText();
						Driver.instance.findElement(By.xpath(insuffclosedoc)).click();				
						text("insuff Document Uploaded and downloaded Successfully");
						Excel_write.inputExcel("insuff document upload and download", "Pass");
						}catch(Exception e)
						{
							text("Insuff Document Fails to upload/Fails to download");
							Excel_write.inputExcel("insuff document upload and download", "Fail");
						}
						}
						else
						{
							Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl0"+i+"_lnkbtnDownload")).click();
							Driver.instance.findElement(By.xpath(insuffclosedoc)).click();
							Excel_write.inputExcel("insuff document upload and download", "Already uploaded");						
						}
					  }
					  s++;
				  }catch(Exception e)
				  {
					  break;
				  }
			  }
			  Driver.instance.findElement(By.id(clearinsuff)).click();
			  wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan"))).click();		
			  text("insuff cleared successfully");
			  Excel_write.inputExcel("insuff Clear", "Pass");
		  }
		  Assert.assertTrue(bool);
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(workstage))).click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(action)));
		  wait.until(ExpectedConditions.elementToBeClickable(By.id(action)));
		  Driver.instance.findElement(By.id(workcaseref)).sendKeys(ref);
		  Thread.sleep(2000);
		  wait.until(ExpectedConditions.elementToBeClickable(By.id(workcasesearch))).click();
		  casesize=Driver.instance.findElements(By.xpath(Case_Search.DBref)).size();
		  bool=false;
		  if(casesize==1)
		  {
			  bool=true;
			  Thread.sleep(2000);
			  Driver.instance.findElement(By.xpath(Case_Search.DBref)).click();
		  }
		  Assert.assertTrue(bool);
		  try
		  {
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(addcomponent))).click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(savesubmit)));
		  WebElement docclick=Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl07_btnAdddoc_input"));
		  docclick.click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(adddocumentcom)));
		  comdoc=Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdmAddDoc_C_grdDocumentList_ctl00_ctl06_lnkFileName")).getText();
		 
		  if(comdoc.trim().replace(" ","").equals(insuffdoc.trim().replace(" ","")))
		  {
			  text("insuff document present in case registration");
			  Excel_write.inputExcel("insuff document in Case Registration", "Pass");
		  }
		  else
		  {
			  text("insuff document not present in case registration");
			  Excel_write.inputExcel("insuff document in Case Registration", "Fail");
		  }
		  		
		  
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(adddocumentcom)));
		  Driver.instance.findElement(By.id(closedoc)).click();
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(savesubmit))).click();
		  wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan"))).click();	
		  text("Insuff Cleared case submitted successfully");
		  Excel_write.inputExcel("Submitting insuff Cleared case ", "Pass");
		  }catch(Exception e)
		  {
			  System.out.println(e);
			  text("Issue While Submitting insuff cleared case");
			  Excel_write.inputExcel("Submitting insuff Cleared case ", "Fail");
		  }
		  Thread.sleep(1000);
		 return new Logout();
	 }
	 public Logout CEP() throws Exception
	 {
		// text("***************Case Registration with CEP Flow***************");
		 normalcaseregn();  
		 a=0;
		 for(int j=0;j<=Componentsize;j++)
			{
			 try{
					
					if(j<=9)
					{
					Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_chkSelectSelectCheckBox")).click();
					compname=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00__"+a+"']/td[3]")).getText();
					//System.out.println("Before CEP"+compname);
					if(compname.equals("Previous Employment")||compname.equals("Reference 2"))
					{
						Thread.sleep(1000);
						Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_chkYTR")).click();
						Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_chkYTR")).sendKeys(nationality);
					}
					
					}
					else
					{
						Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_chkSelectSelectCheckBox")).click();
						compname=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00__"+a+"']/td[3]")).getText();
						//System.out.println("Before CEP1"+compname);
						if(compname.equals("Previous Employment 2")||compname.equals("Reference 3"))
						{
							Thread.sleep(1000);
							Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_chkYTR")).click();
							Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_txtYTR")).sendKeys(nationality);
						}
					}a++;
			}catch(Exception e)
			{
				//System.out.println(e);
				continue;
			}
			}
		  savesubmit();
		//  System.out.println("Case Registered Successfully with CEP check");
		  text("Case Registered Successfully with CEP check");
		  Excel_write.inputExcel("Case Registered with CEP check", "Pass");
		  Driver.instance.findElement(By.id(home)).click();
		  wait.until(ExpectedConditions.elementToBeClickable(By.id(action)));
		  new Select(Driver.instance.findElement(By.id(stagedropdown))).selectByValue("1");
		  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(action)));
		  Thread.sleep(3000);
		  String ref=Driver.instance.findElement(By.xpath(Case_Search.DBref)).getText();
		  Thread.sleep(2000);
		  wait.until(ExpectedConditions.elementToBeClickable(By.id(action))).click();
		  wait.until(ExpectedConditions.elementToBeClickable(By.id(workstage)));
		  Driver.instance.findElement(By.id(actionstage)).click();
		  Thread.sleep(1000);
		  Driver.instance.findElement(By.xpath(cepstageid)).click();
		  Driver.instance.navigate().refresh();
		  wait.until(ExpectedConditions.elementToBeClickable(By.id(actioncasesearch)));
		  Thread.sleep(2000);
		  Driver.instance.findElement(By.id(coinsuff)).click();
		  Thread.sleep(2000);
		  Driver.instance.findElement(By.xpath(spinsuff)).click();
		  Driver.instance.findElement(By.id(actioncaserefsearch)).sendKeys(ref);
		  wait.until(ExpectedConditions.elementToBeClickable(By.id(actioncasesearch))).click();
		  Thread.sleep(3000);
		  insuffsize=Driver.instance.findElements(By.xpath(cepstagesize)).size();
		  System.out.println(insuffsize);
		 	 bool=false;
		  if(insuffsize>=1)
		  {
			  bool=true;
			  for(int size=0;size<insuffsize;size++)
			  {
				  wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[5]")));
				  Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[5]")).click();
				  wait.until(ExpectedConditions.elementToBeClickable(By.id(cepsubmitbutton)));
				  String raisecomments=Driver.instance.findElement(By.id(cepraisecommentsid)).getText();
				  if(((nationality+idvalue).trim()).equals(raisecomments.trim()))
						  {
					  		text("CEP Raise comments are same");
					  		Excel_write.inputExcel("Comments for CEP", "Pass");
						  }
				  else
				  {
					  text("CEP Raise comments are not same");
					  Excel_write.inputExcel("Comments for CEP", "Fail");
				  }
				  String nullfile=Driver.instance.findElement(By.xpath(cepfilename)).getText();
				  if(nullfile.isEmpty())
				  {
					  Driver.instance.findElement(By.id(cepdocupload)).sendKeys(Base.documentuploadpath2);
					  Thread.sleep(1500);
					  Driver.instance.findElement(By.id(cepdocsave)).click();
					  text("CEP Document uploaded successfully");
					  Excel_write.inputExcel("Upload CEP Document", "Pass");
				  }
				  else
				  {
					  text("Document already uploaded/Failed to upload");
					  Excel_write.inputExcel("Upload CEP Document", "Fail");
				  }
				  		
				  
				  Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_txtClearedRemarks")).sendKeys(firstname);
				  Thread.sleep(2500);
				  Driver.instance.findElement(By.id(cepsubmitbutton)).click();
				  wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan"))).click();
				  wait.until(ExpectedConditions.elementToBeClickable(By.id(actioncasesearch))).click();			
				  Thread.sleep(4000);
			  }
			  text("CEP Cleared Successfully");
			  Excel_write.inputExcel("Clear CEP", "Pass");
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(workstage))).click();
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(action)));
			  new Select(Driver.instance.findElement(By.id(stagedropdown))).selectByValue("1");
			  Driver.instance.findElement(By.id(workcaseref)).sendKeys(ref);
			  //Driver.instance.findElement(By.id(workcasesearch)).click();
			  Thread.sleep(2000);
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(workcasesearch))).click();
			  Thread.sleep(2000);
			  Driver.instance.findElement(By.id(buttonview)).click();
			  Thread.sleep(2500);
			  int statussize=Driver.instance.findElements(By.xpath(".//*[@id='secTable']/tbody/tr/td[2]/span")).size();
			  bool=true;
			  for(int stasize=1;stasize<statussize;stasize++)
			  {
				  String status=Driver.instance.findElement(By.xpath(".//*[@id='secTable']/tbody/tr["+stasize+"]/td[2]/span")).getText();
				  if(status.contains("CEP"))
				  {
					  
					  bool=false;
				  }
			  }		 
			  if(bool==false)
			  {
				  text("CEP Not Cleared");
				  Excel_write.inputExcel("CEP Cleared Component", "Pass");
			  }
			  else
			  {
				  text("CEP Cleared");
				  Excel_write.inputExcel("CEP Cleared Component", "Fail");
			  }
			  Driver.instance.findElement(By.xpath(closeviewcomp)).click();
		  }		  
		  
		  Assert.assertTrue(bool);
		  Thread.sleep(1000);
		  return new Logout();
	 }
	 public Logout insuffcep() throws Exception
	 {
		// text("***************Case Registration with CEP & Insuff Flow***************");
		 normalcaseregn();
		 a=0;b=0;
		 
		 for(int j=0;j<=Componentsize;j++)
			{
				try{
				
				if(j<=9)
				{
				Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_chkSelectSelectCheckBox")).click();
				compname=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00__"+a+"']/td[3]")).getText();
				System.out.println("Before insuff"+compname);
				if(compname.equals("Permanent")||compname.equals("UG1")||compname.equals("Previous Employment")||compname.equals("Highest 1")||compname.equals("Voter ID")||compname.equals("ID Check 2")||compname.equals("Database")||compname.equals("Reference 2"))
				{
					compnamelist[b]=compname;
				Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_chkInsuff")).click();
				Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_txtInsuff")).sendKeys(nationality);
				System.out.println("raise component["+b+"]="+compnamelist[b]);
				b++;
				}
				
				}
				else
				{
					Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_chkSelectSelectCheckBox")).click();
					compname=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00__"+a+"']/td[3]")).getText();
					System.out.println("Before insuff1"+compname);
					if(compname.equals("Previous Employment 3")||compname.equals("Reference 2")||compname.equals("Reference 3"))
					{
						compnamelist[b]=compname;
						Thread.sleep(750);
					Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_chkInsuff")).click();
					Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_txtInsuff")).sendKeys(nationality);
					Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_chkYTR")).click();
					Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_txtYTR")).sendKeys(nationality+idvalue);
					System.out.println("raise component["+b+"]="+compnamelist[b]);
					b++;
					}
				}a++;
			}catch(Exception e)
			{
				//System.out.println(e);
				continue;
			}
			}
		 savesubmit();
			//  System.out.println("Case Registered Successfully with CEP check");
			  text("Case Registered Successfully with insuff & CEP check");
			  Excel_write.inputExcel("Case Registered with insuff & CEP check", "Pass");
			  Driver.instance.findElement(By.id(home)).click();
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(action)));
			  new Select(Driver.instance.findElement(By.id(stagedropdown))).selectByValue("1");
			  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(action)));
			  Thread.sleep(4000);
			  String ref=Driver.instance.findElement(By.xpath(Case_Search.DBref)).getText();
			  Thread.sleep(2000);
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(action))).click();
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(workstage)));
			 
			  Driver.instance.findElement(By.id(actionstage)).click();
			  Thread.sleep(1000);
			  Driver.instance.findElement(By.xpath(cepstageid)).click();
			 // Driver.instance.navigate().refresh();
			  Thread.sleep(2000);
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(actioncasesearch)));
			  Thread.sleep(3000);
			  Driver.instance.findElement(By.id(coinsuff)).click();
			  Thread.sleep(2000);
			  Driver.instance.findElement(By.xpath(spinsuff)).click();
			  Driver.instance.findElement(By.id(actioncaserefsearch)).sendKeys(ref);
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(actioncasesearch))).click();
			  Thread.sleep(3000);
			  insuffsize=Driver.instance.findElements(By.xpath(cepstagesize)).size();
			  System.out.println(insuffsize);
			 	 bool=false;
			  if(insuffsize>=1)
			  {
				  bool=true;
				  for(int size=0;size<insuffsize;size++)
				  {
					  try
					  {
					  wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[5]")));
					  Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[5]")).click();
					  wait.until(ExpectedConditions.elementToBeClickable(By.id(cepsubmitbutton)));
					  String raisecomments=Driver.instance.findElement(By.id(cepraisecommentsid)).getText();
					  if(((nationality+idvalue).trim()).equals(raisecomments.trim()))
							  {
						  		text("CEP Raise comments are same");
						  		 Excel_write.inputExcel("CEP Raise Comments", "Pass");
							  }
					  else
					  {
						  text("CEP Raise comments are not same");
						  Excel_write.inputExcel("CEP Raise Comments", "Fail");
					  }
					  String nullfile=Driver.instance.findElement(By.xpath(cepfilename)).getText();
					  if(nullfile.isEmpty())
					  {
						  Driver.instance.findElement(By.id(cepdocupload)).sendKeys(Base.documentuploadpath2);
						  Thread.sleep(1500);
						  Driver.instance.findElement(By.id(cepdocsave)).click();
						  text("CEP Document uploaded successfully");
						  Excel_write.inputExcel("Upload CEP Document", "Pass");
					  }
					  else
					  {
						  text("Document already uploaded/Failed to upload");
						  Excel_write.inputExcel("Upload CEP Document", "Fail");
					  }
					  
					  Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwClearCEP_C_txtClearedRemarks")).sendKeys(firstname);
					  Thread.sleep(2500);
					  Driver.instance.findElement(By.id(cepsubmitbutton)).click();
					  wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan"))).click();
					  wait.until(ExpectedConditions.elementToBeClickable(By.id(actioncasesearch))).click();			
					  Thread.sleep(4000);
				  }
				  catch(Exception e)
				  {
					  break;
				  }
				  }
				  text("CEP Cleared Successfully");
				  Excel_write.inputExcel("CEP Cleared", "Pass");
			  }
			  
			  
			  Driver.instance.findElement(By.id(actionstage)).click();
			  Thread.sleep(1000);
			  Driver.instance.findElement(By.xpath(insuffstageid)).click();
			 // Driver.instance.navigate().refresh();
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(actioncasesearch)));
			  Thread.sleep(3000);
			  Driver.instance.findElement(By.id(coinsuff)).click();
			  Thread.sleep(1000);
			  Driver.instance.findElement(By.xpath(spinsuff)).click();
			  Driver.instance.findElement(By.id(actioncaserefsearch)).sendKeys(ref);
			//  Driver.instance.findElement((By.id(casesource))).click();
			 // Driver.instance.findElement(By.xpath(casesourcevalue)).click();
			  Driver.instance.findElement(By.id(actioncasesearch)).click();
			 insuffsize=Driver.instance.findElements(By.xpath(caseinsuff)).size();
			 	 bool=false;
			  if(insuffsize==1)
			  {
				  bool=true;
				   wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(caseinsuffclear)));
				  Thread.sleep(6000);
				  WebElement element = Driver.instance.findElement(By.id(caseinsuffclear));

				  Actions actions = new Actions(Driver.instance);

				  actions.moveToElement(element).click().perform();
				  
				 // Driver.instance.findElement(By.id(caseinsuffclear)).click();
				  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(clearinsuff)));
				  int s=0;
				  for(int i=4;i<=Componentsize;i=i+2)
				  {
					  Thread.sleep(1500);
					  try
					  {
						      //Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl0"+i+"_chkInsuffComponentSelectCheckBox"));
							  String comment=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00__"+s+"']/td[27]")).getText();
							  String insuffcomp=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00__"+s+"']/td[17]")).getText();
							  System.out.println("After insuff"+insuffcomp);
							  System.out.println("s="+s);
							  System.out.println(compnamelist[s]);
							  if(insuffcomp.equals(compnamelist[s]))
							  {
								  bool=true;
								  text("Insuff components are matched");
								  Excel_write.inputExcel("Insuff Components match", "Pass");
							  if(comment.equals(nationality))
							  {
								  text("Insuff Comments matched");
								  Excel_write.inputExcel("Insuff Comments match", "Pass");
							  }
							  else
							  {
								  text("Insuff Comments are not matched");
								  Excel_write.inputExcel("Insuff Comments match", "Fail");
							  }
							  }
							  else
							  {
								  text("Insuff components are not matched");
								  Excel_write.inputExcel("Insuff Component match", "Pass");
								  bool=false;
							  }
							  Assert.assertTrue(bool);
							  String dummy="0";
							  if(i>9)
							  {
								  dummy="";
							  }
						  Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl"+dummy+i+"_chkInsuffComponentSelectCheckBox")).click();
						  Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl"+dummy+i+"_txtInsuffClearComments")).sendKeys(nationality+firstname);
						  Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwInsuffClearWindow_C_grdInsuffClear_ctl00_ctl"+dummy+i+"_btnUploadInsuffDocuments")).click();
						  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(insuffadddoc)));
						   doc=Driver.instance.findElement(By.xpath(insuffdocpath)).getText();
							if(doc.isEmpty())
							{
							Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl04_rauComponentDocumentfile0")).sendKeys(Base.documentuploadpath4);
							Thread.sleep(2500);
							try
							{
							Driver.instance.findElement(By.id(insuffadddoc)).click();
							Thread.sleep(2000);
							Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl04_lnkbtnDownload")).click();
							insuffdoc=Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl04_lnkFileName")).getText();
							Driver.instance.findElement(By.xpath(insuffclosedoc)).click();				
							text("insuff Document Uploaded and downloaded Successfully");
							Excel_write.inputExcel("Upload Insuff Document", "Pass");
							}catch(Exception e)
							{
								text("Insuff Document Fails to upload/Fails to download");
								Excel_write.inputExcel("Upload Insuff Document", "Fail");
							}
							}
							else
							{
								Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdwDocumentUpload_C_grdUploadDocuments_ctl00_ctl04_lnkbtnDownload")).click();
								Driver.instance.findElement(By.xpath(insuffclosedoc)).click();
								text("insuff Document Already Uploaded");
								Excel_write.inputExcel("Upload Insuff Document", "Already uploaded");
						  }
						  s++;
					  }catch(Exception e)
					  {
						  //System.out.println(e);
						  break;
					  }
				  }
				  Driver.instance.findElement(By.id(clearinsuff)).click();
				  wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan"))).click();		
				  text("insuff cleared successfully");
				  Excel_write.inputExcel("Insuff Clear", "Pass");
			  }
			  Assert.assertTrue(bool);
			  
			  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(workstage))).click();
			  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(action)));
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(action)));
			  Driver.instance.findElement(By.id(workcaseref)).sendKeys(ref);
			  Thread.sleep(2000);
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(workcasesearch))).click();
			  Thread.sleep(1000);
			  casesize=Driver.instance.findElements(By.xpath(Case_Search.DBref)).size();
			  bool=false;
			  if(casesize==1)
			  {
				  bool=true;
				  Thread.sleep(2000);
				  Driver.instance.findElement(By.xpath(Case_Search.DBref)).click();
			  }
			  Assert.assertTrue(bool);
			  
			  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(addcomponent))).click();
			  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(savesubmit)));
				 try
				 {
						Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl05_btnAdddoc_input")).click();
							 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(adddocumentcom)));
							  comdoc=Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_rdmAddDoc_C_grdDocumentList_ctl00_ctl04_lnkFileName")).getText();
							  System.out.println(comdoc+"&&"+insuffdoc);
							  
							  if(comdoc.trim().replace(" ","").equals(insuffdoc.trim().replace(" ","")))
							  {
								  text("insuff document present in case registration");
								  Excel_write.inputExcel("Insuff Document in Case Registration", "Pass");
							  }
							  else
							  {
								  text("insuff document not present in case registration");
								  Excel_write.inputExcel("Insuff Document in Case Registration", "Fail");
							  }
							  
							  wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(adddocumentcom)));
							  Driver.instance.findElement(By.id(closedoc)).click();
				 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(savesubmit))).click();
				  wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan"))).click();	
				  text("Insuff Cleared case submitted successfully");
				  Excel_write.inputExcel("Insuff Cleared Case Submitted", "Pass");
			  }
			  catch(Exception e)
			  {
				  System.out.println(e);
				  text("Issue While Submitting insuff cleared case");
				  Excel_write.inputExcel("Insuff Cleared Case Submitted", "Fail");
			  }			  
			  Driver.instance.findElement(By.id(home)).click();
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(action)));
			  new Select(Driver.instance.findElement(By.id(stagedropdown))).selectByValue("1");
			  Driver.instance.findElement(By.id(workcaseref)).sendKeys(ref);
			  //Driver.instance.findElement(By.id(workcasesearch)).click();
			  Thread.sleep(2000);
			  wait.until(ExpectedConditions.elementToBeClickable(By.id(workcasesearch))).click();
			  Thread.sleep(2000);
			  Driver.instance.findElement(By.id(buttonview)).click();
			  Thread.sleep(2500);
			  int statussize=Driver.instance.findElements(By.xpath(".//*[@id='secTable']/tbody/tr/td[2]/span")).size();
			  bool=true;
			  for(int stasize=1;stasize<statussize;stasize++)
			  {
				  String status=Driver.instance.findElement(By.xpath(".//*[@id='secTable']/tbody/tr["+stasize+"]/td[2]/span")).getText();
				  if(status.contains("CEP"))
				  {
					  
					  bool=false;
				  }
			  }		 
			  if(bool==false)
			  {
				  text("CEP Not Cleared");
				  Excel_write.inputExcel("CEP Clear", "Pass");
			  }
			  else
			  {
				  text("CEP Cleared");		
				  Excel_write.inputExcel("CEP Clear", "Fail");
			  }
			  Driver.instance.findElement(By.xpath(closeviewcomp)).click();	  		  
		  Assert.assertTrue(bool);
		  Thread.sleep(1500);
			  return new Logout();
	 }
	 
	 public Logout NA() throws Exception
	 {
		 //text("***************Case Registration with Not Applicable Flow***************");
		 normalcaseregn();
		 a=0;b=0;
		 
		 for(int j=0;j<=Componentsize;j++)
			{
				try{
				
				if(j<=9)
				{
				Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_chkSelectSelectCheckBox")).click();
				compname=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00__"+a+"']/td[3]")).getText();
				//System.out.println("Before insuff"+compname);
				if(compname.equals("Permanent")||compname.equals("Previous Employment")||compname.equals("Voter ID")||compname.equals("ID Check 2")||compname.equals("Database")||compname.equals("Reference 2"))
				{
					compnamelist[b]=compname;
				Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_chkNotRequired")).click();
				Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl0"+j+"_txtNotRequired")).sendKeys(nationality);
				//System.out.println("raise component["+b+"]="+compnamelist[b]);
				b++;
				}
				}
				else
				{
					Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_chkSelectSelectCheckBox")).click();
					compname=Driver.instance.findElement(By.xpath(".//*[@id='ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00__"+a+"']/td[3]")).getText();
					//System.out.println("Before insuff1"+compname);
					if(compname.equals("Previous Employment 3")||compname.equals("Reference 1")||compname.equals("UG1")||compname.equals("Highest 1"))
					{
						compnamelist[b]=compname;
						Thread.sleep(750);
					Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_chkNotRequired")).click();
					Driver.instance.findElement(By.id("ctl00_ContentPlaceHolder1_grdComponentDetails_ctl00_ctl"+j+"_txtNotRequired")).sendKeys(nationality);
					//System.out.println("raise component["+b+"]="+compnamelist[b]);
					b++;
					}
				}a++;
			}catch(Exception e)
			{
				//System.out.println(e);
				continue;
			}
			}
		 savesubmit();
		 Driver.instance.findElement(By.id(home)).click();
		 wait.until(ExpectedConditions.elementToBeClickable(By.id(action)));
		  new Select(Driver.instance.findElement(By.id(stagedropdown))).selectByValue("1");
		  //wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(action)));
		  Thread.sleep(3000);
		  String name=Driver.instance.findElement(By.xpath(casename)).getText();
		  bool=false;
		  if(name.trim().replace(" ", "").equals((firstname.concat(lastname)).trim()))
		  {
			  bool=true;
			  text("Registered case Present in Data Entry Supervision Stage");
			  Excel_write.inputExcel("Registered Case in Data Entry Supervision", "Pass");
		  }
		  Assert.assertTrue(bool);
		  Thread.sleep(1000);
		  Driver.instance.findElement(By.id(buttonview)).click();
		  Thread.sleep(1500);		  
		  int comsize=Driver.instance.findElements(By.xpath("//*[@id='secTable']/tbody/tr/td[1]/span")).size();
		  bool=true;
		  for(int size=1;size<=comsize;size++)
		  {
			  String getcomp=Driver.instance.findElement(By.xpath("//*[@id='secTable']/tbody/tr["+size+"]/td[1]/span")).getText();
			  if(getcomp.equals(compnamelist[size]))
			  {
				  bool=false;
			  }
			  Assert.assertTrue(bool);
		  }
		 text("Not Applicable funtionality working fine");
		 Excel_write.inputExcel("Not Applicable funtionality", "Pass");
		 Driver.instance.findElement(By.xpath(closeviewcomp)).click();
		 Thread.sleep(1000);
		 return new Logout();
	 }
	 public void savesubmit()
	 {
		 Driver.instance.findElement(By.id(savesubmit)).click();
		 wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.rwInnerSpan"))).click();
	 }
}