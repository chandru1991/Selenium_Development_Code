package check360_flow;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Reopen {

	public static void main(String[] args) throws InterruptedException {
		 WebDriver driver=new FirefoxDriver();
	      driver.manage().window().maximize();
	      driver.get("http://192.168.2.17:96");
	      driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).clear();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtUserName")).sendKeys("demoemp");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).clear();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtPassword")).sendKeys("Checks@123");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnLogin")).click();
		  Thread.sleep(10000);
		  driver.findElement(By.id("btnActions")).click();
		  Thread.sleep(3000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[7]")).click();
		  Thread.sleep(2500);
		  String rno=JOptionPane.showInputDialog("Enter reference number");
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).clear();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_TextBoxCaseReference")).sendKeys(rno);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnSearch")).click();
		  Thread.sleep(2500);
		  int wait=driver.findElements(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_grdTaskList_ctl00__0']/td[6]")).size();
		  if(wait!=0)
		  {
			  driver.findElement(By.id("ctl00_ContentPlaceHolder1_grdTaskList_ctl00_ctl04_btnCloseCase")).click();
		  }
		Thread.sleep(1500);
		driver.switchTo().alert().accept();
		Thread.sleep(3500);
		  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		  Thread.sleep(6000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_ddlAct_Input")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceHolder1_ddlAct_DropDown']/div/ul/li[8]")).click();
		  Thread.sleep(2500);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtReOpenCaseRefNo")).clear();
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_txtReOpenCaseRefNo")).sendKeys(rno);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnReOpenSearch_input")).click();
		  Thread.sleep(3500);
		  driver.findElement(By.id("_rfdSkinnedctl00_ContentPlaceHolder1_gviewReopenComponents_ctl00_ctl02_ctl01_chkSelectSelectCheckBox")).click();
		  Thread.sleep(1000);
		  driver.findElement(By.id("ctl00_ContentPlaceHolder1_btnReopenAssign_input")).click();
		  Thread.sleep(4000);
		  driver.findElement(By.cssSelector("span.rwInnerSpan")).click();
		  Thread.sleep(3000);
	}

}
